common-api-doc-template
=======================

Сервис предназначен для хранения, обработки и автозаполнения
запрашиваемых документов значениями параметров. ## Сторонние фреймворки
и библиотеки - SpringFramework - Apache HttpComponents - Spring Vault -
Micrometer application monitoring - Project Lombok - YARG - PF4J - JAXB
- Groovy - SpringFox - PDFBox - JSON Web Token Support

Загрузить новый шаблон
----------------------

Метод предназначен для загрузки нового предзаполненного шаблона формата
.xlsx или .docx В теле документа могут присутствовать переменные,
например ``${data.orderNumber}`` значения которых в дальнейшем могут
быть заполнены при дальнейшем экспорте. ### Формат запроса \| POST \|
/upload \| \|–|–\| В теле запроса передается файл. ### Формат ответа \|
Код \| Описание \| \|–|–\| \| 200 \| Шаблон загружен \| \| 401 \| Ошибка
авторизации \| \| 403 \| Ошибка в правах доступа \| \| 404 \| Ошибка в
endpoint \| ### Логика работы метода После успешной загрузки файла в
директории ``\rbip-service\templates`` создаются 2 файла: копия
загруженного и настроечный файл с именем {filename}+-json.xml, например,
``bg_reissueCommission_bill.docx`` и
``bg_reissueCommission_bill.docx-json.xml`` В XML файле присутствует
служебная информация как о самом файле, например, ``documentName``,
``docx`` так и о переменных, входящих в его тело. Так, если в теле
присутствуют переменные вида ``${companyHead.name}``,
``${common.currentDate}``, то их типы, в данном случае, ``companyHead``
и ``common`` попадут в соответствующий раздел ``bands``:

.. code:: xml

           <bands>
               <band name="companyHead" orientation="H">
                   <bands/>
                   <queries>
                       <query name="companyHead" type="json">
                           <script>parameter=param1 $.companyHead</script>
                       </query>
                   </queries>
               </band>
               <band name="common" orientation="H">
                   <bands/>
                   <queries>
                       <query name="common" type="json">
                           <script>parameter=param1 $.common</script>
                       </query>
                   </queries>
               </band>
           </bands>

Вставить QR-код в загруженный .docx документ
--------------------------------------------

Метод предназначен для вставки QR-код в конец загружаемого клиентом docx
документа. Ссылка с QR-кода ведет на сервис ``/verify`` в котором можно
посмотреть основную информацию о банковской гарантиии ### Формат запроса
\| POST \| /qrcode/docx \| \|–|–\| Параметры \| Параметр \| Тип \|
Описание \| \|–|–|–\| \| file \| file \| Загружаемый docx файл \| \|
orderId \| string \| ID заявки \| \| lotNumber \| string \| Номер лота
\| ### Формат ответа \| Код \| Описание \| \|–|–\| \| 200 \| Запрс
принят и обработан \| \| 401 \| Ошибка авторизации \| \| 403 \| Ошибка в
правах доступа \| \| 404 \| Ошибка в endpoint \| ### Логика работы
метода - Сервис принимает файл, и значения параметров ``orderId``,
``lotNumber`` - По параметрам ``key``, ``orderId`` и, опционально,
``lotNumber`` генерирует хэш - На основании хэш формируется QR-код,
ссылка из которого ведёт на отдельный сервис ``/verify`` - В ответ
клиенту с кодом ``200`` прикладывается ранее загруженный файл с
прикрепленным в конец документа QR-кодом

Настройки метода QR-кода
~~~~~~~~~~~~~~~~~~~~~~~~

-  baseURL в генерируемом QR коде
-  baseUrl: http://localhsot/test
-  #HMAC-key для полписи JWT в кодировке Base64
-  key: 2OJnRYbu9C9WWfBuXsOB7ENO7mXIALsJW3EKjd54Nhs=
-  Error Correction Level: L = ~7% correction, M = ~15% correction, Q =
   ~25% correction, H = ~30% correction
-  ecl: L
-  Image margin in pixels
-  margin: 0
-  ppm: 16
-  Image DPI
-  dpi: 600

Создать документ по шаблону
---------------------------

``endPoint: /form`` ### Формат запроса Существуют 2 варианта запроса,
исполняющих одну и ту функцию и отличающихся лишь формой подачи части
входных параметров: - GET: все параметры передаются в адресной строке,
при этом параметры заявки ``pd`` формата JSON “упакованы” в строку -
POST: параметры заявки ``pd`` формата JSON передаются в теле запроса

Параметры \| Параметр \| Тип \| Описание \| \|–|–|–\| \| p \| string \|
Название плагина \| \| pd \| string (JSON) \| Входные данные для плагина
\| \| template \| string \| Название шаблона \| ### Формат ответа \| Код
\| Описание \| \|–|–\| \| 200 \| Запрос принят и обработан \| \| 401 \|
Ошибка авторизации \| \| 403 \| Ошибка в правах доступа \| \| 404 \|
Ошибка в endpoint \| ### Логика работы сервиса После принятия запроса на
выдачу файла сервис, в зависимости от входных параметров, запрашивает в
БД требуемую информацию и выдаёт сгенерированный файл с заполненными
значениями переменных. #### Диаграмма последовательностей

.. code:: sequence

     Note right of client: /form?p={plugin}&template={template}&pd={parameters}
     client->CommonAPIDocTemplate: request
     CommonAPIDocTemplate->Репозитории: requests
     Note left of Репозитории: Запросы для заполнения значений переменных
     Репозитории->CommonAPIDocTemplate: responses
     CommonAPIDocTemplate->client: file

Плагины
^^^^^^^

Задачей плагина является собрать из внешних репозиториев необходимый для
подстановки в запрашиваемый шаблон набор значений параметров. Набор
плагинов хранится в папке
`sample <common-api-doc-template-pear-develop\rbip-plugin-bg\src\main\java\com\farzoom\common\pf\plugin\sample>`__
\| pluginName \| Class \| Входные параметры (pd) \| \|–|–|–\| \| agent
\| AgentPlugin.java \| orderId \| \| bgAgentAct \| BgAgentActPlugin.java
\| companyId, period \| \| BG \| BGPlugin.java \| orderId, lotNumber \|
\| bgStatsJournal \| BgStatsJournalPlugin.java \| orderId, lotNumber \|
#### Шаблоны Шаблоном представляет собой заранее подготовленный файл с
переменными, значения которых будут расчитаны соответствующим плагином.
Набор шаблонов хранится в папке `templates <rbip-service/templates>`__
## Расчитать количество страниц ### Формат запроса \| POST \|
/pageCounter \| \|–|–\| В теле запроса передаётся файл, для которого
требуется расчитать количество страниц. ### Формат ответа \| Код \|
Описание \| \|–|–\| \| 200 \| Запрос принят и обработан \| \| 401 \|
Ошибка авторизации \| \| 403 \| Ошибка в правах доступа \| \| 404 \|
Ошибка в endpoint \| \| 500 \| Ошибка в файле \| ### Ограничения -
Расширение: pdf, docx - Размер файла: 1048576 bytes ### Логика работы
сервиса Сервис принимает на вход файл с расширением .docx/.pdf,
вычисляет количество составляющих его страниц и вместе с кодом 200
выдает результат. ## Неиспользуемые методы ### Получить пример модели
/example ### Получить схему /schema ## Информация для разработчиков ###
Запуск в корне проекта mvn package cd rbip-service выполнить java
-Dfile.encoding=utf-8 -jar target/rbip-service-1.0-SNAPSHOT.jar

Шаблоны
~~~~~~~

папка ./templates должна лежать там же откуда производится запуск
приложения. внутри лежат примеры шаблонов, по которым должно быть
понятно как их использовать.

Функции в примерах: - ${Band.property} заменяется на соответствующее
значение свойства property из бэнда Band - ${IF param1.band.property ==
true} {ELSE} {ENDIF} в документ попадает часть, cоответвенно
вычисленному значению условия на языке groovy, адресация к входным
данным осуществляется от переменной param1, дальше название бэнда,
дальше свойство - Редактирование HTML <#if band.property> <#else> </#if>
- ${FOR items} Повторяющийся текст ${iter.property} {ENDFOR} в документ
добавляются копии повторяющегося текста, ${iter.property} заменяются на
соответствующие значения свойства property элементов списочного бэнда
items, адресация к очередному элементу итератора через происходит через
переменную ${iter} - работа с таблицами требует помещения выражения
##band=Items в левую верхнюю ячейку, дальше в рамках таблицы можно
адресоваться напрямую к свойству, например ${property}

нэйминг файлов должен совпадать (тоже по примеру с шаблонами). вот тут
пояснения на англ -
https://github.com/cuba-platform/yarg/wiki/Quick-start коротко, в
-json.xml описываются “bands”, в шаблоне используются ${band.property}.

В excel шаблонах нужно использовать “named regions” (формулы ->
диспетчер имен). “named regions” должны быть одноименны с “bands”, их
последовательность должна совпадать.

.. _плагины-1:

Плагины
~~~~~~~

Для предоставления данных используются плагины. Для примера есть sample
и echo plugin. Эхо плагин возвращает ровно то, что получил на вход.
Полезен для тестирования. Пример вызова сервиса есть в тестах
rbip-service.

Типовой процесс добавления нового плагина выглядит следующим образом. 1)
Создаем в корневом проекте common-api-doc-template новый модуль
rbip-plugin-new, прописываем его в pom.xml. 2) В новом модуле создаем
класс, реализующий интерфейс AbstractRBIPPlugin из модуля rbip-api. 3) В
нем метод getModel должен отвечать за получение данных. Реализация тут
на усмотрение разработчика. 4) Добавляем в определение плагина
getPluginDefinition() его название (например .pluginName(“NEW”)) и
список принимаемых параметров (например .paramName(“orderId”)). 5)
Добавляем получение тестовой модели getModelExample(). 6) В модуль
rbip-service добавляем зависимость rbip-plugin-new. 7) Собираем,
деплоим, передаем название плагина и параметры из пункта 4.

Пример application.yml
~~~~~~~~~~~~~~~~~~~~~~

.. code:: yaml

   rbip:
     templateLocation: './templates'

   app:
     elasticSearchBaseUrl: http://localhost:9200
     userInfoServiceBaseUrl: http://localhost:42000/api/user-profile
     sequenceBaseUrl: http://localhost:42000/api/sequence
     limitServiceBaseUrl: http://localhost:8080/api/limit
     pdfExportBaseUrl: http://localhost:8080/api/pdfExport


   pdfExport:
     calcPagesCount: false

-  ``pdfExport.calcPagesCount`` - включение подсчета страниц в
   docx-документе через сервис конвертации в PDF.

API
~~~

Для вызова доступны точки: /upload - принимает файл шаблона “file”,
загружает его в каталог с шаблонами и добавляет запись о нем в
стандартный конфигурационный файл. /form - принимает название плагина
“p”, параметры для работы плагина “pd”, название шаблона “template”,
возвращает заполненный шаблон.
/form/{dataProviderId}/{docTemplate}?orderId={orderId} примеры
использования: - /form/BG/contract.docx?orderId=AWM5_06E0JOiIHBbYQZ6 -
/form/bgStatsJournal/bg_statsJournal.xlsx?dateFrom=2020-07-01&dateTo=2020-07-10
