Bg Api Cbs
==========

Сервис интеграции с АБС. ## Назначение Сервис предназначен для экспорта
документов в специализированные службы банка, принимающие документы по
протоколу SOAP. Иными словами, сервис принимает на вход набор параметров
и, преобразовав согласно описанной ниже логике, перенаправляет в сервис
банка. ## Используемые фреймворки - Spring Boot - Micrometer application
monitoring - SpringFox - Project Lombok ## Методы сервиса ###
cbs-integration-rest-controller #### ### mock-controller #### Создание
компании ##### Формат запроса Метод предназначен для обработки компании
по номеру заказа \| Тип запроса \| endPoint \| \|–|–\| \| POST \|
/company/{orderId} \| Дополнительных параметров не предусмотрено #####
Коды ответа \| Статус \| Описание \| \|–|–\| \| 200 \| Запрос принят и
обработан успешно \| \| 401 \| Ошибка авторизации \| \| 403 \| Запрос
запрещен \| \| 404 \| Сервис не найден \| ##### Формат запроса в CBS

::

   Дима: DataProviderCbs.java
   1. В комментариях присутствуют методы, возвращающие значения параметров, например, getShortName(). Предполагаю, что это дергается БД для получения конкретного значения соответствующего параметра. Верно?

/createFirm

.. code:: xml

           <createFirmInput>
               <ApplicantFirmLoad>
                  <ClientInfoFirm>
                     <ClientClass>?</ClientClass> //Juridical или Individual.
                     <ShortName>?</ShortName> //company.getShortName()
                     <!--Optional:-->
                     <Name>?</Name> //company.getFullName()
                     <INN>?</INN> //company.getINN()
                     <!--Optional:-->
                     <PersonId>?</PersonId> //relations.get(0).getLeftId() Выводить главу компании "person", personId, PERSON_CBS_ID_ATTR. Если есть ошибки, выводить тип ошибки (No companyHead found for individual company: " + companyId, "Индивидуальный предприниматель %s", company.getFullName())
                     <CountryCode>?</CountryCode> //всегда 643
                     <ClientType>?</ClientType> // Тип клиента из таблицы /clientTypes.md Если пусто, ставится текст "clientType is empty for company: " + companyId
                     <Source>?</Source> //всегда 10900000009L
                  </ClientInfoFirm>
               </ApplicantFirmLoad>
               <ApplicationFirmLoad>
                  <!--Optional:-->
                  <Branch>?</Branch> //setBranch(integrationConfig.getBranch())
                  <!--Optional:-->
                  <Staffer>?</Staffer> //setStaffer(integrationConfig.getStafferPrincipal())
               </ApplicationFirmLoad>
               <SourceGUID>?</SourceGUID>
               <!--Optional:-->
               <ExternalSource>?</ExternalSource> //setSourceGUID(integrationConfig.getSourceGuid())
            </createFirmInput>

Диаграмма последовательностей
'''''''''''''''''''''''''''''

.. code:: sequence

     Note right of client: /purchases/document
     client->BGAPISBS: POST request
     Note left of BGAPISBS: /company/{orderId}
     BGAPISBS->ElasticSearch: POST request
     Note left of ElasticSearch: запрос данных по компании
     ElasticSearch->BGAPISBS: response
     BGAPISBS->CBS: SOAP
     Note left of CBS:  /createFirm
     CBS->BGAPISBS: Response
     BGAPISBS->client: Response

Отправить документы компании
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Формат запроса
''''''''''''''

Метод предназначен для прикрепления документов компании по номеру заказа
\| Тип запроса \| endPoint \| \|–|–\| \| POST \| /company/{orderId}/docs
\| Дополнительных параметров не предусмотрено ##### Коды ответа \|
Статус \| Описание \| \|–|–\| \| 200 \| Запрос принят и обработан
успешно \| \| 401 \| Ошибка авторизации \| \| 403 \| Запрос запрещен \|
\| 404 \| Сервис не найден \| ##### Формат запроса в CBS Сервис по
параметру orderId загружает из БД массив документов, относящихся к
заказу и формирует последовательность запросов по протоколу SOAP. #####
Диаграмма последовательностей

.. code:: sequence

     Note right of client: /company/{orderId}/docs
     client->BGAPISBS: POST request
     Note left of BGAPISBS: /company/{orderId}
     BGAPISBS->ElasticSearch: POST request
     Note left of ElasticSearch: запрос документов с параметрами
     ElasticSearch->BGAPISBS: response
     BGAPISBS->CBS: SOAP
     Note left of CBS:  /startLoading (запрос loadId)
     CBS->BGAPISBS: Response + loadId
       BGAPISBS->CBS: SOAP
     Note left of CBS:  /addFileToLoad (загрузка документов)
     CBS->BGAPISBS: Response
     BGAPISBS->CBS: SOAP
     Note left of CBS:  /finishLoading (информирование о завершении)
     CBS->BGAPISBS: Response  
     BGAPISBS->client: Response

Подготовка документов
                     

По параметру orderId поисходит подготовка DTO с массивом документов,
относящихся к заказу: \| Параметр \| Описание \| \|–|–\| \| loadUID \|
ID загрузки (значение добавится на шаге 2) \| \| sourceGUID \| ID
источника загрузки \| \| fileName \| Название файла + расширение \| \|
clientHash \| cbsCompanyId \| \| CategoryID \| cbsCategoryID \| \|
fileContent \| Содержимое файла \|

Подготовка к загрузке в CBS (startLoading)
                                          

/EADocumentLoad/EADocumentLoadService Метод на стороне CBS,
предназначенный для выдачи ID загрузки (loadUID)

.. code:: xml

      <soapenv:Body>
         <ws:startLoading>
            <!--Optional:-->
            <startLoadRequest>
               <SourceGUID>A711CD36BACA9057F4EED8A4CCC539EB</SourceGUID> // Предустановленное значение
               <UserGUID>785BE6DD-25E6-4E2A-9177-7860162CB456</UserGUID> // Предустановленное значение
               <ValidationSchemaName>БАНКОВСКАЯ ГАРАНТИЯ ТТ ТЕСТ1</ValidationSchemaName> // Предустановленное значение
            </startLoadRequest>
         </ws:startLoading>
      </soapenv:Body>

В ответ ожидается приём значения loadUID ###### Загрузка файла
(addFileToLoad) /EADocumentLoad/EADocumentLoadService Метод на стороне
CBS, предназначенный для непосредственного экспорта документов.

.. code:: xml

      <soapenv:Body>
         <ws:addFileToLoad>
            <!--Optional:-->
            <addFileToLoadRequest>
               <SourceGUID>A711CD36BACA9057F4EED8A4CCC539EB</SourceGUID> //
               <LoadGUID>?</LoadGUID> //ID загрузки, параметр, принятый из предущего шага
   | clientHash | cbsCompanyId |
               <ClientHash>?</ClientHash> //
               <!--Optional:-->
               <AgreementHash>?</AgreementHash> //null
               <CategoryID>?</CategoryID> //cbsCategoryID
               <FileName>?</FileName> //Название файла с расширением
               <File>?</File> Содержимой файла [Byte]
            </addFileToLoadRequest>
         </ws:addFileToLoad>
      </soapenv:Body>

Завершение загрузки (finishLoading)
                                   

/EADocumentLoad/EADocumentLoadService Метод на стороне CBS,
предназначенный для информирования CBS о завершении процесса загрузки
документов по LoadGUID

.. code:: xml

      <soapenv:Body>
         <ws:finishLoading>
            <!--Optional:-->
            <finishLoadingRequest>
               <SourceGUID>A711CD36BACA9057F4EED8A4CCC539EB</SourceGUID> //ID источника (предустановлен)
               <LoadGUID>?</LoadGUID> //ID загрузки
            </finishLoadingRequest>
         </ws:finishLoading>
      </soapenv:Body>

Отправить банковскую гарантию
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _формат-запроса-1:

Формат запроса
''''''''''''''

Метод предназначен для загрузки выданных гарантий в CBS по номеру заказа
\| Тип запроса \| endPoint \| \|–|–\| \| POST \| /guarantee/{orderId} \|
Дополнительных параметров не предусмотрено ##### Коды ответа \| Статус
\| Описание \| \|–|–\| \| 200 \| Запрос принят и обработан успешно \| \|
401 \| Ошибка авторизации \| \| 403 \| Запрос запрещен \| \| 404 \|
Сервис не найден \| ##### Диаграмма последовательностей

.. code:: sequence

     Note right of client: /guarantee/{orderId}
     client->BGAPISBS: POST request
     BGAPISBS->ElasticSearch: POST requests
     Note left of ElasticSearch: Запросы для формирования guarantee.xml
     ElasticSearch->BGAPISBS: responses
     BGAPISBS->CBS: SOAP
     Note left of CBS:  /getPackageId (запрос getPackageId)
     CBS->BGAPISBS: Response + packageId
     BGAPISBS->CBS: SOAP
     Note left of CBS:  /uploadPackage (загрузка документов)
     CBS->BGAPISBS: Response
     BGAPISBS->CBS: SOAP
     Note left of CBS:  /uploadPackageAttachments (Загрузка прикреплённых документов)
     CBS->BGAPISBS: Response  
     BGAPISBS->CBS: SOAP
     Note left of CBS:  /getResult (Запрос результата загрузки)
     CBS->BGAPISBS: Response  
     BGAPISBS->client: Response  

Расчет агентских комиссий
                         

По параметру orderId из БД выводится список гарантий, их общий номинал.
По каждому лоту требуется прикрепить собственную агентскую комиссию
таким образом, чтобы их общая сумма не отличалась от поленной. Это
делается следующим образом:

::

   1. Общая сумма агентских комиссий делится поровну между всеми заявками
   2. Для всех заявок, кроме первой происходит округление "вниз"
   3. Размер комиссии для первой заявки вычисляется как разность между общей комиссией и суммой остальных

Запросить packageId
                   

Метод предназначен для получения packageId, используемого в последующих
обращениях к CBS в рамках работы службы загрузки гарантий.

.. code:: xml

      <soapenv:Body>
         <ws:getPackageId>
            <!--Optional:-->
            <vendorId>946355003</vendorId> //Константа
            <!--Optional:-->
            <headerTypeID>21</headerTypeID> //Константа
         </ws:getPackageId>
      </soapenv:Body>

Загрузить пакет документов
                          

Метод предназначен для загрузки пакета банковских гарантий.

.. code:: xml

     <soapenv:Body>
         <ws:uploadPackage>
            <!--Optional:-->
            <pkgId>?</pkgId> //ID загрузки. Берётся из предыдущего шага
            <!--Optional:-->
            <xmlFilesList>
               <!--Zero or more repetitions:-->
               <xmlFile>
                  <!--Optional:-->
                  <fileContent>?</fileContent> //Содержимое прикреплённого guarantee.xml
                  <!--Optional:-->
                  <fileName>guarantee.xml</fileName> //Константа
                  <!--Optional:-->
                  <typeXmlfile>122</typeXmlfile> //Константа
               </xmlFile>
            </xmlFilesList>
         </ws:uploadPackage>
      </soapenv:Body>

Со схемой файла guarantee.xml можно ознакомиться из файла
`offerForm.xsd <src\main\resources\xsd\offer\offerForm.xsd>`__. В
процессе подготовки загрузочного файла происходят обращения к БД. С
форматом DTO и вызовами к БД можно ознакомиться из файла в форматах
`xlsx <readMeAttachedFiles\guaranteeDTO.xlsx>`__ или
`markdown <readMeAttachedFiles\guaranteeDTO.md>`__ ###### Загрузить
прикрепленные к гарантиям файлы Метод предназначен для загрузки
прикрепленных к гарантиям файлов.

.. code:: xml

      <soapenv:Body>
         <ws:uploadPackageAttachments>
            <!--Optional:-->
            <pkgId>?</pkgId> //ID загрузки
            <!--Optional:-->
            <attachmentFile>
               <fileName>?</fileName>  //file.getName()
               <categoryName>?</categoryName> //file.getType().value()
               <xmlFileTypeId>122</xmlFileTypeId> //Константа
               <!--Optional:-->
               <data>cid:598419123471</data> //Файл
            </attachmentFile>
         </ws:uploadPackageAttachments>
      </soapenv:Body>

Получить результат загрузки
                           

Метод требуется для получения результата загрузки гарантий и аттача по
параметру pkgId.

.. code:: xml

      <soapenv:Body>
         <ws:getResult>
            <!--Optional:-->
            <pkgId>?</pkgId> //ID загрузки
         </ws:getResult>
      </soapenv:Body>

Отправить участников
^^^^^^^^^^^^^^^^^^^^

.. _формат-запроса-2:

Формат запроса
''''''''''''''

Метод предназначен для экспорта информации об участниках сделки \| Тип
запроса \| endPoint \| \|–|–\| \| POST \| /persons/{orderId} \|
Дополнительных параметров не предусмотрено ##### Коды ответа \| Статус
\| Описание \| \|–|–\| \| 200 \| Запрос принят и обработан успешно \| \|
401 \| Ошибка авторизации \| \| 403 \| Запрос запрещен \| \| 404 \|
Сервис не найден \| ##### Диаграмма последовательностей

.. code:: sequence

     Note right of client: /persons/{orderId}
     client->BGAPISBS: POST request
     BGAPISBS->ElasticSearch: POST requests
     Note left of ElasticSearch: Запросы для формирования DTO
     ElasticSearch->BGAPISBS: responses
     BGAPISBS->CBS: SOAP
     Note left of CBS:  /PersonLoan
     CBS->BGAPISBS: Response
     BGAPISBS->client: Response  

Формат запроса в CBS
''''''''''''''''''''

Укороченный вид запроса:

.. code:: xml

      <soapenv:Body>
         <ws:PersonLoanRequest>
            <!--Optional:-->
            <PersonLoanInput>
               <ApplicantLoad>
                  <PersonalInformationLoad>
                     <CRMID>?</CRMID>
                     ......
                     <!--Optional:-->
                     <FATCAStatus>?</FATCAStatus>
                  </PersonalInformationLoad>
               </ApplicationLoad>
               <!--Optional:-->
               <SourceGUID>?</SourceGUID>
            </PersonLoanInput>
         </ws:PersonLoanRequest>
      </soapenv:Body>

-  Ознакомиться с полным составом XML можно в файле
   `PersonLoanRequest.xml <readMeAttachedFiles\PersonLoanRequest.xml>`__
-  Ознакомиться с описанием полей и вызовов к БД можно в файле
   `personLoan.md <readMeAttachedFiles\personLoan.md>`__ #### Получить
   информацию о платеже ##### Формат запроса Метод предназначен для
   получения информации о платежах в заданном параметрами периоде \| Тип
   запроса \| endPoint \| \|–|–\| \| GET \| /payments \| Параметры: \|
   Параметр \| Тип \| Описание \| \|–|–|–\| \| dateFrom \|
   string(:math:`date) | Начальная дата | | dateFrom | string(`\ date)
   \| Начальная дата \| ##### Формат ответа

.. code:: json

   [
     {
       "amount": 0, //Сумма платежа
       "date": "string", //Дата платежа
       "id": "string", //ID платежа
       "number": "string", //?
       "payerAccount": "string", //Счет плательщика
       "payerBic": "string", //БИК банка плательщика
       "payerInn": "string", //ИНН плательщика
       "payerKpp": "string", //КПП плательщика
       "payerName": "string", //Имя плательщика
       "purpose": "string", //Назначение платежа
       "type": "string" //Тип платежа
     }
   ]

Коды ответа
'''''''''''

====== =================================
Статус Описание
====== =================================
200    Запрос принят и обработан успешно
401    Ошибка авторизации
403    Запрос запрещен
404    Сервис не найден
====== =================================

.. _диаграмма-последовательностей-1:

Диаграмма последовательностей
'''''''''''''''''''''''''''''

.. code:: sequence

     Note right of client: /persons/{orderId}
     client->BGAPISBS: GET request
     BGAPISBS->CBS: SOAP
     Note left of CBS:  /getPaymentDocument
     CBS->BGAPISBS: Response
     BGAPISBS->client: Response  

.. _формат-запроса-в-cbs-1:

Формат запроса в CBS
                    

.. code:: xml

      <soapenv:Body>
         <ws:getPaymentDocument>
            <!--Optional:-->
            <sourceGUID>A711CD36BACA9057F4EED8A4CCC539EB</sourceGUID> //Константа
            <!--Optional:-->
            <beginDate>?</beginDate> //Параметр из запроса клиента
            <!--Optional:-->
            <endDate>?</endDate> //Параметр из запроса клиента
         </ws:getPaymentDocument>
      </soapenv:Body>

Формат ответа из CBS и маппинг в ответ клиенту
                                              

====================== =======================
Параметр из ответа CBS Параметр ответа клиенту
====================== =======================
natValue               amount
docProcessDate         date
payDocId               id
docNum                 number
senderAccountNum       payerAccount
senderBankCode         payerBic
senderInn              payerInn
senderKpp              payerKpp
senderName             payerName
comments               purpose
docFlag                type
====================== =======================

Дополнительная информация
-------------------------

В рамках интеграции с АБС реализован сервис, использующий ``wsdl`` и
``xsd`` банка. Интерфейсы для использования сгенерены автоматически с
помощью ``wsimport``.

Так же была создана примитивная заглушка, эмулирущая вэб-сервис
`MockController <src/main/java/com/farzoom/pear/bg/api/cbs/controller/MockController.java>`__.
При отправке запроса в Mock-режиме на урл http://localhost:8080/ws, в
зависимосте от тела сообщения, будет создан и отправлен ответ
соответствующего типа. Активируется при указании профиля ``mock``.

Добавлена заглушка, эмулирующая SOAP-сервис по экспорту документов
(EADocLoadService). Активируется при указании профиля ``mock``. Доступна
по адресу ``http://localhost:8080/mock-docs``.

В конфиг добавлена секция ``docs-integration`` для настройки интеграции
по документам. В этой секции нужно указать все документы, которые
необходимо экспортировать по компании ``companyDocs`` и по гарантии
``guaranteeDocs``. При этом нужно указать перечень доктайпов (как они
заведены в конфиге сервиса common-api-doc) и соответствующие categoryID
(ID типа документа на стороне электронного архива) для каждого из них -
см. пример ниже.

Build
-----

.. code:: bash

   mvn clean package

Local run
---------

::

   java -Dfile.encoding=utf-8 -jar target/bg-api-cbs.jar

Разработка так же весьма затруднительна без предварительной сборки
проекта

Configuration
-------------

.. code:: yaml

   # установка активного профиля
   spring:
     profiles:
       active: swagger, mock
   # установка параметров старта сервера
   server:
     # порт
     port: 8080
   # указание хоста сваггера
   swagger:
     host: localhost:8080
   # параметры интеграции
   integration:
     # урл вэб-сервиса
     baseUrl: http://localhost:8080/ws
     vendorId: 946355003
     headerTypeID: 21
     xmlFileTypePrincipal: 121
     xmlFileTypeGuarantee: 122
     sourceGuid: "A711CD36BACA9057F4EED8A4CCC539EB"
     stafferGuarantee: "2524860680"
     stafferPrincipal: "785BE6DD-25E6-4E2A-9177-7860162CB456"
     branch: 10300001111
   # параметры интеграции по документам
   docs-integration:
     baseUrl: https://nginx-gate-qa.absolutbank.ru/farzoom_eadl_test/
     sourceGuid: "A711CD36BACA9057F4EED8A4CCC539EB"
     userGuid: "785BE6DD-25E6-4E2A-9177-7860162CB456"
     validationSchemaName: "БАНКОВСКАЯ ГАРАНТИЯ ТТ ТЕСТ1"
     # перечень всех docType-ов, которые нужно экспортировать по компании, с указанием categoryID
     companyDocs:
       dulHead: 0
       charter: 1
     # перечень всех docType-ов, которые нужно экспортировать по гарантии, с указанием categoryID
     guaranteeDocs:
       bgScanLot: 2
       ufkScanLot: 3
   # параметры подключения к common-api-doc
   docapi:
     baseUrl: http://localhost:8081/
   # параметры доступа к elasticsearch
   elasticsearch:
     # урл
     baseUrl: http://localhost:19200
   # параметры подключения к сервису bg-api-agent-billing
   billing:
     baseUrl: http://proxy:8888/api/bg-agent-billing
   # параметры подключения к сервису common-api-user-profile
   user-info:
     baseUrl: http://proxy:8888/api/user-profile
   # логирование запросов к ws
   logging:
     level:
       com.sun.xml:
         ws.transport.http: TRACE
         internal.ws.transport.http: TRACE

TODO
----

1. На данный момент, неоходимо для некоторых классов указывать
   ``@XmlRootElement``
