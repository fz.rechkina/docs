Documents Service
=================

Описание
--------

Сервис реализует API для работы с файлами.

Ранее в рамках проекта БГ был реализован сервис **docapi** на **NodeJS**
по работе с файлами в **SharePoint**. Данный сервис повторяет по
возможности API **docapi** для замены в системе **MOOS**, вместо
хранения файлов в **SharePoint** реализуется хранение файлов в обычной
файловой системе.

Для дополнительной информации о файлах используется база данных **H2**.
Есть возможность использовать другую реляционную СУБД.

В реализации сервиса использованы:

-  Spring Boot
-  Maven
-  Groovy
-  JPA
-  H2
-  Springfox Swagger

Предоставляемые API
-------------------

Определения
~~~~~~~~~~~

DocFileDetachedSignature
^^^^^^^^^^^^^^^^^^^^^^^^

============= ====== ========
Параметр      Тип    Описание
============= ====== ========
fileId        string ``???``
id            string ``???``
path          string ``???``
signatureInfo string ``???``
signatureType string ``???``
============= ====== ========

File
^^^^

============= ================ ==============================
Параметр      Тип              Описание
============= ================ ==============================
absolute      boolean          ``???``
absoluteFile  `File <#File>`__ `Описание типа File <#File>`__
absolutePath  string           ``???``
canonicalFile `File <#File>`__ `Описание типа File <#File>`__
canonicalPath string           ``???``
directory     boolean          ``???``
executable    boolean          ``???``
file          boolean          ``???``
freeSpace     integer (int64)  ``???``
hidden        boolean          ``???``
lastModified  integer (int64)  ``???``
name          string           ``???``
parent        string           ``???``
parentFile    `File <#File>`__ `Описание типа File <#File>`__
path          string           ``???``
readable      boolean          ``???``
totalSpace    integer (int64)  ``???``
usableSpace   integer (int64)  ``???``
writable      boolean          ``???``
============= ================ ==============================

InputStream
^^^^^^^^^^^

======== === ========
Параметр Тип Описание
======== === ========
======== === ========

Resource
^^^^^^^^

+---------+-----------------------+------------------------------------+
| П       | Тип                   | Описание                           |
| араметр |                       |                                    |
+=========+=======================+====================================+
| desc    | string                | ``???``                            |
| ription |                       |                                    |
+---------+-----------------------+------------------------------------+
| file    | `File <#File>`__      | `Описание типа File <#File>`__     |
+---------+-----------------------+------------------------------------+
| f       | string                | ``???``                            |
| ilename |                       |                                    |
+---------+-----------------------+------------------------------------+
| inpu    | `InputStr             | `Описание типа                     |
| tStream | eam <#InputStream>`__ | InputStream <#InputStream>`__      |
+---------+-----------------------+------------------------------------+
| open    | boolean               | ``???``                            |
+---------+-----------------------+------------------------------------+
| r       | boolean               | ``???``                            |
| eadable |                       |                                    |
+---------+-----------------------+------------------------------------+
| uri     | `URI <#URI>`__        | `Описание типа URI <#URI>`__       |
+---------+-----------------------+------------------------------------+
| url     | `URL <#URL>`__        | `Описание типа URL <#URL>`__       |
+---------+-----------------------+------------------------------------+

URI
^^^

===================== =============== ========
Параметр              Тип             Описание
===================== =============== ========
absolute              boolean         ``???``
authority             string          ``???``
fragment              string          ``???``
host                  string          ``???``
opaque                boolean         ``???``
path                  string          ``???``
port                  integer (int32) ``???``
query                 string          ``???``
rawAuthority          string          ``???``
rawFragment           string          ``???``
rawPath               string          ``???``
rawQuery              string          ``???``
rawSchemeSpecificPart string          ``???``
rawUserInfo           string          ``???``
scheme                string          ``???``
schemeSpecificPart    string          ``???``
userInfo              string          ``???``
===================== =============== ========

URL
^^^

+-----------+------------------------+---------------------------------+
| Параметр  | Тип                    | Описание                        |
+===========+========================+=================================+
| authority | string                 | ``???``                         |
+-----------+------------------------+---------------------------------+
| content   | object                 | ``???``                         |
+-----------+------------------------+---------------------------------+
| de        | integer (int32)        | ``???``                         |
| faultPort |                        |                                 |
+-----------+------------------------+---------------------------------+
| deseriali | `URLStreamHandler      | `Описание типа                  |
| zedFields | <#URLStreamHandler>`__ | URLStrea                        |
|           |                        | mHandler <#URLStreamHandler>`__ |
+-----------+------------------------+---------------------------------+
| file      | string                 | ``???``                         |
+-----------+------------------------+---------------------------------+
| host      | string                 | ``???``                         |
+-----------+------------------------+---------------------------------+
| path      | string                 | ``???``                         |
+-----------+------------------------+---------------------------------+
| port      | integer (int32)        | ``???``                         |
+-----------+------------------------+---------------------------------+
| protocol  | string                 | ``???``                         |
+-----------+------------------------+---------------------------------+
| query     | string                 | ``???``                         |
+-----------+------------------------+---------------------------------+
| ref       | string                 | ``???``                         |
+-----------+------------------------+---------------------------------+
| serialize | integer (int32)        | ``???``                         |
| dHashCode |                        |                                 |
+-----------+------------------------+---------------------------------+
| userInfo  | string                 | ``???``                         |
+-----------+------------------------+---------------------------------+

URLStreamHandler
^^^^^^^^^^^^^^^^

======== === ========
Параметр Тип Описание
======== === ========
======== === ========

Методы
------

``/``
~~~~~

``GET /``
^^^^^^^^^

root

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Client->>API: GET /
      API->>PostgreSQL: 
      Note left of Client: Получение root
      PostgreSQL->>API: 
      API->>Client: ???

Ответ
'''''

``/checkAttachedSignature/{fileId}``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``POST /checkAttachedSignature/{fileId}``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Проверить подпись

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Note over Client: Клиент формирует {fileId}

      Client->>API: POST /checkAttachedSignature/{fileId}
      API->>PostgreSQL: 
      Note left of Client: Создание Проверить подпись
      PostgreSQL->>API: 
      API->>Client: Подтверждение

======== ============ ============ ====== =============
Параметр Обязательный Содержится в Тип    Описание
======== ============ ============ ====== =============
fileId   Нет          path         string Идентификатор
======== ============ ============ ====== =============

.. _ответ-1:

Ответ
'''''

``/commitDraft/{entityId}/{docType}``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``POST /commitDraft/{entityId}/{docType}``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Коммит черновика файла

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Note over Client: Клиент формирует {docType}

      Client->>API: POST /commitDraft/{entityId}/{docType}
      API->>PostgreSQL: 
      Note left of Client: Создание Коммит черновика файла
      PostgreSQL->>API: 
      API->>Client: Подтверждение

+--------+-------------+-------------+------+-------------------------+
| Па     | О           | Содержится  | Тип  | Описание                |
| раметр | бязательный | в           |      |                         |
+========+=============+=============+======+=========================+
| d      | Нет         | path        | st   | Тип документа           |
| ocType |             |             | ring |                         |
+--------+-------------+-------------+------+-------------------------+
| en     | Нет         | path        | st   | Идентификатор сущности  |
| tityId |             |             | ring |                         |
+--------+-------------+-------------+------+-------------------------+

.. _ответ-2:

Ответ
'''''

``/content/{fileId}``
~~~~~~~~~~~~~~~~~~~~~

``GET /content/{fileId}``
^^^^^^^^^^^^^^^^^^^^^^^^^

Получение контента файла

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Client->>API: GET /content/{fileId}
      API->>PostgreSQL: 
      Note left of Client: Получение Получение контента файла
      PostgreSQL->>API: 
      API->>Client: ???

======== ============ ============ ====== =============
Параметр Обязательный Содержится в Тип    Описание
======== ============ ============ ====== =============
fileId   Нет          path         string Идентификатор
======== ============ ============ ====== =============

.. _ответ-3:

Ответ
'''''

``/copy``
~~~~~~~~~

``POST /copy``
^^^^^^^^^^^^^^

Копирование файлов из одного раздела в дргой

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Note over Client: Клиент формирует copy

      Client->>API: POST /copy
      API->>PostgreSQL: 
      Note left of Client: Создание Копирование файлов из одного раздела в дргой
      PostgreSQL->>API: 
      API->>Client: Подтверждение

+-------------+-------------+-------------+------+---------------------+
| Параметр    | О           | Содержится  | Тип  | Описание            |
|             | бязательный | в           |      |                     |
+=============+=============+=============+======+=====================+
| fileId      | Нет         | query       | st   | Идентификатор файла |
|             |             |             | ring |                     |
+-------------+-------------+-------------+------+---------------------+
| fromDocType | Нет         | query       | st   | Из типа документа   |
|             |             |             | ring |                     |
+-------------+-------------+-------------+------+---------------------+
| f           | Нет         | query       | st   | Из сущности         |
| romEntityId |             |             | ring |                     |
+-------------+-------------+-------------+------+---------------------+
| toDocType   | Нет         | query       | st   | В тип документа     |
|             |             |             | ring |                     |
+-------------+-------------+-------------+------+---------------------+
| toEntityId  | Нет         | query       | st   | В сущность          |
|             |             |             | ring |                     |
+-------------+-------------+-------------+------+---------------------+

.. _ответ-4:

Ответ
'''''

``/delete/{fileId}``
~~~~~~~~~~~~~~~~~~~~

``POST /delete/{fileId}``
^^^^^^^^^^^^^^^^^^^^^^^^^

Удаление файла

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Note over Client: Клиент формирует {fileId}

      Client->>API: POST /delete/{fileId}
      API->>PostgreSQL: 
      Note left of Client: Создание Удаление файла
      PostgreSQL->>API: 
      API->>Client: Подтверждение

======== ============ ============ ====== =============
Параметр Обязательный Содержится в Тип    Описание
======== ============ ============ ====== =============
fileId   Нет          path         string Идентификатор
======== ============ ============ ====== =============

.. _ответ-5:

Ответ
'''''

``/deleteDraft/{entityId}/{docType}``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``POST /deleteDraft/{entityId}/{docType}``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Удаление черновика файла

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Note over Client: Клиент формирует {docType}

      Client->>API: POST /deleteDraft/{entityId}/{docType}
      API->>PostgreSQL: 
      Note left of Client: Создание Удаление черновика файла
      PostgreSQL->>API: 
      API->>Client: Подтверждение

+--------+-------------+-------------+------+-------------------------+
| Па     | О           | Содержится  | Тип  | Описание                |
| раметр | бязательный | в           |      |                         |
+========+=============+=============+======+=========================+
| d      | Нет         | path        | st   | Тип документа           |
| ocType |             |             | ring |                         |
+--------+-------------+-------------+------+-------------------------+
| en     | Нет         | path        | st   | Идентификатор сущности  |
| tityId |             |             | ring |                         |
+--------+-------------+-------------+------+-------------------------+

.. _ответ-6:

Ответ
'''''

``/detachedSignature``
~~~~~~~~~~~~~~~~~~~~~~

``GET /detachedSignature``
^^^^^^^^^^^^^^^^^^^^^^^^^^

Получение списка открепленных подписей

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Client->>API: GET /detachedSignature
      API->>PostgreSQL: 
      Note left of Client: Получение Получение списка открепленных подписей
      PostgreSQL->>API: 
      API->>Client: ???

+------+----------+----------+----+-----------------------------------+
| Пара | Обяз     | Со       | Т  | Описание                          |
| метр | ательный | держится | ип |                                   |
|      |          | в        |    |                                   |
+======+==========+==========+====+===================================+
| fi   | Нет      | query    | st | ID файла, которому соответствует  |
| leId |          |          | ri | подпись                           |
|      |          |          | ng |                                   |
+------+----------+----------+----+-----------------------------------+

.. _ответ-7:

Ответ
'''''

`Описание типа DocFileDetachedSignature <#DocFileDetachedSignature>`__

``POST /detachedSignature``
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Загрузка открепленной подписи

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Note over Client: Клиент формирует detachedSignature

      Client->>API: POST /detachedSignature
      API->>PostgreSQL: 
      Note left of Client: Создание Загрузка открепленной подписи
      PostgreSQL->>API: 
      API->>Client: Подтверждение

+----------+---------+---------+----+---------------------------------+
| Параметр | Обяза   | Сод     | Т  | Описание                        |
|          | тельный | ержится | ип |                                 |
|          |         | в       |    |                                 |
+==========+=========+=========+====+=================================+
| fileId   | Нет     | query   | st | ID файла, которому              |
|          |         |         | ri | соответствует подпись           |
|          |         |         | ng |                                 |
+----------+---------+---------+----+---------------------------------+
| signa    | Нет     | query   | st | Тип подписи                     |
| tureType |         |         | ri |                                 |
|          |         |         | ng |                                 |
+----------+---------+---------+----+---------------------------------+

.. _ответ-8:

Ответ
'''''

`Описание типа DocFileDetachedSignature <#DocFileDetachedSignature>`__

``/detachedSignatureFile``
~~~~~~~~~~~~~~~~~~~~~~~~~~

``GET /detachedSignatureFile``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Скачиваиние файла открепленной подписи

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Client->>API: GET /detachedSignatureFile
      API->>PostgreSQL: 
      Note left of Client: Получение Скачиваиние файла открепленной подписи
      PostgreSQL->>API: 
      API->>Client: ???

+--------+---------+---------+----+----------------------------------+
| Па     | Обяза   | Сод     | Т  | Описание                         |
| раметр | тельный | ержится | ип |                                  |
|        |         | в       |    |                                  |
+========+=========+=========+====+==================================+
| fileId | Нет     | query   | st | ID файла, которому соответствует |
|        |         |         | ri | подпись                          |
|        |         |         | ng |                                  |
+--------+---------+---------+----+----------------------------------+
| signa  | Нет     | query   | st | ID подписи                       |
| tureId |         |         | ri |                                  |
|        |         |         | ng |                                  |
+--------+---------+---------+----+----------------------------------+

.. _ответ-9:

Ответ
'''''

``/detachedSignatureInfo``
~~~~~~~~~~~~~~~~~~~~~~~~~~

``POST /detachedSignatureInfo``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Загрузка результата проверки SVS файла открепленной подписи

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Note over Client: Клиент формирует detachedSignatureInfo

      Client->>API: POST /detachedSignatureInfo
      API->>PostgreSQL: 
      Note left of Client: Создание Загрузка результата проверки SVS файла открепленной подписи
      PostgreSQL->>API: 
      API->>Client: Подтверждение

+--------+---------+---------+----+----------------------------------+
| Па     | Обяза   | Сод     | Т  | Описание                         |
| раметр | тельный | ержится | ип |                                  |
|        |         | в       |    |                                  |
+========+=========+=========+====+==================================+
| fileId | Нет     | query   | st | ID файла, которому соответствует |
|        |         |         | ri | подпись                          |
|        |         |         | ng |                                  |
+--------+---------+---------+----+----------------------------------+
| signa  | Нет     | query   | st | ID подписи                       |
| tureId |         |         | ri |                                  |
|        |         |         | ng |                                  |
+--------+---------+---------+----+----------------------------------+

.. _ответ-10:

Ответ
'''''

``/download/{fileId}``
~~~~~~~~~~~~~~~~~~~~~~

``GET /download/{fileId}``
^^^^^^^^^^^^^^^^^^^^^^^^^^

Получение файла

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Client->>API: GET /download/{fileId}
      API->>PostgreSQL: 
      Note left of Client: Получение Получение файла
      PostgreSQL->>API: 
      API->>Client: ???

======== ============ ============ ====== =============
Параметр Обязательный Содержится в Тип    Описание
======== ============ ============ ====== =============
fileId   Нет          path         string Идентификатор
======== ============ ============ ====== =============

.. _ответ-11:

Ответ
'''''

``/download/{fileId}/sig-content``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``GET /download/{fileId}/sig-content``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Получение подписи файла

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Client->>API: GET /download/{fileId}/sig-content
      API->>PostgreSQL: 
      Note left of Client: Получение Получение подписи файла
      PostgreSQL->>API: 
      API->>Client: ???

======== ============ ============ ====== =============
Параметр Обязательный Содержится в Тип    Описание
======== ============ ============ ====== =============
fileId   Нет          path         string Идентификатор
======== ============ ============ ====== =============

.. _ответ-12:

Ответ
'''''

``/getDocTypes``
~~~~~~~~~~~~~~~~

``GET /getDocTypes``
^^^^^^^^^^^^^^^^^^^^

Получение типов документов

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Client->>API: GET /getDocTypes
      API->>PostgreSQL: 
      Note left of Client: Получение Получение типов документов
      PostgreSQL->>API: 
      API->>Client: ???

========== ============ ============ ====== ============
Параметр   Обязательный Содержится в Тип    Описание
========== ============ ============ ====== ============
entityType Нет          query        string Тип сущности
q          Нет          query        string Запрос
========== ============ ============ ====== ============

.. _ответ-13:

Ответ
'''''

``/update/{fileId}/signInfo``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``POST /update/{fileId}/signInfo``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Обновление файла

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Note over Client: Клиент формирует signInfo

      Client->>API: POST /update/{fileId}/signInfo
      API->>PostgreSQL: 
      Note left of Client: Создание Обновление файла
      PostgreSQL->>API: 
      API->>Client: Подтверждение

======== ============ ============ ====== ==================
Параметр Обязательный Содержится в Тип    Описание
======== ============ ============ ====== ==================
fileId   Нет          path         string Идентификатор поля
======== ============ ============ ====== ==================

.. _ответ-14:

Ответ
'''''

``/upload/{entityId}/{docType}``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``POST /upload/{entityId}/{docType}``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Загрузка файла

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Note over Client: Клиент формирует {docType}

      Client->>API: POST /upload/{entityId}/{docType}
      API->>PostgreSQL: 
      Note left of Client: Создание Загрузка файла
      PostgreSQL->>API: 
      API->>Client: Подтверждение

+--------+-------------+-------------+------+-------------------------+
| Па     | О           | Содержится  | Тип  | Описание                |
| раметр | бязательный | в           |      |                         |
+========+=============+=============+======+=========================+
| d      | Нет         | path        | st   | Тип документа           |
| ocType |             |             | ring |                         |
+--------+-------------+-------------+------+-------------------------+
| en     | Нет         | path        | st   | Идентификатор сущности  |
| tityId |             |             | ring |                         |
+--------+-------------+-------------+------+-------------------------+
| file   | Нет         | formData    | file | Содержимое файла        |
+--------+-------------+-------------+------+-------------------------+

.. _ответ-15:

Ответ
'''''

``/{entityType}/{entityId}``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``GET /{entityType}/{entityId}``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Получение файлов сущности

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Client->>API: GET /{entityType}/{entityId}
      API->>PostgreSQL: 
      Note left of Client: Получение Получение файлов сущности
      PostgreSQL->>API: 
      API->>Client: ???

+--------+-------+-------+-----------+--------------------------------+
| Па     | Об    | Содер | Тип       | Описание                       |
| раметр | язате | жится |           |                                |
|        | льный | в     |           |                                |
+========+=======+=======+===========+================================+
| c      | Нет   | query | string    | Дата создания, до которой      |
| reated |       |       | (d        | нужно получить сущности        |
| Before |       |       | ate-time) |                                |
+--------+-------+-------+-----------+--------------------------------+
| en     | Нет   | path  | string    | Идентификатор сущности         |
| tityId |       |       |           |                                |
+--------+-------+-------+-----------+--------------------------------+
| enti   | Нет   | path  | string    | Тип сущности                   |
| tyType |       |       |           |                                |
+--------+-------+-------+-----------+--------------------------------+
| ow     | Нет   | query | string    | Идентификатор владельца        |
| ner_id |       |       |           |                                |
+--------+-------+-------+-----------+--------------------------------+
| q      | Нет   | query | string    | Запрос                         |
+--------+-------+-------+-----------+--------------------------------+

.. _ответ-16:

Ответ
'''''

``/{entityType}/{entityId}/archive``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``POST /{entityType}/{entityId}/archive``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Архивация файла

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Note over Client: Клиент формирует archive

      Client->>API: POST /{entityType}/{entityId}/archive
      API->>PostgreSQL: 
      Note left of Client: Создание Архивация файла
      PostgreSQL->>API: 
      API->>Client: Подтверждение

+----------+------------+------------+-----+------------------------+
| Параметр | Об         | Содержится | Тип | Описание               |
|          | язательный | в          |     |                        |
+==========+============+============+=====+========================+
| entityId | Нет        | path       | str | Идентификатор сущности |
|          |            |            | ing |                        |
+----------+------------+------------+-----+------------------------+
| en       | Нет        | path       | str | Тип сущности           |
| tityType |            |            | ing |                        |
+----------+------------+------------+-----+------------------------+

.. _ответ-17:

Ответ
'''''

``/{entityType}/{entityId}/draft``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``GET /{entityType}/{entityId}/draft``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Получение черновиков файлов сущности

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Client->>API: GET /{entityType}/{entityId}/draft
      API->>PostgreSQL: 
      Note left of Client: Получение Получение черновиков файлов сущности
      PostgreSQL->>API: 
      API->>Client: ???

+--------+-------+-------+-----------+--------------------------------+
| Па     | Об    | Содер | Тип       | Описание                       |
| раметр | язате | жится |           |                                |
|        | льный | в     |           |                                |
+========+=======+=======+===========+================================+
| c      | Нет   | query | string    | Дата создания, до которой      |
| reated |       |       | (d        | нужно получить сущности        |
| Before |       |       | ate-time) |                                |
+--------+-------+-------+-----------+--------------------------------+
| en     | Нет   | path  | string    | Идентификатор сущности         |
| tityId |       |       |           |                                |
+--------+-------+-------+-----------+--------------------------------+
| enti   | Нет   | path  | string    | Тип сущности                   |
| tyType |       |       |           |                                |
+--------+-------+-------+-----------+--------------------------------+
| ow     | Нет   | query | string    | Идентификатор владельца        |
| ner_id |       |       |           |                                |
+--------+-------+-------+-----------+--------------------------------+
| q      | Нет   | query | string    | Запрос                         |
+--------+-------+-------+-----------+--------------------------------+

.. _ответ-18:

Ответ
'''''

``/{entityType}/{entityId}/zip``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``GET /{entityType}/{entityId}/zip``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Скачивание архива с файлами сущности

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant API
      participant PostgreSQL

      Client->>API: GET /{entityType}/{entityId}/zip
      API->>PostgreSQL: 
      Note left of Client: Получение Скачивание архива с файлами сущности
      PostgreSQL->>API: 
      API->>Client: ???

+----------+------------+------------+-----+------------------------+
| Параметр | Об         | Содержится | Тип | Описание               |
|          | язательный | в          |     |                        |
+==========+============+============+=====+========================+
| entityId | Нет        | path       | str | Идентификатор сущности |
|          |            |            | ing |                        |
+----------+------------+------------+-----+------------------------+
| en       | Нет        | path       | str | Тип сущности           |
| tityType |            |            | ing |                        |
+----------+------------+------------+-----+------------------------+

.. _ответ-19:

Ответ
'''''

Конфигурация
------------

`additional-spring-configuration-metadata.json </src/main/resources/META-INF/additional-spring-configuration-metadata.json>`__

Конфигурация сервиса осуществляется в файле **application.yml**,
аналогично прочим Spring Boot сервисам.

Параметры специфичные для данного сервиса

.. code:: yaml

   # Base path in file system for storing files
   doc.fs.basePath: .
   # Stored file name pattern. Default '{id}_{name}'
   doc.fs.namePattern: '{id}_{name}'
   # URL to Elasticsearch
   doc.esBaseURL: http://localhost:9200
   # Migration mode
   migrationMode: false
   # URL without scheme for Swagger UI.
   swagger.host: localhost:8080

   #Metrics related configurations
   management:
     endpoint:
       metrics:
         enabled: true
       prometheus:
         enabled: true
     endpoints:
       web:
         exposure:
           include: "*"
     metrics:
       export:
         prometheus:
           enabled: true

Для отправки подписанного файла на проверку подписей в сервис
``common-api-svs`` в application.yml необходимо указать значение
параметра ``svs.url``. Для того, чтобы файл пошел на проверку,
необходимо в теле запроса ``/upload/{entityId}/{docType}`` указать
параметр ``isSigned=true``

ВАЖНО: сервис ожидает, что запросы в сервис ``common-api-svs`` будут
выполняться очень быстро, сопоставимо с отправкой сообщения в очередь.
Поэтому таймауты на запрос в ``svs`` по умолчанию установлены не
большие.

.. code:: yaml

   svs:
     url: http://proxy/api/svs
     # в миллисекундах
     connectTimeout: 5000
     readTimeout: 5000

Остальные параметры как для любого Spring Boot приложения `Ссылка на
документацию <http://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html>`__

Настройка БД:

.. code:: yaml

   spring:
     datasource:
       url: jdbc:postgresql://localhost:5432/docs
       username: docs
       password: docs
       platform: POSTGRESQL
     jpa:
       properties.hibernate.jdbc.lob.non_contextual_creation: true
       hibernate:
         ddl-auto: update

Swagger
~~~~~~~

For enable Swagger activate **swagger** Spring Boot profile in
**application.yml**

.. code:: yaml

   spring:
     profiles:
       active: swagger

ID MIGRATION
~~~~~~~~~~~~

Для изменения колонки ID *в строковую SID* необходимо выполнить
следующий sql

.. code:: sql

   alter table doc_file_ add column  sid_ VARCHAR(32);
   update doc_file_ set sid_=id_;
   alter table doc_file_ drop column id_;
   alter table doc_file_ alter column sid_ set not null ;
   alter table doc_file_ add primary key (sid_);

Upload file example
~~~~~~~~~~~~~~~~~~~

.. code:: json

   {
     "filename": "sample.docx.sig",
     "fileOrder": 1,
     "isSigned": true,
     "content": "LS0tLS1CRUdJTiBDTVMtLS0tLQ0KTUlBR0NTcUdTSWIzRFFFSEFxQ0FNSUFDQVFFeEREQUtCZ1lxaFFNQ0Fna0ZBRENBQmdrcWhraUc5dzBCQndHZw0KZ0NTQUJBWnhkMlZ5ZEhrQUFBQUFBQUNnZ2crZk1JSURHVENDQXNpZ0F3SUJBZ0lURWdBbXZZTXk3cWtFaFpmNQ0KSHdBQUFDYTlnekFJQmdZcWhRTUNBZ013ZnpFak1DRUdDU3FHU0liM0RRRUpBUllVYzNWd2NHOXlkRUJqY25sdw0KZEc5d2NtOHVjblV4Q3pBSkJnTlZCQVlUQWxKVk1ROHdEUVlEVlFRSEV3Wk5iM05qYjNjeEZ6QVZCZ05WQkFvVA0KRGtOU1dWQlVUeTFRVWs4Z1RFeERNU0V3SHdZRFZRUURFeGhEVWxsUVZFOHRVRkpQSUZSbGMzUWdRMlZ1ZEdWeQ0KSURJd0hoY05NVGd3TXpFMk1UTXpOekUwV2hjTk1UZ3dOakUyTVRNME56RTBXakFvTVNZd0pBWURWUVFEREIzUQ0Ka05DNzBMWFF1dEdCMExEUXZkQzAwWUFnMEovUmo5R0MwWXZRdVRCak1Cd0dCaXFGQXdJQ0V6QVNCZ2NxaFFNQw0KQWlRQUJnY3FoUU1DQWg0QkEwTUFCRUFSbUtPSTJ3QUpNSHVQaE82aDJ1Tkx1ZE4vaGJ0KzNyTnVRZE1tUUdKbQ0KREMrUVBTTStRcjdtTHEyZFJkQmFYaE9SOVpQTWwycFo1TGlxLzNFMEEvVG9vNElCY0RDQ0FXd3dEZ1lEVlIwUA0KQVFIL0JBUURBZ1R3TUJNR0ExVWRKUVFNTUFvR0NDc0dBUVVGQndNQ01CMEdBMVVkRGdRV0JCVEpKUXJpVG1mbw0KQURHVTgxQ2duZGZUSTFNNjZ6QWZCZ05WSFNNRUdEQVdnQlFWTVh5d2pScmVadGNWbkVsU2x4Y2t1UUY2Z3pCWg0KQmdOVkhSOEVVakJRTUU2Z1RLQktoa2hvZEhSd09pOHZkR1Z6ZEdOaExtTnllWEIwYjNCeWJ5NXlkUzlEWlhKMA0KUlc1eWIyeHNMME5TV1ZCVVR5MVFVazhsTWpCVVpYTjBKVEl3UTJWdWRHVnlKVEl3TWk1amNtd3dnYWtHQ0NzRw0KQVFVRkJ3RUJCSUdjTUlHWk1HRUdDQ3NHQVFVRkJ6QUNobFZvZEhSd09pOHZkR1Z6ZEdOaExtTnllWEIwYjNCeQ0KYnk1eWRTOURaWEowUlc1eWIyeHNMM1JsYzNRdFkyRXRNakF4TkY5RFVsbFFWRTh0VUZKUEpUSXdWR1Z6ZENVeQ0KTUVObGJuUmxjaVV5TURJdVkzSjBNRFFHQ0NzR0FRVUZCekFCaGlob2RIUndPaTh2ZEdWemRHTmhMbU55ZVhCMA0KYjNCeWJ5NXlkUzl2WTNOd0wyOWpjM0F1YzNKbU1BZ0dCaXFGQXdJQ0F3TkJBSE1yTVVITXArdkJuNnpWWU1SaA0KNW1neWRpRFRiNEhqRzB5UitpaU5MNU1yR3Fyd2hVOHYySXFGNHIrK05mUWM4WGlMNWxrZGM5TWtGbEdRclNKVQ0KdmNJd2dnTWJNSUlDeXFBREFnRUNBaE1TQUNhN0hjWHlkclRXU1pzUkFBQUFKcnNkTUFnR0JpcUZBd0lDQXpCLw0KTVNNd0lRWUpLb1pJaHZjTkFRa0JGaFJ6ZFhCd2IzSjBRR055ZVhCMGIzQnlieTV5ZFRFTE1Ba0dBMVVFQmhNQw0KVWxVeER6QU5CZ05WQkFjVEJrMXZjMk52ZHpFWE1CVUdBMVVFQ2hNT1ExSlpVRlJQTFZCU1R5Qk1URU14SVRBZg0KQmdOVkJBTVRHRU5TV1ZCVVR5MVFVazhnVkdWemRDQkRaVzUwWlhJZ01qQWVGdzB4T0RBek1UWXdPREEwTlRkYQ0KRncweE9EQTJNVFl3T0RFME5UZGFNQ294S0RBbUJnTlZCQU1NSDlDUTBMdlF0ZEM2MFlIUXNOQzkwTFRSZ0NEUQ0KbjlDMTBZRFFzdEdMMExrd1l6QWNCZ1lxaFFNQ0FoTXdFZ1lIS29VREFnSWtBQVlIS29VREFnSWVBUU5EQUFSQQ0KVXdDTmF6bzlrTzFLWDhFdEdEQVd5TUpma1BzTkdNa3B1ejh3VEd1Um5tZ2lpcmtBWkNpZlk3Y3ZhQ3UxdVBhTA0KUTNzTDFhbDlHdUpDUjBjNWRROXF4Nk9DQVhBd2dnRnNNQTRHQTFVZER3RUIvd1FFQXdJRThEQVRCZ05WSFNVRQ0KRERBS0JnZ3JCZ0VGQlFjREFqQWRCZ05WSFE0RUZnUVVYWjdzK3oyZ3RNbkxnZiswOU9TcU5SbForRVV3SHdZRA0KVlIwakJCZ3dGb0FVRlRGOHNJMGEzbWJYRlp4SlVwY1hKTGtCZW9Nd1dRWURWUjBmQkZJd1VEQk9vRXlnU29aSQ0KYUhSMGNEb3ZMM1JsYzNSallTNWpjbmx3ZEc5d2NtOHVjblV2UTJWeWRFVnVjbTlzYkM5RFVsbFFWRTh0VUZKUA0KSlRJd1ZHVnpkQ1V5TUVObGJuUmxjaVV5TURJdVkzSnNNSUdwQmdnckJnRUZCUWNCQVFTQm5EQ0JtVEJoQmdncg0KQmdFRkJRY3dBb1pWYUhSMGNEb3ZMM1JsYzNSallTNWpjbmx3ZEc5d2NtOHVjblV2UTJWeWRFVnVjbTlzYkM5MA0KWlhOMExXTmhMVEl3TVRSZlExSlpVRlJQTFZCU1R5VXlNRlJsYzNRbE1qQkRaVzUwWlhJbE1qQXlMbU55ZERBMA0KQmdnckJnRUZCUWN3QVlZb2FIUjBjRG92TDNSbGMzUmpZUzVqY25sd2RHOXdjbTh1Y25VdmIyTnpjQzl2WTNOdw0KTG5OeVpqQUlCZ1lxaFFNQ0FnTURRUUJsRmMreTJ2V3d0Y0dBU29CWFRGTVlKZnVaUmoxTFNSRUVIMU4wK0czTw0KeFV6VEZBcFp2NDdGU3A4LzVvcnlQVnJVNWZjYkJHMzlQRFdjY0JBdEtCQ09NSUlER3pDQ0FzcWdBd0lCQWdJVA0KRWdBbXV5SXkwbU40aXdRZ2lRQUFBQ2E3SWpBSUJnWXFoUU1DQWdNd2Z6RWpNQ0VHQ1NxR1NJYjNEUUVKQVJZVQ0KYzNWd2NHOXlkRUJqY25sd2RHOXdjbTh1Y25VeEN6QUpCZ05WQkFZVEFsSlZNUTh3RFFZRFZRUUhFd1pOYjNOag0KYjNjeEZ6QVZCZ05WQkFvVERrTlNXVkJVVHkxUVVrOGdURXhETVNFd0h3WURWUVFERXhoRFVsbFFWRTh0VUZKUA0KSUZSbGMzUWdRMlZ1ZEdWeUlESXdIaGNOTVRnd016RTJNRGd3T1RBMFdoY05NVGd3TmpFMk1EZ3hPVEEwV2pBcQ0KTVNnd0pnWURWUVFEREIvUWtOQzcwTFhRdXRHQjBMRFF2ZEMwMFlBZzBKTFJndEMrMFlEUXZ0QzVNR013SEFZRw0KS29VREFnSVRNQklHQnlxRkF3SUNKQUFHQnlxRkF3SUNIZ0VEUXdBRVFHRk9ZUUVhYkpFdHdFTmozdnlzVm5TZw0KaWFELzM3bVpmKzZSUnpzeGxLZ2VTQ1BOL3Y4cVVyb3JOdFNiNHJvWEs4d2NUalRNemVkKzB6ZjkrWGk2N0U2ag0KZ2dGd01JSUJiREFPQmdOVkhROEJBZjhFQkFNQ0JQQXdFd1lEVlIwbEJBd3dDZ1lJS3dZQkJRVUhBd0l3SFFZRA0KVlIwT0JCWUVGRmZpQVJ4Y2VaUlVRQUJxWndES2NrWFVUTFE1TUI4R0ExVWRJd1FZTUJhQUZCVXhmTENOR3Q1bQ0KMXhXY1NWS1hGeVM1QVhxRE1Ga0dBMVVkSHdSU01GQXdUcUJNb0VxR1NHaDBkSEE2THk5MFpYTjBZMkV1WTNKNQ0KY0hSdmNISnZMbkoxTDBObGNuUkZibkp2Ykd3dlExSlpVRlJQTFZCU1R5VXlNRlJsYzNRbE1qQkRaVzUwWlhJbA0KTWpBeUxtTnliRENCcVFZSUt3WUJCUVVIQVFFRWdad3dnWmt3WVFZSUt3WUJCUVVITUFLR1ZXaDBkSEE2THk5MA0KWlhOMFkyRXVZM0o1Y0hSdmNISnZMbkoxTDBObGNuUkZibkp2Ykd3dmRHVnpkQzFqWVMweU1ERTBYME5TV1ZCVQ0KVHkxUVVrOGxNakJVWlhOMEpUSXdRMlZ1ZEdWeUpUSXdNaTVqY25Rd05BWUlLd1lCQlFVSE1BR0dLR2gwZEhBNg0KTHk5MFpYTjBZMkV1WTNKNWNIUnZjSEp2TG5KMUwyOWpjM0F2YjJOemNDNXpjbVl3Q0FZR0tvVURBZ0lEQTBFQQ0KczlXalBWZjhvUFRCRklZSDEvUmplTDFTL21zdFBTOW5LVXdCbWpiL2xFdDR0b0JIemxiMWkxSW5RTGdsdit5MQ0KaGNuUHBpRm02elZqVjBLRnFubDBxakNDQXhzd2dnTEtvQU1DQVFJQ0V4SUFKcjErcitKU0c1bkdlUmdBQUFBbQ0Kdlg0d0NBWUdLb1VEQWdJRE1IOHhJekFoQmdrcWhraUc5dzBCQ1FFV0ZITjFjSEJ2Y25SQVkzSjVjSFJ2Y0hKdg0KTG5KMU1Rc3dDUVlEVlFRR0V3SlNWVEVQTUEwR0ExVUVCeE1HVFc5elkyOTNNUmN3RlFZRFZRUUtFdzVEVWxsUQ0KVkU4dFVGSlBJRXhNUXpFaE1COEdBMVVFQXhNWVExSlpVRlJQTFZCU1R5QlVaWE4wSUVObGJuUmxjaUF5TUI0WA0KRFRFNE1ETXhOakV6TXpNd05Gb1hEVEU0TURZeE5qRXpORE13TkZvd0tqRW9NQ1lHQTFVRUF3d2YwSkRRdTlDMQ0KMExyUmdkQ3cwTDNRdE5HQUlOQ2kwWURRdGRHQzBMalF1VEJqTUJ3R0JpcUZBd0lDRXpBU0JnY3FoUU1DQWlRQQ0KQmdjcWhRTUNBaDRCQTBNQUJFQ2cyL3RlRjBEbFdJZHdtalhmZDdDaitnUEFnWnVjaEEyS3dsRUJKTWRNRC9YZA0KWnlTQkhFTDJPV2lUKzMyb1lLUlRDc3NDTkUzbXB3YWVsM1N3VURYdm80SUJjRENDQVd3d0RnWURWUjBQQVFILw0KQkFRREFnVHdNQk1HQTFVZEpRUU1NQW9HQ0NzR0FRVUZCd01DTUIwR0ExVWREZ1FXQkJUV01ZVEpMSmh5Y0t4TQ0KNXFjOTcwZ3pNTG5xNlRBZkJnTlZIU01FR0RBV2dCUVZNWHl3alJyZVp0Y1ZuRWxTbHhja3VRRjZnekJaQmdOVg0KSFI4RVVqQlFNRTZnVEtCS2hraG9kSFJ3T2k4dmRHVnpkR05oTG1OeWVYQjBiM0J5Ynk1eWRTOURaWEowUlc1eQ0KYjJ4c0wwTlNXVkJVVHkxUVVrOGxNakJVWlhOMEpUSXdRMlZ1ZEdWeUpUSXdNaTVqY213d2dha0dDQ3NHQVFVRg0KQndFQkJJR2NNSUdaTUdFR0NDc0dBUVVGQnpBQ2hsVm9kSFJ3T2k4dmRHVnpkR05oTG1OeWVYQjBiM0J5Ynk1eQ0KZFM5RFpYSjBSVzV5YjJ4c0wzUmxjM1F0WTJFdE1qQXhORjlEVWxsUVZFOHRVRkpQSlRJd1ZHVnpkQ1V5TUVObA0KYm5SbGNpVXlNREl1WTNKME1EUUdDQ3NHQVFVRkJ6QUJoaWhvZEhSd09pOHZkR1Z6ZEdOaExtTnllWEIwYjNCeQ0KYnk1eWRTOXZZM053TDI5amMzQXVjM0ptTUFnR0JpcUZBd0lDQXdOQkFHQlF4Z3VMOFU5eXd2VFBHRTBINTdJbA0KUlFXRWJVSlpoaDJsMGpBaTc0S1J1VFZlVXZ1alJ6bHd6b1FuV09iSlFGeTRjaElZVUgyK01aSHVsbFhqSWVFdw0KZ2dNaE1JSUMwS0FEQWdFQ0FoTVNBQ2E5Z05ZT3ZkOW1XY0J4QUFBQUpyMkFNQWdHQmlxRkF3SUNBekIvTVNNdw0KSVFZSktvWklodmNOQVFrQkZoUnpkWEJ3YjNKMFFHTnllWEIwYjNCeWJ5NXlkVEVMTUFrR0ExVUVCaE1DVWxVeA0KRHpBTkJnTlZCQWNUQmsxdmMyTnZkekVYTUJVR0ExVUVDaE1PUTFKWlVGUlBMVkJTVHlCTVRFTXhJVEFmQmdOVg0KQkFNVEdFTlNXVkJVVHkxUVVrOGdWR1Z6ZENCRFpXNTBaWElnTWpBZUZ3MHhPREF6TVRZeE16TTFNalJhRncweA0KT0RBMk1UWXhNelExTWpSYU1EQXhMakFzQmdOVkJBTU1KZENRMEx2UXRkQzYwWUhRc05DOTBMVFJnQ0RRcDlDMQ0KMFlMUXN0R1IwWURSZ3RHTDBMa3dZekFjQmdZcWhRTUNBaE13RWdZSEtvVURBZ0lrQUFZSEtvVURBZ0llQVFORA0KQUFSQURoRHRLQVRLd1AzVGZoazJnbzM1MXZ6L2JMVGh5NTRCNm1FRmFjNWtGODVMUnNoQkVHTU9oWWd1MndMUg0KZ0pVdHI5ZVJRbmVtMmhwK1Z0NE9LS0k3UktPQ0FYQXdnZ0ZzTUE0R0ExVWREd0VCL3dRRUF3SUU4REFUQmdOVg0KSFNVRUREQUtCZ2dyQmdFRkJRY0RBakFkQmdOVkhRNEVGZ1FVVXM1YXBnN2FaSTB5QWtOSWR6cTNrRUZkeUlJdw0KSHdZRFZSMGpCQmd3Rm9BVUZURjhzSTBhM21iWEZaeEpVcGNYSkxrQmVvTXdXUVlEVlIwZkJGSXdVREJPb0V5Zw0KU29aSWFIUjBjRG92TDNSbGMzUmpZUzVqY25sd2RHOXdjbTh1Y25VdlEyVnlkRVZ1Y205c2JDOURVbGxRVkU4dA0KVUZKUEpUSXdWR1Z6ZENVeU1FTmxiblJsY2lVeU1ESXVZM0pzTUlHcEJnZ3JCZ0VGQlFjQkFRU0JuRENCbVRCaA0KQmdnckJnRUZCUWN3QW9aVmFIUjBjRG92TDNSbGMzUmpZUzVqY25sd2RHOXdjbTh1Y25VdlEyVnlkRVZ1Y205cw0KYkM5MFpYTjBMV05oTFRJd01UUmZRMUpaVUZSUExWQlNUeVV5TUZSbGMzUWxNakJEWlc1MFpYSWxNakF5TG1OeQ0KZERBMEJnZ3JCZ0VGQlFjd0FZWW9hSFIwY0RvdkwzUmxjM1JqWVM1amNubHdkRzl3Y204dWNuVXZiMk56Y0M5dg0KWTNOd0xuTnlaakFJQmdZcWhRTUNBZ01EUVFCVVcwZXliL2E1UDg0QmZHaHFVUzluS0JDS2FGL3pDTm9PRHdUcQ0KWlVhbWR6Y0FtZGYxQ3Q2ZTB5aDFORWxJa2NrYjlUNkFoSDFkSXFCaDVoYy9ZcDcxTVlJTUx6Q0NCSzhDQVFFdw0KZ1pZd2Z6RWpNQ0VHQ1NxR1NJYjNEUUVKQVJZVWMzVndjRzl5ZEVCamNubHdkRzl3Y204dWNuVXhDekFKQmdOVg0KQkFZVEFsSlZNUTh3RFFZRFZRUUhFd1pOYjNOamIzY3hGekFWQmdOVkJBb1REa05TV1ZCVVR5MVFVazhnVEV4RA0KTVNFd0h3WURWUVFERXhoRFVsbFFWRTh0VUZKUElGUmxjM1FnUTJWdWRHVnlJRElDRXhJQUpyc2lNdEpqZUlzRQ0KSUlrQUFBQW11eUl3Q2dZR0tvVURBZ0lKQlFDZ2dnRlFNQmdHQ1NxR1NJYjNEUUVKQXpFTEJna3Foa2lHOXcwQg0KQndFd0hBWUpLb1pJaHZjTkFRa0ZNUThYRFRFNE1ETXhOakV6TXpreU1sb3dMd1lKS29aSWh2Y05BUWtFTVNJRQ0KSUdTZmhLK1pjUEZybzVIY0d6eE5sWjVFdTErUWo4NEJvUUtLVVZrL0s5alpNSUhrQmdzcWhraUc5dzBCQ1JBQw0KTHpHQjFEQ0IwVENCempDQnl6QUlCZ1lxaFFNQ0Fna0VJTW1qbkdNYWIrM3p5L0hucElhSmtocGpQQ1VRMExSSw0KOU9XR003emYvaHBkTUlHY01JR0VwSUdCTUg4eEl6QWhCZ2txaGtpRzl3MEJDUUVXRkhOMWNIQnZjblJBWTNKNQ0KY0hSdmNISnZMbkoxTVFzd0NRWURWUVFHRXdKU1ZURVBNQTBHQTFVRUJ4TUdUVzl6WTI5M01SY3dGUVlEVlFRSw0KRXc1RFVsbFFWRTh0VUZKUElFeE1RekVoTUI4R0ExVUVBeE1ZUTFKWlVGUlBMVkJTVHlCVVpYTjBJRU5sYm5SbA0KY2lBeUFoTVNBQ2E3SWpMU1kzaUxCQ0NKQUFBQUpyc2lNQW9HQmlxRkF3SUNFd1VBQkVEdGtQTFRTWnZqUWpUMA0KS1NVY0NCN3NhZXhQUTRxS0xrdU02RmVmL0lLVG5aNkxVeXFJbS8rTm5ueGhjVGFpZGoyNXM5eS9NWmJhc2Rjcg0KWDFDTHIzZGlvWUlDWVRDQ0FsMEdDU3FHU0liM0RRRUpCakdDQWs0d2dnSktBZ0VCTUlHV01IOHhJekFoQmdrcQ0KaGtpRzl3MEJDUUVXRkhOMWNIQnZjblJBWTNKNWNIUnZjSEp2TG5KMU1Rc3dDUVlEVlFRR0V3SlNWVEVQTUEwRw0KQTFVRUJ4TUdUVzl6WTI5M01SY3dGUVlEVlFRS0V3NURVbGxRVkU4dFVGSlBJRXhNUXpFaE1COEdBMVVFQXhNWQ0KUTFKWlVGUlBMVkJTVHlCVVpYTjBJRU5sYm5SbGNpQXlBaE1TQUNhOWd6THVxUVNGbC9rZkFBQUFKcjJETUFvRw0KQmlxRkF3SUNDUVVBb0lJQlVEQVlCZ2txaGtpRzl3MEJDUU14Q3dZSktvWklodmNOQVFjQk1Cd0dDU3FHU0liMw0KRFFFSkJURVBGdzB4T0RBek1UWXhNelEzTVRKYU1DOEdDU3FHU0liM0RRRUpCREVpQkNERU1oMnVaNlRRZS9sYw0KNjRXd3RIYkNQeGR1SnFTM3orcFBXNDhZQU1oWjNqQ0I1QVlMS29aSWh2Y05BUWtRQWk4eGdkUXdnZEV3Z2M0dw0KZ2Nzd0NBWUdLb1VEQWdJSkJDQ28yaFZSeHhrSnlJSWxFb2RCeDR3cWZ4NjhQcHhNcUExaDlFQWxwQU9KN3pDQg0KbkRDQmhLU0JnVEIvTVNNd0lRWUpLb1pJaHZjTkFRa0JGaFJ6ZFhCd2IzSjBRR055ZVhCMGIzQnlieTV5ZFRFTA0KTUFrR0ExVUVCaE1DVWxVeER6QU5CZ05WQkFjVEJrMXZjMk52ZHpFWE1CVUdBMVVFQ2hNT1ExSlpVRlJQTFZCUw0KVHlCTVRFTXhJVEFmQmdOVkJBTVRHRU5TV1ZCVVR5MVFVazhnVkdWemRDQkRaVzUwWlhJZ01nSVRFZ0FtdllNeQ0KN3FrRWhaZjVId0FBQUNhOWd6QUtCZ1lxaFFNQ0FoTUZBQVJBUm53S21HM3NRN3VRT3dnRkxzN1gvd05NU0pVLw0KbWZiN3AvZENrajlFRENzSCs0VlhIK0VVdmtnc1JONnJQZDdnanRteG9qaWhLZW5Ldm5RVDlnYTJ6RENDQjNnQw0KQVFFd2daWXdmekVqTUNFR0NTcUdTSWIzRFFFSkFSWVVjM1Z3Y0c5eWRFQmpjbmx3ZEc5d2NtOHVjblV4Q3pBSg0KQmdOVkJBWVRBbEpWTVE4d0RRWURWUVFIRXdaTmIzTmpiM2N4RnpBVkJnTlZCQW9URGtOU1dWQlVUeTFRVWs4Zw0KVEV4RE1TRXdId1lEVlFRREV4aERVbGxRVkU4dFVGSlBJRlJsYzNRZ1EyVnVkR1Z5SURJQ0V4SUFKcnNkeGZKMg0KdE5aSm14RUFBQUFtdXgwd0NnWUdLb1VEQWdJSkJRQ2dnZ0c0TUJnR0NTcUdTSWIzRFFFSkF6RUxCZ2txaGtpRw0KOXcwQkJ3RXdIQVlKS29aSWh2Y05BUWtGTVE4WERURTRNRE14TmpBNE1UY3hOVm93TEFZSUtvVURBaTBCQVFJeA0KSUI0ZUFHWUFhUUJzQUdVQU9nQnhBSGNBWlFCeUFIUUFlUUF1QUhRQWVBQjBNQzhHQ1NxR1NJYjNEUUVKQkRFaQ0KQkNCa240U3ZtWER4YTZPUjNCczhUWldlUkx0ZmtJL09BYUVDaWxGWlB5dlkyVEE0QmdvckJnRUVBWUkzQWdGNA0KTVNvd0tCNENBQ0FlSGdCbUFHa0FiQUJsQURvQWNRQjNBR1VBY2dCMEFIa0FMZ0IwQUhnQWRCNENBQ0F3Z2VRRw0KQ3lxR1NJYjNEUUVKRUFJdk1ZSFVNSUhSTUlIT01JSExNQWdHQmlxRkF3SUNDUVFnVytsa0t3UW0wZWxKTG10Ug0KZjR0VWpyMGtYa3dzV3ltQlo1Sjh4cjQ1WVljd2dad3dnWVNrZ1lFd2Z6RWpNQ0VHQ1NxR1NJYjNEUUVKQVJZVQ0KYzNWd2NHOXlkRUJqY25sd2RHOXdjbTh1Y25VeEN6QUpCZ05WQkFZVEFsSlZNUTh3RFFZRFZRUUhFd1pOYjNOag0KYjNjeEZ6QVZCZ05WQkFvVERrTlNXVkJVVHkxUVVrOGdURXhETVNFd0h3WURWUVFERXhoRFVsbFFWRTh0VUZKUA0KSUZSbGMzUWdRMlZ1ZEdWeUlESUNFeElBSnJzZHhmSjJ0TlpKbXhFQUFBQW11eDB3Q2dZR0tvVURBZ0lUQlFBRQ0KUURCOWMzb0dOdXJvUVV6cWczVkFWVXcxcUhBQmNhSVI2NVhiKzdadDByemIzTndrVUR0by94NDZDcFFhYUJvVg0KQXEzOGxiMWJUUHllaVZyYTEwVjEwbW1oZ2dUQ01JSUNYUVlKS29aSWh2Y05BUWtHTVlJQ1RqQ0NBa29DQVFFdw0KZ1pZd2Z6RWpNQ0VHQ1NxR1NJYjNEUUVKQVJZVWMzVndjRzl5ZEVCamNubHdkRzl3Y204dWNuVXhDekFKQmdOVg0KQkFZVEFsSlZNUTh3RFFZRFZRUUhFd1pOYjNOamIzY3hGekFWQmdOVkJBb1REa05TV1ZCVVR5MVFVazhnVEV4RA0KTVNFd0h3WURWUVFERXhoRFVsbFFWRTh0VUZKUElGUmxjM1FnUTJWdWRHVnlJRElDRXhJQUpyMStyK0pTRzVuRw0KZVJnQUFBQW12WDR3Q2dZR0tvVURBZ0lKQlFDZ2dnRlFNQmdHQ1NxR1NJYjNEUUVKQXpFTEJna3Foa2lHOXcwQg0KQndFd0hBWUpLb1pJaHZjTkFRa0ZNUThYRFRFNE1ETXhOakV6TkRNek1Gb3dMd1lKS29aSWh2Y05BUWtFTVNJRQ0KSVAvUjhOZXdSRSs4Wjlsd2tRd0pUdkI0d1NubTVOM2FuZTBsdEpYRWgrV3RNSUhrQmdzcWhraUc5dzBCQ1JBQw0KTHpHQjFEQ0IwVENCempDQnl6QUlCZ1lxaFFNQ0Fna0VJTEE4Z25Ec3UrSVNqcHBJNEl0MW1wVFN6Z0hVQnlBMQ0KbDlWamdqbGZRNnFGTUlHY01JR0VwSUdCTUg4eEl6QWhCZ2txaGtpRzl3MEJDUUVXRkhOMWNIQnZjblJBWTNKNQ0KY0hSdmNISnZMbkoxTVFzd0NRWURWUVFHRXdKU1ZURVBNQTBHQTFVRUJ4TUdUVzl6WTI5M01SY3dGUVlEVlFRSw0KRXc1RFVsbFFWRTh0VUZKUElFeE1RekVoTUI4R0ExVUVBeE1ZUTFKWlVGUlBMVkJTVHlCVVpYTjBJRU5sYm5SbA0KY2lBeUFoTVNBQ2E5ZnEvaVVodVp4bmtZQUFBQUpyMStNQW9HQmlxRkF3SUNFd1VBQkVCUitWOG9jTmlDRU9tMw0KaU84L3VkZENNVEVNV1dPRFk0Y2p2cGpZb2loUkpTcDAzbjZYd1FadU5BRjJoUVFxRHpMTmE3d0RrYm03cndQdQ0KVEdLbzJ4VFlNSUlDWFFZSktvWklodmNOQVFrR01ZSUNUakNDQWtvQ0FRRXdnWll3ZnpFak1DRUdDU3FHU0liMw0KRFFFSkFSWVVjM1Z3Y0c5eWRFQmpjbmx3ZEc5d2NtOHVjblV4Q3pBSkJnTlZCQVlUQWxKVk1ROHdEUVlEVlFRSA0KRXdaTmIzTmpiM2N4RnpBVkJnTlZCQW9URGtOU1dWQlVUeTFRVWs4Z1RFeERNU0V3SHdZRFZRUURFeGhEVWxsUQ0KVkU4dFVGSlBJRlJsYzNRZ1EyVnVkR1Z5SURJQ0V4SUFKcjJBMWc2OTMyWlp3SEVBQUFBbXZZQXdDZ1lHS29VRA0KQWdJSkJRQ2dnZ0ZRTUJnR0NTcUdTSWIzRFFFSkF6RUxCZ2txaGtpRzl3MEJCd0V3SEFZSktvWklodmNOQVFrRg0KTVE4WERURTRNRE14TmpFek5EVXlORm93THdZSktvWklodmNOQVFrRU1TSUVJUC9SOE5ld1JFKzhaOWx3a1F3Sg0KVHZCNHdTbm01TjNhbmUwbHRKWEVoK1d0TUlIa0Jnc3Foa2lHOXcwQkNSQUNMekdCMURDQjBUQ0J6akNCeXpBSQ0KQmdZcWhRTUNBZ2tFSUdIbGc5dUU2dk15WCtCYk5nbkxCbkhGdmF6Y2h5UUNtTDRNYlBIVlROblpNSUdjTUlHRQ0KcElHQk1IOHhJekFoQmdrcWhraUc5dzBCQ1FFV0ZITjFjSEJ2Y25SQVkzSjVjSFJ2Y0hKdkxuSjFNUXN3Q1FZRA0KVlFRR0V3SlNWVEVQTUEwR0ExVUVCeE1HVFc5elkyOTNNUmN3RlFZRFZRUUtFdzVEVWxsUVZFOHRVRkpQSUV4TQ0KUXpFaE1COEdBMVVFQXhNWVExSlpVRlJQTFZCU1R5QlVaWE4wSUVObGJuUmxjaUF5QWhNU0FDYTlnTllPdmQ5bQ0KV2NCeEFBQUFKcjJBTUFvR0JpcUZBd0lDRXdVQUJFQUpYQ3BXaVBjL0VPVm12bVk1eFY4UDVxR1lhb3hpT1JsVQ0KOE14M0k0VjZkMDNHb0lMYnVwWVA2UXNwelVVb1lZYnVZclBLRktMbHVxVkFCeis4aFdEcUFBQUFBQUFBDQotLS0tLUVORCBDTVMtLS0tLQ0K"
   }

Метрики
^^^^^^^

Включены все дефолтные метрики Spring Boot и Prometheus:

.. code:: bash

   http://localhost:8080/actuator
   http://localhost:8080/actuator/prometheus

Vault
~~~~~

Vault активизируется при конфигурации vault.

Модель Secret:

.. code:: java

   class Secret {
       private String datasourceUsername;
       private String datasourcePassword;
   }

соответсвует JSON,размещаемый на сервере,где развернут Vault

.. code:: json

   {
    "datasourceUsername": "username",
    "datasourcePassword": "password"
   }

Vault (клиентская часть) настраивается так:

.. code:: json

   "vault": {
     "uri": "https://vault.tusvc.bcs.ru/",
     "authentication": "KUBERNETES",
     "kubernetes":{"role": "ebg_r"},
     "entity": {"path":"ebg/common-api-doc/test"}
   }

При неактивной конфигурации vault Secret поднимется из application.yml

Зависимости
-----------

Описание используемых фреймворков и стороннего ПО
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

   <!-- ---------- -->

.. raw:: html

   <!-- ИЗ pom.xml -->

.. raw:: html

   <!-- ---------- -->

Данный сервис написан при помощи `Spring
Boot <https://spring.io/projects/spring-boot>`__ версии 2.2.6 и входящие
него компоненты.

Также используются следующие зависимости:

+----------------------------+---+---------------------------------------+
| Зависимость                | В | Описание                              |
|                            | е |                                       |
|                            | р |                                       |
|                            | с |                                       |
|                            | и |                                       |
|                            | я |                                       |
+============================+===+=======================================+
| `spring-b                  | 2 | Зависимости для запуска web-сервера   |
| oot-starter-web <https://m | . | внутри приложения                     |
| vnrepository.com/artifact/ | 3 |                                       |
| org.springframework.boot/s | . |                                       |
| pring-boot-starter-web>`__ | 0 |                                       |
|                            | . |                                       |
|                            | R |                                       |
|                            | E |                                       |
|                            | L |                                       |
|                            | E |                                       |
|                            | A |                                       |
|                            | S |                                       |
|                            | E |                                       |
+----------------------------+---+---------------------------------------+
| `spring-boot-starter-act   | 2 | Эндпоинты с информацией о сервисе     |
| uator <https://docs.spring | . |                                       |
| .io/spring-boot/docs/curre | 3 |                                       |
| nt/reference/html/producti | . |                                       |
| on-ready-features.html>`__ | 0 |                                       |
|                            | . |                                       |
|                            | R |                                       |
|                            | E |                                       |
|                            | L |                                       |
|                            | E |                                       |
|                            | A |                                       |
|                            | S |                                       |
|                            | E |                                       |
+----------------------------+---+---------------------------------------+
| `spring                    |   | Процессор аннотации конфигурации      |
| -boot-configuration-proces |   | Spring Boot                           |
| sor <https://mvnrepository |   |                                       |
| .com/artifact/org.springfr |   |                                       |
| amework.boot/spring-boot-c |   |                                       |
| onfiguration-processor>`__ |   |                                       |
+----------------------------+---+---------------------------------------+
| `spring-boot-starte        | 2 | Стартер для использования Spring Data |
| r-data-jpa <https://mvnrep | . | JPA с Hibernate                       |
| ository.com/artifact/org.s | 3 |                                       |
| pringframework.boot/spring | . |                                       |
| -boot-starter-data-jpa>`__ | 0 |                                       |
|                            | . |                                       |
|                            | R |                                       |
|                            | E |                                       |
|                            | L |                                       |
|                            | E |                                       |
|                            | A |                                       |
|                            | S |                                       |
|                            | E |                                       |
+----------------------------+---+---------------------------------------+
| `spring-boo                |   | Стартер для тестирования приложений   |
| t-starter-test <https://mv |   | Spring Boot с библиотеками, включая   |
| nrepository.com/artifact/o |   | JUnit, Hamcrest и Mockito             |
| rg.springframework.boot/sp |   |                                       |
| ring-boot-starter-test>`__ |   |                                       |
+----------------------------+---+---------------------------------------+
| `spring-vault-core <http   | 2 | Интерфейс к HashiCorp Vault           |
| s://mvnrepository.com/arti | . |                                       |
| fact/org.springframework.v | 1 |                                       |
| ault/spring-vault-core>`__ | . |                                       |
|                            | 1 |                                       |
|                            | . |                                       |
|                            | R |                                       |
|                            | E |                                       |
|                            | L |                                       |
|                            | E |                                       |
|                            | A |                                       |
|                            | S |                                       |
|                            | E |                                       |
+----------------------------+---+---------------------------------------+
| `springfox-swagger2 <htt   | 2 | Генерация документации в формате Open |
| ps://springfox.github.io/s | . | API Specification                     |
| pringfox/docs/current/>`__ | 9 |                                       |
|                            | . |                                       |
|                            | 2 |                                       |
+----------------------------+---+---------------------------------------+
| `springfox                 | 2 | Генерация страницы swagger-ui.html    |
| -swagger-ui <https://www.g | . | для отображения swagger-докуаментации |
| oogle.com/search?q=springf | 9 |                                       |
| ox-swagger-ui+maven&oq=spr | . |                                       |
| ingfox-swagger-ui&aqs=chro | 2 |                                       |
| me.1.69i57j0l7.1136j0j4&so |   |                                       |
| urceid=chrome&ie=UTF-8>`__ |   |                                       |
+----------------------------+---+---------------------------------------+
| `microme                   | 1 | Сбор метрик                           |
| ter-core <https://mvnrepos | . |                                       |
| itory.com/artifact/io.micr | 5 |                                       |
| ometer/micrometer-core>`__ | . |                                       |
| и                          | 1 |                                       |
| `micrometer-               |   |                                       |
| registry-prometheus <https |   |                                       |
| ://mvnrepository.com/artif |   |                                       |
| act/io.micrometer/micromet |   |                                       |
| er-registry-prometheus>`__ |   |                                       |
+----------------------------+---+---------------------------------------+
| `snakeyaml <https://b      | 1 | Сериализация и десериализация yaml    |
| itbucket.org/asomov/snakey | . |                                       |
| aml/wiki/Documentation>`__ | 2 |                                       |
|                            | 6 |                                       |
+----------------------------+---+---------------------------------------+
| `h2 <https://www.h2datab   | 1 | Встраемавая СУБД H2                   |
| ase.com/html/main.html>`__ | . |                                       |
|                            | 4 |                                       |
|                            | . |                                       |
|                            | 2 |                                       |
|                            | 0 |                                       |
|                            | 0 |                                       |
+----------------------------+---+---------------------------------------+
| `postgres                  | 4 | PostgreSQL JDBC Driver                |
| ql <https://jdbc.postgresq | 2 |                                       |
| l.org/about/about.html>`__ | . |                                       |
|                            | 2 |                                       |
|                            | . |                                       |
|                            | 1 |                                       |
|                            | 1 |                                       |
+----------------------------+---+---------------------------------------+
| `bcpkix-j                  | 1 | API Java Bouncy Castle для CMS, PKCS, |
| dk15on <https://mvnreposit | . | EAC, TSP, CMP, CRMF, OCSP и генерации |
| ory.com/artifact/org.bounc | 6 | сертификатов. Этот jar содержит API   |
| ycastle/bcpkix-jdk15on>`__ | 4 | для JDK 1.5 до JDK 1.8.               |
|                            |   | API-интерфейсы можно использовать     |
|                            |   | совместно с JCE / JCA-провайдером,    |
|                            |   | например, предоставляемым с           |
|                            |   | API-интерфейсами криптографии Bouncy  |
|                            |   | Castle.                               |
+----------------------------+---+---------------------------------------+
| `gro                       | 2 | Мощный, динамичный язык для JVM       |
| ovy-all <https://mvnreposi | . |                                       |
| tory.com/artifact/org.code | 4 |                                       |
| haus.groovy/groovy-all>`__ | . |                                       |
|                            | 1 |                                       |
|                            | 9 |                                       |
+----------------------------+---+---------------------------------------+
| `gro                       | 2 | Адаптер Maven для пакетного           |
| ovy-eclipse-compiler <http | . | компилятора Groovy-Eclipse            |
| s://mvnrepository.com/arti | 9 |                                       |
| fact/org.codehaus.groovy/g | . |                                       |
| roovy-eclipse-compiler>`__ | 2 |                                       |
|                            | - |                                       |
|                            | 0 |                                       |
|                            | 1 |                                       |
+----------------------------+---+---------------------------------------+
| `groovy-eclipse-batch <h   | 2 | Groovy Eclipse Compiler, упакованный  |
| ttps://mvnrepository.com/a | . | для пакетного использования от Maven  |
| rtifact/org.codehaus.groov | 4 |                                       |
| y/groovy-eclipse-batch>`__ | . |                                       |
|                            | 1 |                                       |
|                            | 9 |                                       |
|                            | - |                                       |
|                            | 0 |                                       |
|                            | 1 |                                       |
+----------------------------+---+---------------------------------------+

Сборка
------

Сборка java-проекта
~~~~~~~~~~~~~~~~~~~

Сборка проекта осуществляется при помощи maven.

.. code:: bash

   mvn clean package

Сборка Docker-образа
~~~~~~~~~~~~~~~~~~~~

.. code:: bash

   mvn clean package
   cd ./service
   docker build .
