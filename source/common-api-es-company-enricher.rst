common-api-es-company-enricher
==============================

Описание
--------

Сервис для сбора информации о компании из открытых источников.

Предоставляемые API
-------------------

Методы
~~~~~~

``/company``
^^^^^^^^^^^^

``GET /company``
''''''''''''''''

Получение ID компании по ИНН и ОГРН

+-------------+-------------+-------------+-------------+-------------+
| Параметр    | О           | Содержится  | Тип         | Описание    |
|             | бязательный | в           |             |             |
+=============+=============+=============+=============+=============+
| ``inn``     | Да          | query       | string      | ИНН         |
+-------------+-------------+-------------+-------------+-------------+
| ``ogrn``    | Да          | query       | string      | ОГРН        |
+-------------+-------------+-------------+-------------+-------------+
| ``s         | Нет         | query       | array       |             |
| ourceTags`` |             |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| ``sourc     | Нет         | query       | string      | Список      |
| eTags``\ [] |             |             |             | внешних     |
|             |             |             |             | источников. |
|             |             |             |             | Если        |
|             |             |             |             | передан то  |
|             |             |             |             | проверяются |
|             |             |             |             | только они. |
+-------------+-------------+-------------+-------------+-------------+

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant api as API
      participant es as ElasticSearch
      participant kf as KonturFocusService

      Note left of Client: Получение ID компании по ИНН и ОГРН
      
      Client->>api: GET /company
      
      rect rgba(227, 238, 242, 30)
         Note right of api: findCompanyByInnAndOgrn(inn, ogrn)
         api->>es: inn, ogrn
         es->>api: companyId
      end

      opt !companyId

         rect rgba(174, 234, 227, 30)
            Note right of api: requestDataFromExternalSources(inn, ogrn, sourceTags)
            api->>kf: inn, ogrn, sourceTags
            kf->>api: req
            api->>kf: inn, ogrn, sourceTags
            kf->>api: egrDetails
            api->>kf: inn, ogrn, sourceTags
            kf->>api: analyticsResponse
            api->>kf: inn, ogrn, sourceTags
            kf->>api: licenses
            api->>kf: inn, ogrn, sourceTags
            kf->>api: lazyBranches
         end
      
         rect rgba(227, 238, 242, 30)
            Note right of api: findCompanyByInnAndOgrn(inn, ogrn)
            api->>es: inn, ogrn
            es->>api: companyId
         end

         opt !companyId
            rect rgba(242, 220, 166, 30)
               Note right of api: createCompanyIfNotExists(inn, ogrn)
      
               rect rgba(227, 238, 242, 30)
                  Note right of api: findCompanyByInnAndOgrn(inn, ogrn)
                  api->>es: inn, ogrn
                  es->>api: companyId
               end

               rect rgba(237, 207, 130, 30)
                  Note right of api: createCompanyByInnAndOgrn(inn, ogrn)
                  api->>es: inn, ogrn
                  es->>api: companyId
               end
            end
         end

         opt result.isNew
            rect rgba(232, 193, 94, 30)
               Note right of api: createCompanyDoc(data, true)
            end
            rect rgba(244, 162, 97, 30)
               Note right of api: updateCompany(result.companyId, createCompanyDoc)
               api->>es: companyId, doc
            end
         end
      
      end
      
      api->>Client: companyId

Логика
      

.. raw:: html

   <!-- findCompanyByInnAndOgrn -->

1. По ИНН и ОГРН в ``ElasticSearch`` ищется компания (``companyId``)
2. Если компания найдена, ``companyId`` возвращается пользователю
3. Если компания не найдена, то у ``KonturFocusService`` запрашивается
   информация о компании

   1. По ИНН и ОГРН запрашиваются `базовые
      реквизиты <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2Freq&type=get>`__
      (``req``)
   2. По ИНН, ОГРН запрашиваются `расширенные сведения на основе
      ЕГРЮЛ/ЕГРИП <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2FegrDetails&type=get>`__
      (``egrDetails``)
   3. По ИНН, ОГРН запрашивается `расширенная
      аналитика <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2Fanalytics&type=get>`__
      (``analyticsResponse``)
   4. По ИНН, ОГРН запрашивается `информация о
      лицензиях <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2Flicences&type=get>`__
      (``licenses``)
   5. По ИНН, ОГРН запрашиваются ```???`` <>`__ (``lazyBranches``) >
      ``KonturFocusService`` больше не поддерживает метод
      ``/companyBranches``

4. По ИНН и ОГРН в ``ElasticSearch`` снова проверяется наличие компании
   (``companyId``)
5. Если компания найдена, ``companyId`` возвращается пользователю
6. Если компания не найдена, то в ``ElasticSearch`` по полученным данным
   создаётся новая запись о компании

   1. По ИНН и ОГРН в ``ElasticSearch`` снова проверяется наличие
      компании (``companyId``)
   2. Если компания не найдена, в ``ElasticSearch`` создаётся запись с
      ИНН и ОГРН компании. Ответом приходит ``companyId``

7. Если в ходе проверки была создана запись о компании
   (``result.isNew``), то

   1. По ИНН и ОГРН организации в ``ElasticSearch`` добавляются
      полученные из ``KonturFocusService`` данные (``data``)

8. ``companyId`` возвращается пользователю

Ответ
     

``/enrich``
^^^^^^^^^^^

``POST /enrich``
''''''''''''''''

Обогащение компании

+-------------+-------------+-------------+-------------+-------------+
| Параметр    | О           | Содержится  | Тип         | Описание    |
|             | бязательный | в           |             |             |
+=============+=============+=============+=============+=============+
| ``          | Да          | query       | string      | Ид          |
| companyId`` |             |             |             | ентификатор |
|             |             |             |             | компании    |
+-------------+-------------+-------------+-------------+-------------+
| ``mustCreat | Нет         | query       | boolean     | Признак     |
| eFounders`` |             |             |             | не          |
|             |             |             |             | обходимости |
|             |             |             |             | создания    |
|             |             |             |             | учредителей |
+-------------+-------------+-------------+-------------+-------------+
| ``mustEnric | Нет         | query       | boolean     | Признак     |
| hBranches`` |             |             |             | не          |
|             |             |             |             | обходимости |
|             |             |             |             | создания    |
|             |             |             |             | филиалов    |
+-------------+-------------+-------------+-------------+-------------+
| `           | Нет         | query       | boolean     | Признак     |
| `mustEnrich |             |             |             | не          |
| Relations`` |             |             |             | обходимости |
|             |             |             |             | создания    |
|             |             |             |             | связей      |
+-------------+-------------+-------------+-------------+-------------+
| ``mustUpda  | Нет         | query       | boolean     | Признак     |
| teAddress`` |             |             |             | не          |
|             |             |             |             | обходимости |
|             |             |             |             | обновления  |
|             |             |             |             | адреса      |
+-------------+-------------+-------------+-------------+-------------+
| ``s         | Нет         | query       | array       |             |
| ourceTags`` |             |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| ``sourc     | Нет         | query       | string      | Список      |
| eTags``\ [] |             |             |             | внешних     |
|             |             |             |             | источников. |
|             |             |             |             | Если        |
|             |             |             |             | передан то  |
|             |             |             |             | проверяются |
|             |             |             |             | только они. |
+-------------+-------------+-------------+-------------+-------------+

.. code:: mermaid

   sequenceDiagram
      participant Client
      participant api as API
      participant es as ElasticSearch
      participant kf as KonturFocusService

      Note left of Client: Обогащение компании

      Client->>api: POST /enrich
      
      rect rgba(115, 169, 191, 30)
         Note right of api: getCompany 
         api->>es: companyId
         es->>api: company
      end

      rect rgba(174, 234, 227, 30)
         Note right of api: requestDataFromExternalSources(inn, ogrn, sourceTags)
         api->>kf: inn, ogrn, sourceTags
         kf->>api: req
         api->>kf: inn, ogrn, sourceTags
         kf->>api: egrDetails
         api->>kf: inn, ogrn, sourceTags
         kf->>api: analyticsResponse
         api->>kf: inn, ogrn, sourceTags
         kf->>api: licenses
         api->>kf: inn, ogrn, sourceTags
         kf->>api: lazyBranches
      end
      rect rgba(244, 162, 97, 30)
         Note right of api: updateCompany(result.companyId, createCompanyDoc)
         api->>es: companyId, doc
      end
      opt mustEnrichRelations
         rect rgba(240, 166, 147, 30)
            Note right of api: enrichCompanyRelations 
            rect rgba(115, 169, 191, 30)
               Note right of api: getCompany 
               api->>es: companyId
               es->>api: company
            end
            api->>kf: inn, ogrn, sourceTags
            kf->>api: req
            loop req.UL.heads.each
               rect rgba(143, 187, 204, 30)
                  Note right of api: findPersonsByInn 
                  api->>es: inn
                  es->>api: personIds
               end
               opt !personIds 
                  rect rgba(185, 213, 223, 30)
                     Note right of api: findPersonsByFio
                     api->>es: lastName, secondName, firstName
                     es->>api: personIds
                  end
                  opt !personIds
                     rect rgba(240, 166, 147, 30)
                        Note right of api: createPersonByInn
                        api->>es: inn, fio
                        es->>api: personId
                     end
                  end
               end

               api->>es: personIds, companyId
               es->>api: relations

               opt relations.hits.total == 0
                  rect rgba(227, 179, 59, 30)
                     Note right of api: createEmployeeRelation
                     api->>es: personId, companyId, validFromDate, isMain, position
                  end
               end
               
               api->>kf: innfl, sourceTags
               kf->>api: affs

               loop affs.each
                  rect rgba(227, 238, 242, 30)
                     Note right of api: findCompanyByInnAndOgrn(inn, ogrn)
                     api->>es: inn, ogrn
                     es->>api: affCompanyId
                  end
                  opt !affCompanyId
                     rect rgba(242, 220, 166, 30)
                        Note right of api: createCompanyIfNotExists(inn, ogrn)
               
                        rect rgba(227, 238, 242, 30)
                           Note right of api: findCompanyByInnAndOgrn(inn, ogrn)
                           api->>es: inn, ogrn
                           es->>api: affCompanyId
                        end

                        rect rgba(237, 207, 130, 30)
                           Note right of api: createCompanyByInnAndOgrn(inn, ogrn)
                           api->>es: inn, ogrn
                           es->>api: affCompanyId
                        end
                     end
                  end

                  opt affCompanyId && affCompanyId != crmId
                     opt headInnCheck
                        rect rgba(240, 166, 147, 30)
                           Note right of api: enrichCompanyRelations(affCompanyId, sourceTags, false) 
                        end
                     end
                  end
                  opt needEnrich
                     rect rgba(240, 166, 147, 30)
                        Note right of api: enrichCompanyRelations(affCompanyId, sourceTags, false, false, mustUpdateAddress) 
                     end
                  end
               end

            end
         end
      end
      opt mustCreateFounders
         rect rgba(240, 214, 148, 30)
            Note right of api: createFounders
            rect rgba(115, 169, 191, 30)
               Note right of api: getCompany 
               api->>es: companyId
               es->>api: company
            end
         end
         
         api->>kf: inn, ogrn, sourceTags
         kf->>api: egrDetails

         loop egrDetails?.UL?.foundersFL?.each
            rect rgba(143, 187, 204, 30)
               Note right of api: findPersonsByInn 
               api->>es: inn
               es->>api: personIds
            end
            opt !personIds 
               rect rgba(185, 213, 223, 30)
                  Note right of api: findPersonsByFio
                  api->>es: lastName, secondName, firstName
                  es->>api: personIds
               end
            end
            opt !personIds 
               rect rgba(235, 200, 112, 30)
                  Note right of api: createPersonByInn
                  api->>es: inn, fio
                  es->>api: personId
               end
            end
         end
      end

      api->>Client: Подтверждение

.. _логика-1:

Логика
      

1. По ``companyId`` в ``ElasticSearch`` ищется имеющаяся информация о
компании (``company``) 1. В ``KonturFocus`` запрашивается информация о
компании (``data``) 1. По ИНН и ОГРН запрашиваются `базовые
реквизиты <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2Freq&type=get>`__
(``req``) 2. По ИНН, ОГРН запрашиваются `расширенные сведения на основе
ЕГРЮЛ/ЕГРИП <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2FegrDetails&type=get>`__
(``egrDetails``) 3. По ИНН, ОГРН запрашивается `расширенная
аналитика <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2Fanalytics&type=get>`__
(``analyticsResponse``) 4. По ИНН, ОГРН запрашивается `информация о
лицензиях <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2Flicences&type=get>`__
(``licenses``) 5. По ИНН, ОГРН запрашиваются ```???`` <>`__
(``lazyBranches``) 1. По ``companyId`` в ``ElasticSearch`` добавляются
полученные из ``KonturFocus`` данные (``data``) 1. Если в запросе стоит
флаг на заполнение информации о связях (``mustEnrichRelations``), то
запускается поиск информации: 1. хм 1. По ``companyId`` в
``Elasticsearch`` ищется имеющаяся информация о компании (``company``)
2. По ИНН и ОГРН запрашиваются `базовые
реквизиты <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2Freq&type=get>`__
(``req``) 1. Производится поиск информации по каждому руководителю
организации (``req.UL.heads -> head``): 1. По ИНН руководителя в
``Elasticsearch`` ищется существующая информация (``personIds``) 1. Если
``personIds`` не были получены, то производится повторный поиск
``Elasticsearch`` по ФИО руководителя 1. Если ``personIds`` не были
получены, то по ИНН и ФИО в ``Elasticsearch`` создаётся новая запись.
Ответом получается идентификатор записи (``personIds``) 2. Производится
запрос на то, какой ``id`` из списка ``personIds`` находится в числе
сотрудников организации (``relations``) 1. Если совпадений не найдено
(``relations.hits.total == 0``), то создаётся новая связь в
``Elasticsearch``:
``groovy             [                leftId           : personId,      // personIds.get(0)                rightId          : companyId,     // companyId                validFromDate    : validFromDate, // head.date                employee         : [                   isMain  : isMain,              // true                   position: position             // head.position                ],                relationTypeRefId: 'employee'             ]``
1. Если установлен флаг на проверку аффилиатов, то такая проверка
осуществляется: 1. По ИНН руководителя в ``KonturService`` производится
поиск аффилиатов (``affs``) 2. Каждый найденный аффилиат (``aff``)
обрабатывается 1. По ИНН и ОГРН в ``ElasticSearch`` ищется
организация-аффилиат (``affCompanyId``) 1. Если компания не найдена, то
в ``ElasticSearch`` по полученным данным создаётся новая запись о
компании 1. По ИНН и ОГРН в ``ElasticSearch`` снова проверяется наличие
компании (``affCompanyId``) 2. Если компания не найдена, в
``ElasticSearch`` создаётся запись с ИНН и ОГРН компании. Ответом
приходит ``affCompanyId`` 1. Если компания не найдена, в
``ElasticSearch`` создаётся запись с ИНН и ОГРН компании. Ответом
приходит ``affCompanyId`` 1. Если организация найдена, и её
идентификатор не совпадает с идентификатором исходной организации
(``affCompanyId && affCompanyId != crmId``), то 1. Все ИНН руководителей
аффилиата проверяются на совпадение с ИНН руководителя исходной
организации (``affHead.innfl == head.innfl``) 1. Если совпадение было
найдно, то для аффилиата запускается полный сбор информации
(``affHead.innfl == head.innfl``) 1. Если в ``Elasticsearch`` ранее не
было записи об аффилиате, то запускается сбор информации о компании 1.
По ``companyId`` в ``ElasticSearch`` ищется имеющаяся информация о
компании (``company``) 2. В ``KonturFocusService`` запрашивается
информация о компании 1. По ИНН и ОГРН запрашиваются `базовые
реквизиты <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2Freq&type=get>`__
(``req``) 2. По ИНН, ОГРН запрашиваются `расширенные сведения на основе
ЕГРЮЛ/ЕГРИП <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2FegrDetails&type=get>`__
(``egrDetails``) 3. По ИНН, ОГРН запрашивается `расширенная
аналитика <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2Fanalytics&type=get>`__
(``analyticsResponse``) 4. По ИНН, ОГРН запрашивается `информация о
лицензиях <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2Flicences&type=get>`__
(``licenses``) 5. По ИНН, ОГРН запрашиваются ```???`` <>`__
(``lazyBranches``) > ``KonturFocusService`` больше не поддерживает метод
``/companyBranches`` 1. По ИНН и ОГРН организации в ``ElasticSearch``
добавляются полученные из ``KonturFocusService`` данные (``data``) 1.
Если в запросе стоит флаг на создание записей об учредителях организации
(``mustCreateFounders``), то запускается поиск информации: 1. По
``companyId`` в ``ElasticSearch`` ищется имеющаяся информация о компании
(``company``) 2. По ИНН, ОГРН в ``KonturFocus`` запрашиваются
`расширенные сведения на основе
ЕГРЮЛ/ЕГРИП <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2FegrDetails&type=get>`__
(``egrDetails``) 3. Для каждого найденного учредителя (физические лица)
производятся следующие действия: 1. По ИНН учредителя в
``Elasticsearch`` ищется существующая информация (``personIds``) 1. Если
``personIds`` не были получены, то производится повторный поиск
``Elasticsearch`` по ФИО учредителя 1. Если ``personIds`` не были
получены, то по ИНН и ФИО в ``Elasticsearch`` создаётся новая запись.
Ответом получается идентификатор записи (``personIds``) 2. В
``Elasticsearch`` производится запрос на то, какой ``id`` из списка
``personIds`` находится в числе сотрудников организации (``relations``)
3. Проверяется, существуют ли записи о человеке, как об учредителе: 1. В
``Elasticsearch`` запрашиваются все связи, где лицо выступает в качестве
учредителя 4. Если связей не существует, то 1. Вычисляется доля
учредителя ``share`` 2. В ``Elasticsearch`` создаётся новая связь
``groovy             [                leftId           : personId,                rightId          : companyId,                validFromDate    : LocalDate.now().toString(),                personFounder    : [                   share: [                      sum    : share.sum,                      percent: share.percent                   ]                ],                relationTypeRefId: 'personFounder'             ]``
4. По всем учредителям (юридические лица), у которых указан ИНН также
производится сбор информации: 1. По ИНН и ОГРН в ``ElasticSearch`` снова
проверяется наличие компании (``companyId``) 2. Если компания найдена,
``companyId`` возвращается пользователю 3. Если компания не найдена, то
в ``ElasticSearch`` по полученным данным создаётся новая запись о
компании 1. По ИНН и ОГРН в ``ElasticSearch`` снова проверяется наличие
компании (``companyId``) 2. Если компания не найдена, в
``ElasticSearch`` создаётся запись с ИНН и ОГРН компании. Ответом
приходит ``companyId`` > Если организация не найдена и не была создана,
сбор заканчивается 2. Запускается сбор информации о компании 1. По
``companyId`` в ``ElasticSearch`` ищется имеющаяся информация о компании
(``company``) 2. В ``KonturFocusService`` запрашивается информация о
компании 1. По ИНН и ОГРН запрашиваются `базовые
реквизиты <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2Freq&type=get>`__
(``req``) 2. По ИНН, ОГРН запрашиваются `расширенные сведения на основе
ЕГРЮЛ/ЕГРИП <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2FegrDetails&type=get>`__
(``egrDetails``) 3. По ИНН, ОГРН запрашивается `расширенная
аналитика <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2Fanalytics&type=get>`__
(``analyticsResponse``) 4. По ИНН, ОГРН запрашивается `информация о
лицензиях <https://developer.kontur.ru/doc/focus/method?path=%2Fapi3%2Flicences&type=get>`__
(``licenses``) 5. По ИНН, ОГРН запрашиваются ```???`` <>`__
(``lazyBranches``) > ``KonturFocusService`` больше не поддерживает метод
``/companyBranches`` 1. По ИНН и ОГРН организации в ``ElasticSearch``
добавляются полученные из ``KonturFocusService`` данные (``data``) 3.
Вычисляется доля организации-учредителя (``share``) 4. Проверяется
наличие записи о связи организации-учредителя и исходной организации: 1.
По идентификаторам обоих организаций в ``Elasticsearch`` ищется
отношение типа ``companyFounder`` 5. Если отношения найдены не были, то
такая запись создаётся: 1. В ``Elasticsearch`` создаётся запись:
``groovy               [                 leftId           : companyFounderId,                 rightId          : companyId,                 validFromDate    : LocalDate.now().toString(),                 companyFounder   : [                         share: share                 ],                 relationTypeRefId: 'companyFounder'               ]``

Модель данных ``common-api-es-company-enricher``
------------------------------------------------

Person
~~~~~~

Адрес: ``/crm/person``

=============== ====== ==========
Параметр        Тип    Описание
=============== ====== ==========
``INN``         srting ИНН
``displayName`` srting Полное ФИО
``lastName``    srting Фамилия
``firstName``   srting Имя
``secondName``  srting Отчество
=============== ====== ==========

Relation
~~~~~~~~

Адрес: ``/crm/relation``

+-----------------+-----------------+-----------------+-----------------+
| Параметр        | Тип             | Описание        | Источник        |
+=================+=================+=================+=================+
| ``leftId``      | string          | Идентификатор   |                 |
|                 |                 | левой сущности  |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``rightId``     | string          | Идентификатор   |                 |
|                 |                 | правой сущности |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``              | string          | Дата создания   | LocalDate.no    |
| validFromDate`` |                 | связи           | w().toString(), |
+-----------------+-----------------+-----------------+-----------------+
| ``c             | object          | Указывается,    |                 |
| ompanyFounder`` |                 | если тип        |                 |
|                 |                 | отношения       |                 |
|                 |                 | ``relation      |                 |
|                 |                 | TypeRefId = 'co |                 |
|                 |                 | mpanyFounder'`` |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``company       | object          | Доля            |                 |
| Founder.share`` |                 |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``companyFoun   | integer         | Доля в рублях   | e               |
| der.share.sum`` |                 |                 | grDetails.UL.fo |
|                 |                 |                 | undersUL.share, |
+-----------------+-----------------+-----------------+-----------------+
| ``              | BigDecimal      | Доля в          | e               |
| companyFounder. |                 | процентах       | grDetails.UL.fo |
| share.percent`` |                 |                 | undersUL.share, |
|                 |                 |                 | e               |
|                 |                 |                 | grDetails.UL.st |
|                 |                 |                 | atedCapital.sum |
+-----------------+-----------------+-----------------+-----------------+
| ``employee``    | object          | Указывается,    |                 |
|                 |                 | если тип        |                 |
|                 |                 | отношения       |                 |
|                 |                 | ``re            |                 |
|                 |                 | lationTypeRefId |                 |
|                 |                 |  = 'employee'`` |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``em            | bool            |                 | ``true``        |
| ployee.isMain`` |                 |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``empl          | string          | Должность       | req.UL.h        |
| oyee.position`` |                 |                 | eads[].position |
+-----------------+-----------------+-----------------+-----------------+
| ``              | object          | Указывается,    |                 |
| personFounder`` |                 | если тип        |                 |
|                 |                 | отношения       |                 |
|                 |                 | ``relatio       |                 |
|                 |                 | nTypeRefId = 'p |                 |
|                 |                 | ersonFounder'`` |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``person        | object          | Доля            |                 |
| Founder.share`` |                 |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``personFoun    | integer         | Доля в рублях   | e               |
| der.share.sum`` |                 |                 | grDetails.UL.fo |
|                 |                 |                 | undersFL.share, |
+-----------------+-----------------+-----------------+-----------------+
| `               | BigDecimal      | Доля в          | e               |
| `personFounder. |                 | процентах       | grDetails.UL.fo |
| share.percent`` |                 |                 | undersFL.share, |
|                 |                 |                 | e               |
|                 |                 |                 | grDetails.UL.st |
|                 |                 |                 | atedCapital.sum |
+-----------------+-----------------+-----------------+-----------------+
| ``rela          | string          | Тип отношения   |                 |
| tionTypeRefId`` |                 | (``'com         |                 |
|                 |                 | panyFounder'``, |                 |
|                 |                 | ``'employee'``, |                 |
|                 |                 | ``'pe           |                 |
|                 |                 | rsonFounder'``) |                 |
+-----------------+-----------------+-----------------+-----------------+

Address
^^^^^^^

Адрес: ``/crm/address``

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``addressTypeRefId``  | string                | Идентификатор адреса  |
+-----------------------+-----------------------+-----------------------+
| ``area``              | string                | Район в регионе       |
+-----------------------+-----------------------+-----------------------+
| ``area_fias_id``      | string                | Код ФИАС района в     |
|                       |                       | регионе               |
+-----------------------+-----------------------+-----------------------+
| ``area_kladr_id``     | string                | Код КЛАДР района в    |
|                       |                       | регионе               |
+-----------------------+-----------------------+-----------------------+
| ``area_type``         | string                | Тип района в регионе  |
|                       |                       | (сокращенный)         |
+-----------------------+-----------------------+-----------------------+
| ``area_type_full``    | string                | Тип района в регионе  |
+-----------------------+-----------------------+-----------------------+
| ``area_with_type``    | string                | Район в регионе с     |
|                       |                       | типом                 |
+-----------------------+-----------------------+-----------------------+
| ``beltway_distance``  | string                | Расстояние от         |
|                       |                       | кольцевой в           |
|                       |                       | километрах            |
+-----------------------+-----------------------+-----------------------+
| ``beltway_hit``       | string                | Внутри кольцевой?     |
+-----------------------+-----------------------+-----------------------+
| ``block``             | string                | Корпус/строение       |
+-----------------------+-----------------------+-----------------------+
| ``block_type``        | string                | Тип корпуса/строения  |
|                       |                       | (сокращенный)         |
+-----------------------+-----------------------+-----------------------+
| ``block_type_full``   | string                | Тип корпуса/строения  |
+-----------------------+-----------------------+-----------------------+
| ``capital_marker``    | string                | Признак центра района |
|                       |                       | или региона:, ``1`` - |
|                       |                       | центр района          |
|                       |                       | (Московская обл,      |
|                       |                       | Одинцовский р-н, г    |
|                       |                       | Одинцово), ``2`` -    |
|                       |                       | центр региона,        |
|                       |                       | (Новосибирская обл, г |
|                       |                       | Новосибирск), ``3`` - |
|                       |                       | центр района и        |
|                       |                       | региона, (Томская     |
|                       |                       | обл, г Томск), ``4``  |
|                       |                       | - центральный район   |
|                       |                       | региона, (Тюменская   |
|                       |                       | обл, Тюменский р-н),  |
|                       |                       | ``0`` - ничего из     |
|                       |                       | перечисленного,       |
|                       |                       | (Московская обл, г    |
|                       |                       | Балашиха)             |
+-----------------------+-----------------------+-----------------------+
| ``city``              | string                | Город                 |
+-----------------------+-----------------------+-----------------------+
| ``city_area``         | string                | Административный      |
|                       |                       | округ (только для     |
|                       |                       | Москвы)               |
+-----------------------+-----------------------+-----------------------+
| ``city_district``     | string                | Район города          |
+-----------------------+-----------------------+-----------------------+
| ``city_fias_id``      | string                | Код ФИАС города       |
+-----------------------+-----------------------+-----------------------+
| ``city_kladr_id``     | string                | Код КЛАДР города      |
+-----------------------+-----------------------+-----------------------+
| ``city_type``         | string                | Тип города            |
|                       |                       | (сокращенный)         |
+-----------------------+-----------------------+-----------------------+
| ``city_type_full``    | string                | Тип города            |
+-----------------------+-----------------------+-----------------------+
| ``city_with_type``    | string                | Город с типом         |
+-----------------------+-----------------------+-----------------------+
| ``companyId``         | string                | Идентификатор         |
|                       |                       | компании              |
+-----------------------+-----------------------+-----------------------+
| ``country``           | string                | Страна                |
+-----------------------+-----------------------+-----------------------+
| ``fias_id``           | string                | Код ФИАС:             |
|                       |                       | HOUSE.HOUSEGUID, если |
|                       |                       | дом найден в ФИАС по  |
|                       |                       | точному совпадению;   |
|                       |                       | ADDROBJ.AOGUID в      |
|                       |                       | противном случае.     |
+-----------------------+-----------------------+-----------------------+
| ``fias_level``        | string                | Уровень детализации,  |
|                       |                       | до которого адрес     |
|                       |                       | найден в ФИАС: ``0``  |
|                       |                       | - страна, ``1`` -     |
|                       |                       | регион, ``3`` -       |
|                       |                       | район, ``4`` - город, |
|                       |                       | ``5`` - район города, |
|                       |                       | ``6`` - населенный    |
|                       |                       | пункт, ``7`` - улица, |
|                       |                       | ``8`` - дом, ``65`` - |
|                       |                       | планировочная         |
|                       |                       | структура, ``-1`` -   |
|                       |                       | иностранный или       |
|                       |                       | пустой                |
+-----------------------+-----------------------+-----------------------+
| ``flat``              | string                | Квартира              |
+-----------------------+-----------------------+-----------------------+
| ``flat_area``         | string                | Площадь квартиры      |
+-----------------------+-----------------------+-----------------------+
| ``flat_price``        | string                | Рыночная стоимость    |
|                       |                       | квартиры              |
+-----------------------+-----------------------+-----------------------+
| ``flat_type``         | string                | Тип квартиры          |
|                       |                       | (сокращенный)         |
+-----------------------+-----------------------+-----------------------+
| ``flat_type_full``    | string                | Тип квартиры          |
+-----------------------+-----------------------+-----------------------+
| ``house``             | string                | Дом                   |
+-----------------------+-----------------------+-----------------------+
| ``house_fias_id``     | string                | Код ФИАС дома         |
+-----------------------+-----------------------+-----------------------+
| ``house_kladr_id``    | string                | Код КЛАДР дома        |
+-----------------------+-----------------------+-----------------------+
| ``house_type``        | string                | Тип дома              |
|                       |                       | (сокращенный)         |
+-----------------------+-----------------------+-----------------------+
| ``house_type_full``   | string                | Тип дома              |
+-----------------------+-----------------------+-----------------------+
| ``house_with_type``   | string                | Дом с типом           |
+-----------------------+-----------------------+-----------------------+
| ``id``                | string                | ``???``               |
+-----------------------+-----------------------+-----------------------+
| ``kladr_id``          | string                | Код КЛАДР             |
+-----------------------+-----------------------+-----------------------+
| ``location``          | `Lo                   | `Описание типа        |
|                       | cation <#Location>`__ | Lo                    |
|                       |                       | cation <#Location>`__ |
+-----------------------+-----------------------+-----------------------+
| ``okato``             | string                | Код ОКАТО             |
+-----------------------+-----------------------+-----------------------+
| ``oktmo``             | string                | Код ОКТМО             |
+-----------------------+-----------------------+-----------------------+
| ``postal_box``        | string                | Абонентский ящик      |
+-----------------------+-----------------------+-----------------------+
| ``postal_code``       | string                | Индекс                |
+-----------------------+-----------------------+-----------------------+
| ``qc``                | integer (int32)       | ``???``               |
+-----------------------+-----------------------+-----------------------+
| ``qc_complete``       | integer (int32)       | ``???``               |
+-----------------------+-----------------------+-----------------------+
| ``qc_geo``            | integer (int32)       | Код точности          |
|                       |                       | координат:, ``0`` -   |
|                       |                       | точные координаты,    |
|                       |                       | ``1`` - ближайший     |
|                       |                       | дом, ``2`` - улица,   |
|                       |                       | ``3`` - населенный    |
|                       |                       | пункт, ``4`` - город, |
|                       |                       | ``5`` - координаты не |
|                       |                       | определены            |
+-----------------------+-----------------------+-----------------------+
| ``qc_house``          | integer (int32)       | ``???``               |
+-----------------------+-----------------------+-----------------------+
| ``region``            | string                | Регион                |
+-----------------------+-----------------------+-----------------------+
| ``region_fias_id``    | string                | Код ФИАС региона      |
+-----------------------+-----------------------+-----------------------+
| ``region_kladr_id``   | string                | Код КЛАДР региона     |
+-----------------------+-----------------------+-----------------------+
| ``region_type``       | string                | Тип региона           |
|                       |                       | (сокращенный)         |
+-----------------------+-----------------------+-----------------------+
| ``region_type_full``  | string                | Тип региона           |
+-----------------------+-----------------------+-----------------------+
| ``region_with_type``  | string                | Регион с типом        |
+-----------------------+-----------------------+-----------------------+
| ``result``            | string                | ``???``               |
+-----------------------+-----------------------+-----------------------+
| ``settlement``        | string                | Населенный пункт      |
+-----------------------+-----------------------+-----------------------+
| `                     | string                | Код ФИАС нас. пункта  |
| `settlement_fias_id`` |                       |                       |
+-----------------------+-----------------------+-----------------------+
| ``                    | string                | Код КЛАДР нас. пункта |
| settlement_kladr_id`` |                       |                       |
+-----------------------+-----------------------+-----------------------+
| ``settlement_type``   | string                | Тип населенного       |
|                       |                       | пункта (сокращенный)  |
+-----------------------+-----------------------+-----------------------+
| ``s                   | string                | Тип населенного       |
| ettlement_type_full`` |                       | пункта                |
+-----------------------+-----------------------+-----------------------+
| ``s                   | string                | Населенный пункт с    |
| ettlement_with_type`` |                       | типом                 |
+-----------------------+-----------------------+-----------------------+
| ``source``            | string                | ``???``               |
+-----------------------+-----------------------+-----------------------+
| `                     | string                | Рыночная стоимость м² |
| `square_meter_price`` |                       |                       |
+-----------------------+-----------------------+-----------------------+
| ``street``            | string                | Улица                 |
+-----------------------+-----------------------+-----------------------+
| ``street_fias_id``    | string                | Код ФИАС улицы        |
+-----------------------+-----------------------+-----------------------+
| ``street_kladr_id``   | string                | Код КЛАДР улицы       |
+-----------------------+-----------------------+-----------------------+
| ``street_type``       | string                | Тип улицы             |
|                       |                       | (сокращенный)         |
+-----------------------+-----------------------+-----------------------+
| ``street_type_full``  | string                | Тип улицы             |
+-----------------------+-----------------------+-----------------------+
| ``street_with_type``  | string                | Улица с типом         |
+-----------------------+-----------------------+-----------------------+
| ``tax_office``        | string                | Код ИФНС для          |
|                       |                       | физических лиц        |
+-----------------------+-----------------------+-----------------------+
| ``tax_office_legal``  | string                | Код ИФНС для          |
|                       |                       | организаций           |
+-----------------------+-----------------------+-----------------------+
| ``timezone``          | string                | Часовой пояс          |
+-----------------------+-----------------------+-----------------------+
| ``unparsed_parts``    | string                | Нераспознанные части  |
|                       |                       | адреса                |
+-----------------------+-----------------------+-----------------------+

.. _company-1:

Company
~~~~~~~

Адрес: ``/crm/company``

======== ============================ ========
Параметр Тип                          Описание
======== ============================ ========
INN      string                       ИНН
OGRN     string                       ОГРН
doc      `CompanyDoc <#CompanyDoc>`__ 
======== ============================ ========

CompanyDoc
~~~~~~~~~~

legalentity
^^^^^^^^^^^

+-----------------+-----------------+-----------------+-----------------+
| Параметр        | Тип             | Описание        | Источник        |
+=================+=================+=================+=================+
| ``com           | string          | Тип сущности    | ‘legalentity’   |
| panyTypeRefId`` |                 |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``displayName`` | string          | Наименование    | req.UL.         |
|                 |                 | организации     | legalName.short |
|                 |                 |                 | ?:              |
|                 |                 |                 | req.UL          |
|                 |                 |                 | .legalName.full |
+-----------------+-----------------+-----------------+-----------------+
| ``shortName``   | string          | Краткое         | req.UL.         |
|                 |                 | наименование    | legalName.short |
|                 |                 | организации     | ?:              |
|                 |                 |                 | req.UL          |
|                 |                 |                 | .legalName.full |
+-----------------+-----------------+-----------------+-----------------+
| ``fullName``    | string          | Полное          | req.UL          |
|                 |                 | наименование    | .legalName.full |
|                 |                 | организации     |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``shortNameEn`` | string          | ``???``         | req             |
|                 |                 |                 | .UL.shortNameEn |
+-----------------+-----------------+-----------------+-----------------+
| ``INN``         | string          | ИНН             | req.inn         |
+-----------------+-----------------+-----------------+-----------------+
| ``KPP``         | string          | КПП             | req.UL.kpp      |
+-----------------+-----------------+-----------------+-----------------+
| ``OKOGU``       | string          | Код ОКОГУ       | req.UL.okogu    |
+-----------------+-----------------+-----------------+-----------------+
| ``OGRN``        | string          | ОГРН            | req.ogrn        |
+-----------------+-----------------+-----------------+-----------------+
| ``ogrnDate``    | date            | Дата присвоения | e               |
|                 |                 | ОГРН            | grDetails.UL.re |
|                 |                 |                 | gInfo?.ogrnDate |
+-----------------+-----------------+-----------------+-----------------+
| ``OKPO``        | string          | Код ОКПО        | req.UL.okpo     |
+-----------------+-----------------+-----------------+-----------------+
| ``OKOPF``       | string          | Код ОКОПФ       | req.UL.okopf    |
+-----------------+-----------------+-----------------+-----------------+
| ``OKATO``       | string          | Код ОКАТО       | req.UL.okato    |
+-----------------+-----------------+-----------------+-----------------+
| ``OPF``         | string          | Наименование    | req.UL.opf      |
|                 |                 | организа        |                 |
|                 |                 | ционно-правовой |                 |
|                 |                 | формы           |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``OKFS``        | string          | Код ОКФС        | req.UL.okfs     |
+-----------------+-----------------+-----------------+-----------------+
| ``OKTMO``       | string          | Код ОКТМО       | req.UL.oktmo    |
+-----------------+-----------------+-----------------+-----------------+
| ``reg           | date            | Дата            | req.UL.r        |
| istrationDate`` |                 | образования     | egistrationDate |
+-----------------+-----------------+-----------------+-----------------+
| ``status``      | string          | Не              | req.UL.statu    |
|                 |                 | формализованное | s?.statusString |
|                 |                 | описание        |                 |
|                 |                 | статуса         |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``IFNS``        | object          |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``IFNS.code``   | string          | Код налогового  | egrDet          |
|                 |                 | органа          | ails.UL.nalogRe |
|                 |                 |                 | gBody.nalogCode |
+-----------------+-----------------+-----------------+-----------------+
| ``IFNS.name``   | string          | Наименование    | egrDet          |
|                 |                 | налогового      | ails.UL.nalogRe |
|                 |                 | органа          | gBody.nalogName |
+-----------------+-----------------+-----------------+-----------------+
| ``IFNS          | date            | Дата постановки | egrDetail       |
| .nalogRegDate`` |                 | на учет         | s.UL.nalogRegBo |
|                 |                 |                 | dy.nalogRegDate |
+-----------------+-----------------+-----------------+-----------------+
| ``IFNS          | string          | Адрес           | egrDetail       |
| .nalogAddress`` |                 | регистрирующего | s.UL.nalogRegBo |
|                 |                 | органа          | dy.nalogAddress |
+-----------------+-----------------+-----------------+-----------------+
| `               | string          | Код             | e               |
| `IFNS.regCode`` |                 | регистрирующего | grDetails.UL.re |
|                 |                 | органа          | gBody.nalogCode |
+-----------------+-----------------+-----------------+-----------------+
| `               | string          | Наименование    | e               |
| `IFNS.regName`` |                 | регистрирующего | grDetails.UL.re |
|                 |                 | органа          | gBody.nalogName |
+-----------------+-----------------+-----------------+-----------------+
| `               | string          | Дата постановки | egrDetails.     |
| `IFNS.regDate`` |                 | на учет         | UL.regBody.date |
+-----------------+-----------------+-----------------+-----------------+
| ``IFN           | object          | Сертификат о    |                 |
| S.certificate`` |                 | внесении записи |                 |
|                 |                 | в ЕГР           |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``IFNS.certi    | string          | Номер           |                 |
| ficate.number`` |                 |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``IFNS.certi    | string          | Серия           |                 |
| ficate.series`` |                 |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``IFNS.cer      | date            | Дата            |                 |
| tificate.date`` |                 |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| `               | string          | Код основного   | egrD            |
| `primaryOKVED`` |                 | вида            | etails.UL.activ |
|                 |                 | деятельности    | ities?.principa |
|                 |                 |                 | lActivity?.code |
+-----------------+-----------------+-----------------+-----------------+
| ``pri           | string          | Название        | egrD            |
| maryOKVEDtext`` |                 | основного вида  | etails.UL.activ |
|                 |                 | деятельности    | ities?.principa |
|                 |                 |                 | lActivity?.text |
+-----------------+-----------------+-----------------+-----------------+
| ``pri           | date            | Дата            | egrD            |
| maryOKVEDdate`` |                 |                 | etails.UL.activ |
|                 |                 |                 | ities?.principa |
|                 |                 |                 | lActivity?.date |
+-----------------+-----------------+-----------------+-----------------+
| ``OKVED``       | array           | Дополнительные  |                 |
|                 |                 | виды            |                 |
|                 |                 | деятельности    |                 |
+-----------------+-----------------+-----------------+-----------------+
| `               | string          | Код вида        | egrDetai        |
| `OKVED[].code`` |                 | деятельности    | ls.UL.activitie |
|                 |                 |                 | s.complementary |
|                 |                 |                 | Activities.code |
+-----------------+-----------------+-----------------+-----------------+
| `               | string          | Название вида   | egrDetai        |
| `OKVED[].text`` |                 | деятельности    | ls.UL.activitie |
|                 |                 |                 | s.complementary |
|                 |                 |                 | Activities.text |
+-----------------+-----------------+-----------------+-----------------+
| `               | date            | Дата            | egrDetai        |
| `OKVED[].date`` |                 |                 | ls.UL.activitie |
|                 |                 |                 | s.complementary |
|                 |                 |                 | Activities.date |
+-----------------+-----------------+-----------------+-----------------+
| ``              | object          | Уставный        |                 |
| statedCapital`` |                 | капитал         |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``state         | date            | Дата            | eg              |
| dCapital.date`` |                 |                 | rDetails.UL.sta |
|                 |                 |                 | tedCapital.date |
+-----------------+-----------------+-----------------+-----------------+
| ``stat          | integer         | Сумма в рублях  | (e              |
| edCapital.sum`` |                 |                 | grDetails.UL.st |
|                 |                 |                 | atedCapital.sum |
|                 |                 |                 | ?: 0) \* 100L   |
+-----------------+-----------------+-----------------+-----------------+
| ``staffInfo``   | object          |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``staffInfo.    | integer         | Среднесписочная | analy           |
| averageNumber`` |                 | численность     | ticsResponse?.a |
|                 |                 | работников за   | nalytics?.q7022 |
|                 |                 | календарный год | ?: 0            |
+-----------------+-----------------+-----------------+-----------------+
| ``f             | object          |                 | analytics       |
| ocusAnalytics`` |                 |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``focusAn       | boolean         | Дата включения  | analytics.m7023 |
| alytics.m7023`` |                 | лица в реестр   |                 |
|                 |                 | субъектов       |                 |
|                 |                 | малого и        |                 |
|                 |                 | среднего        |                 |
|                 |                 | пред            |                 |
|                 |                 | принимательства |                 |
|                 |                 | (ФНС)           |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``focusAn       | boolean         | Наличие         | analytics.m7024 |
| alytics.m7024`` |                 | категории       |                 |
|                 |                 | малого          |                 |
|                 |                 | предприятия в   |                 |
|                 |                 | едином реестре  |                 |
|                 |                 | субъектов       |                 |
|                 |                 | малого и        |                 |
|                 |                 | среднего        |                 |
|                 |                 | пред            |                 |
|                 |                 | принимательства |                 |
|                 |                 | (ФНС)           |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``focusAn       | boolean         | Наличие         | analytics.m7025 |
| alytics.m7025`` |                 | категории       |                 |
|                 |                 | среднего        |                 |
|                 |                 | предприятия в   |                 |
|                 |                 | едином реестре  |                 |
|                 |                 | субъектов       |                 |
|                 |                 | малого и        |                 |
|                 |                 | среднего        |                 |
|                 |                 | пред            |                 |
|                 |                 | принимательства |                 |
|                 |                 | (ФНС)           |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``licenses``    | array           |                 | flt_licenses    |
+-----------------+-----------------+-----------------+-----------------+
| ``lice          | string          | Номер лицензии  | lice            |
| nses[].number`` |                 | (если известен) | nse.officialNum |
+-----------------+-----------------+-----------------+-----------------+
| `               | string          | Название        | lic             |
| `licenses[].iss |                 | органа,         | ense.issuerName |
| uingAuthority`` |                 | выдавшего       |                 |
|                 |                 | лицензию (если  |                 |
|                 |                 | известен)       |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``licenses      | date            | Дата лицензии   | li              |
| [].issuedDate`` |                 | (если известна) | cense.dateStart |
|                 |                 | - чаще всего    | ?: license.date |
|                 |                 | совпадает с     |                 |
|                 |                 | датой начала    |                 |
|                 |                 | действия        |                 |
|                 |                 | лицензии        |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``licenses[     | date            | Дата окончания  | license.dateEnd |
| ].expiredDate`` |                 | действия        |                 |
|                 |                 | лицензии (если  |                 |
|                 |                 | известна)       |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``licenses      | string          | Описание вида   | l               |
| [].activities`` |                 | деятельности    | icense.activity |
+-----------------+-----------------+-----------------+-----------------+
| ``licens        | array           | Описание видов  | l               |
| es[].services`` |                 | работ/услуг     | icense.services |
+-----------------+-----------------+-----------------+-----------------+
| ``www``         |                 | ``???``         | eg              |
|                 |                 |                 | rDetails.UL.www |
+-----------------+-----------------+-----------------+-----------------+

individual
^^^^^^^^^^

+-----------------+-----------------+-----------------+-----------------+
| Параметр        | Тип             | Описание        | Источник        |
+=================+=================+=================+=================+
| ``com           | string          | Тип сущности    | ‘individual’    |
| panyTypeRefId`` |                 |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``displayName`` | string          | Наименование ИП | St              |
|                 |                 |                 | ringBuilder().a |
|                 |                 |                 | ppend(“ИП”).app |
|                 |                 |                 | end(req.IP.fio) |
+-----------------+-----------------+-----------------+-----------------+
| ``shortName``   | string          | Краткое         | St              |
|                 |                 | наименование ИП | ringBuilder().a |
|                 |                 |                 | ppend(“ИП”).app |
|                 |                 |                 | end(req.IP.fio) |
+-----------------+-----------------+-----------------+-----------------+
| ``fullName``    | string          | Полное          | req.IP.fio      |
|                 |                 | наименование ИП |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``INN``         | string          | ИНН             | req.inn         |
+-----------------+-----------------+-----------------+-----------------+
| ``OGRN``        | string          | ОГРН            | req.ogrn        |
+-----------------+-----------------+-----------------+-----------------+
| ``ogrnDate``    | date            | Дата присвоения | e               |
|                 |                 | ОГРН            | grDetails.UL.re |
|                 |                 |                 | gInfo?.ogrnDate |
+-----------------+-----------------+-----------------+-----------------+
| ``OKPO``        | string          | Код ОКПО        | req.UL.okpo     |
+-----------------+-----------------+-----------------+-----------------+
| ``OKOPF``       | string          | Код ОКОПФ       | req.UL.okopf    |
+-----------------+-----------------+-----------------+-----------------+
| ``OKATO``       | string          | Код ОКАТО       | req.UL.okato    |
+-----------------+-----------------+-----------------+-----------------+
| ``OPF``         | string          | Наименование    | req.UL.opf      |
|                 |                 | организа        |                 |
|                 |                 | ционно-правовой |                 |
|                 |                 | формы           |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``OKFS``        | string          | Код ОКФС        | req.UL.okfs     |
+-----------------+-----------------+-----------------+-----------------+
| ``OKTMO``       | string          | Код ОКТМО       | req.UL.oktmo    |
+-----------------+-----------------+-----------------+-----------------+
| ``reg           | date            | Дата            | req.UL.r        |
| istrationDate`` |                 | образования     | egistrationDate |
+-----------------+-----------------+-----------------+-----------------+
| ``status``      | string          | Не              | req.UL.statu    |
|                 |                 | формализованное | s?.statusString |
|                 |                 | описание        |                 |
|                 |                 | статуса         |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``IFNS``        | object          |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``IFNS.code``   | string          | Код налогового  | egrDet          |
|                 |                 | органа          | ails.UL.nalogRe |
|                 |                 |                 | gBody.nalogCode |
+-----------------+-----------------+-----------------+-----------------+
| ``IFNS.name``   | string          | Наименование    | egrDet          |
|                 |                 | налогового      | ails.UL.nalogRe |
|                 |                 | органа          | gBody.nalogName |
+-----------------+-----------------+-----------------+-----------------+
| ``IFNS          | date            | Дата постановки | egrDetail       |
| .nalogRegDate`` |                 | на учет         | s.UL.nalogRegBo |
|                 |                 |                 | dy.nalogRegDate |
+-----------------+-----------------+-----------------+-----------------+
| ``IFNS          | string          | Адрес           | egrDetail       |
| .nalogAddress`` |                 | регистрирующего | s.UL.nalogRegBo |
|                 |                 | органа          | dy.nalogAddress |
+-----------------+-----------------+-----------------+-----------------+
| `               | string          | Код             | e               |
| `IFNS.regCode`` |                 | регистрирующего | grDetails.UL.re |
|                 |                 | органа          | gBody.nalogCode |
+-----------------+-----------------+-----------------+-----------------+
| `               | string          | Наименование    | e               |
| `IFNS.regName`` |                 | регистрирующего | grDetails.UL.re |
|                 |                 | органа          | gBody.nalogName |
+-----------------+-----------------+-----------------+-----------------+
| `               | date            | Дата постановки | egrDetails.     |
| `IFNS.regDate`` |                 | на учет         | UL.regBody.date |
+-----------------+-----------------+-----------------+-----------------+
| ``IFN           | object          | Сертификат о    |                 |
| S.certificate`` |                 | внесении записи |                 |
|                 |                 | в ЕГР           |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``IFNS.certi    | string          | Номер           |                 |
| ficate.number`` |                 |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``IFNS.certi    | string          | Серия           |                 |
| ficate.series`` |                 |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``IFNS.cer      | date            | Дата            |                 |
| tificate.date`` |                 |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| `               | string          | Код основного   | egrD            |
| `primaryOKVED`` |                 | вида            | etails.UL.activ |
|                 |                 | деятельности    | ities?.principa |
|                 |                 |                 | lActivity?.code |
+-----------------+-----------------+-----------------+-----------------+
| ``pri           | string          | Название        | egrD            |
| maryOKVEDtext`` |                 | основного вида  | etails.UL.activ |
|                 |                 | деятельности    | ities?.principa |
|                 |                 |                 | lActivity?.text |
+-----------------+-----------------+-----------------+-----------------+
| ``pri           | date            | Дата            | egrD            |
| maryOKVEDdate`` |                 |                 | etails.UL.activ |
|                 |                 |                 | ities?.principa |
|                 |                 |                 | lActivity?.date |
+-----------------+-----------------+-----------------+-----------------+
| ``OKVED``       | array           | Дополнительные  |                 |
|                 |                 | виды            |                 |
|                 |                 | деятельности    |                 |
+-----------------+-----------------+-----------------+-----------------+
| `               | string          | Код вида        | egrDetai        |
| `OKVED[].code`` |                 | деятельности    | ls.UL.activitie |
|                 |                 |                 | s.complementary |
|                 |                 |                 | Activities.code |
+-----------------+-----------------+-----------------+-----------------+
| `               | string          | Название вида   | egrDetai        |
| `OKVED[].text`` |                 | деятельности    | ls.UL.activitie |
|                 |                 |                 | s.complementary |
|                 |                 |                 | Activities.text |
+-----------------+-----------------+-----------------+-----------------+
| `               | date            | Дата            | egrDetai        |
| `OKVED[].date`` |                 |                 | ls.UL.activitie |
|                 |                 |                 | s.complementary |
|                 |                 |                 | Activities.date |
+-----------------+-----------------+-----------------+-----------------+
| ``staffInfo``   | object          |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``staffInfo.    | integer         | Среднесписочная | analy           |
| averageNumber`` |                 | численность     | ticsResponse?.a |
|                 |                 | работников за   | nalytics?.q7022 |
|                 |                 | календарный год | ?: 0            |
+-----------------+-----------------+-----------------+-----------------+
| ``f             | object          |                 | analytics       |
| ocusAnalytics`` |                 |                 |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``focusAn       | boolean         | Дата включения  | analytics.m7023 |
| alytics.m7023`` |                 | лица в реестр   |                 |
|                 |                 | субъектов       |                 |
|                 |                 | малого и        |                 |
|                 |                 | среднего        |                 |
|                 |                 | пред            |                 |
|                 |                 | принимательства |                 |
|                 |                 | (ФНС)           |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``focusAn       | boolean         | Наличие         | analytics.m7024 |
| alytics.m7024`` |                 | категории       |                 |
|                 |                 | малого          |                 |
|                 |                 | предприятия в   |                 |
|                 |                 | едином реестре  |                 |
|                 |                 | субъектов       |                 |
|                 |                 | малого и        |                 |
|                 |                 | среднего        |                 |
|                 |                 | пред            |                 |
|                 |                 | принимательства |                 |
|                 |                 | (ФНС)           |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``focusAn       | boolean         | Наличие         | analytics.m7025 |
| alytics.m7025`` |                 | категории       |                 |
|                 |                 | среднего        |                 |
|                 |                 | предприятия в   |                 |
|                 |                 | едином реестре  |                 |
|                 |                 | субъектов       |                 |
|                 |                 | малого и        |                 |
|                 |                 | среднего        |                 |
|                 |                 | пред            |                 |
|                 |                 | принимательства |                 |
|                 |                 | (ФНС)           |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``licenses``    | array           |                 | flt_licenses    |
+-----------------+-----------------+-----------------+-----------------+
| ``lice          | string          | Номер лицензии  | lice            |
| nses[].number`` |                 | (если известен) | nse.officialNum |
+-----------------+-----------------+-----------------+-----------------+
| `               | string          | Название        | lic             |
| `licenses[].iss |                 | органа,         | ense.issuerName |
| uingAuthority`` |                 | выдавшего       |                 |
|                 |                 | лицензию (если  |                 |
|                 |                 | известен)       |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``licenses      | date            | Дата лицензии   | li              |
| [].issuedDate`` |                 | (если известна) | cense.dateStart |
|                 |                 | - чаще всего    | ?: license.date |
|                 |                 | совпадает с     |                 |
|                 |                 | датой начала    |                 |
|                 |                 | действия        |                 |
|                 |                 | лицензии        |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``licenses[     | date            | Дата окончания  | license.dateEnd |
| ].expiredDate`` |                 | действия        |                 |
|                 |                 | лицензии (если  |                 |
|                 |                 | известна)       |                 |
+-----------------+-----------------+-----------------+-----------------+
| ``licenses      | string          | Описание вида   | l               |
| [].activities`` |                 | деятельности    | icense.activity |
+-----------------+-----------------+-----------------+-----------------+
| ``licens        | array           | Описание видов  | l               |
| es[].services`` |                 | работ/услуг     | icense.services |
+-----------------+-----------------+-----------------+-----------------+

Модель данных KoturFocus
------------------------

req
~~~

Актуализация реквизитов

Адрес сервиса:
```https://focus-api.kontur.ru/api3/req`` <https://focus-api.kontur.ru/api3/req>`__

date
^^^^

======== === ========
Параметр Тип Описание
======== === ========
======== === ========

toponym
^^^^^^^

================= ====== ==================================
Параметр          Тип    Описание
================= ====== ==================================
``topoShortName`` string Краткое наименование вида топонима
``topoFullName``  string Полное наименование вида топонима
``topoValue``     string Значение топонима
================= ====== ==================================

parsedAddressRF
^^^^^^^^^^^^^^^

============== ====== ========================================
Параметр       Тип    Описание
============== ====== ========================================
``zipCode``    string Индекс
``regionCode`` string Код региона
``regionName`` object Регион
``district``   object Район
``city``       object Город
``settlement`` object Населенный пункт
``street``     object Улица
``house``      object Дом
``bulk``       object Корпус
``flat``       object Офис/квартира/комната
``kladrCode``  string Код КЛАДР
``houseRaw``   string Полное значение поля “Дом” из ЕГРЮЛ
``bulkRaw``    string Полное значение поля “Корпус” из ЕГРЮЛ
``flatRaw``    string Полное значение поля “Квартира” из ЕГРЮЛ
============== ====== ========================================

foreignAddress
^^^^^^^^^^^^^^

================= ====== ========================
Параметр          Тип    Описание
================= ====== ========================
``countryName``   string Наименование страны
``addressString`` string Строка, содержащая адрес
================= ====== ========================

legalAddress
^^^^^^^^^^^^

+---------------------+------------------+---------------------------+
| Параметр            | Тип              | Описание                  |
+=====================+==================+===========================+
| ``parsedAddressRF`` | object           | Разобранный на            |
|                     |                  | составляющие адрес в РФ   |
+---------------------+------------------+---------------------------+
| ``date``            | `date <#date>`__ | `Описание типа            |
|                     |                  | date <#date>`__           |
+---------------------+------------------+---------------------------+
| ``firstDate``       | `date <#date>`__ | `Описание типа            |
|                     |                  | date <#date>`__           |
+---------------------+------------------+---------------------------+

branch
^^^^^^

+---------------------+------------------+---------------------------+
| Параметр            | Тип              | Описание                  |
+=====================+==================+===========================+
| ``name``            | string           | Наименование филиала или  |
|                     |                  | представительства         |
+---------------------+------------------+---------------------------+
| ``kpp``             | string           | КПП                       |
+---------------------+------------------+---------------------------+
| ``parsedAddressRF`` | object           | Разобранный на            |
|                     |                  | составляющие адрес в РФ   |
+---------------------+------------------+---------------------------+
| ``foreignAddress``  | object           | Адрес вне РФ              |
+---------------------+------------------+---------------------------+
| ``date``            | `date <#date>`__ | `Описание типа            |
|                     |                  | date <#date>`__           |
+---------------------+------------------+---------------------------+

head
^^^^

============= ================ ==============================
Параметр      Тип              Описание
============= ================ ==============================
``fio``       string           ФИО
``innfl``     string           ИННФЛ
``position``  string           Должность
``date``      `date <#date>`__ `Описание типа date <#date>`__
``firstDate`` `date <#date>`__ `Описание типа date <#date>`__
============= ================ ==============================

managementCompany
^^^^^^^^^^^^^^^^^

+---------------+------------------+--------------------------------------------+
| Параметр      | Тип              | Описание                                   |
+===============+==================+============================================+
| ``name``      | string           | Наименование управляющей организации       |
+---------------+------------------+--------------------------------------------+
| ``inn``       | string           | ИНН управляющей организации (если указан)  |
+---------------+------------------+--------------------------------------------+
| ``ogrn``      | string           | ОГРН управляющей организации (если указан) |
+---------------+------------------+--------------------------------------------+
| ``date``      | `date <#date>`__ | `Описание типа date <#date>`__             |
+---------------+------------------+--------------------------------------------+
| ``firstDate`` | `date <#date>`__ | `Описание типа date <#date>`__             |
+---------------+------------------+--------------------------------------------+

legalName
^^^^^^^^^

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``short``             | string                | Краткое наименование  |
|                       |                       | организации           |
+-----------------------+-----------------------+-----------------------+
| ``full``              | string                | Полное наименование   |
|                       |                       | организации           |
+-----------------------+-----------------------+-----------------------+
| ``readable``          | string                | Полное наименование,  |
|                       |                       | приведенное к нижнему |
|                       |                       | регистру с            |
|                       |                       | сокращением           |
|                       |                       | аббревиатур           |
+-----------------------+-----------------------+-----------------------+
| ``date``              | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+

kppWithDate
^^^^^^^^^^^

======== ================ ==============================
Параметр Тип              Описание
======== ================ ==============================
``kpp``  string           КПП
``date`` `date <#date>`__ `Описание типа date <#date>`__
======== ================ ==============================

definitions
^^^^^^^^^^^

======== === ========
Параметр Тип Описание
======== === ========
======== === ========

items
^^^^^

UL
''

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``kpp``               | string                | КПП                   |
+-----------------------+-----------------------+-----------------------+
| ``okpo``              | string                | Код ОКПО              |
+-----------------------+-----------------------+-----------------------+
| ``okato``             | string                | Код ОКАТО             |
+-----------------------+-----------------------+-----------------------+
| ``okfs``              | string                | Код ОКФС              |
+-----------------------+-----------------------+-----------------------+
| ``oktmo``             | string                | Код ОКТМО             |
+-----------------------+-----------------------+-----------------------+
| ``okogu``             | string                | Код ОКОГУ             |
+-----------------------+-----------------------+-----------------------+
| ``okopf``             | string                | Код ОКОПФ             |
+-----------------------+-----------------------+-----------------------+
| ``opf``               | string                | Наименование          |
|                       |                       | ор                    |
|                       |                       | ганизационно-правовой |
|                       |                       | формы                 |
+-----------------------+-----------------------+-----------------------+
| ``legalName``         | object                | Наименование          |
|                       |                       | юридического лица     |
+-----------------------+-----------------------+-----------------------+
| ``legalAddress``      | object                | Юридический адрес     |
+-----------------------+-----------------------+-----------------------+
| ``branches``          | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``branches``\ []      | `branch <#branch>`__  | `Описание типа        |
|                       |                       | branch <#branch>`__   |
+-----------------------+-----------------------+-----------------------+
| ``registrationDate``  | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+
| ``dissolutionDate``   | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+
| ``heads``             | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``heads``\ []         | `head <#head>`__      | `Описание типа        |
|                       |                       | head <#head>`__       |
+-----------------------+-----------------------+-----------------------+
| ``                    | array                 |                       |
| managementCompanies`` |                       |                       |
+-----------------------+-----------------------+-----------------------+
| ``mana                | `managementCompany <# | `Описание типа        |
| gementCompanies``\ [] | managementCompany>`__ | managementCompany <#  |
|                       |                       | managementCompany>`__ |
+-----------------------+-----------------------+-----------------------+
| ``history``           | object                | ``???``               |
+-----------------------+-----------------------+-----------------------+

history
       

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``kpps``              | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``kpps``\ []          | `kppWithD             | `Описание типа        |
|                       | ate <#kppWithDate>`__ | kppWithD              |
|                       |                       | ate <#kppWithDate>`__ |
+-----------------------+-----------------------+-----------------------+
| ``legalNames``        | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``legalNames``\ []    | `lega                 | `Описание типа        |
|                       | lName <#legalName>`__ | lega                  |
|                       |                       | lName <#legalName>`__ |
+-----------------------+-----------------------+-----------------------+
| ``legalAddresses``    | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| `                     | `legalAddre           | `Описание типа        |
| `legalAddresses``\ [] | ss <#legalAddress>`__ | legalAddre            |
|                       |                       | ss <#legalAddress>`__ |
+-----------------------+-----------------------+-----------------------+
| ``branches``          | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``branches``\ []      | `branch <#branch>`__  | `Описание типа        |
|                       |                       | branch <#branch>`__   |
+-----------------------+-----------------------+-----------------------+
| ``                    | array                 |                       |
| managementCompanies`` |                       |                       |
+-----------------------+-----------------------+-----------------------+
| ``mana                | `managementCompany <# | `Описание типа        |
| gementCompanies``\ [] | managementCompany>`__ | managementCompany <#  |
|                       |                       | managementCompany>`__ |
+-----------------------+-----------------------+-----------------------+
| ``heads``             | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``heads``\ []         | `head <#head>`__      | `Описание типа        |
|                       |                       | head <#head>`__       |
+-----------------------+-----------------------+-----------------------+

IP
''

+----------------------+------------------+------------------------+
| Параметр             | Тип              | Описание               |
+======================+==================+========================+
| ``fio``              | string           | ФИО                    |
+----------------------+------------------+------------------------+
| ``okpo``             | string           | ОКПО                   |
+----------------------+------------------+------------------------+
| ``okato``            | string           | ОКАТО                  |
+----------------------+------------------+------------------------+
| ``okfs``             | string           | ОКФС                   |
+----------------------+------------------+------------------------+
| ``okogu``            | string           | ОКОГУ                  |
+----------------------+------------------+------------------------+
| ``okopf``            | string           | Код ОКОПФ              |
+----------------------+------------------+------------------------+
| ``opf``              | string           | Наименование           |
|                      |                  | о                      |
|                      |                  | рганизационно-правовой |
|                      |                  | формы                  |
+----------------------+------------------+------------------------+
| ``oktmo``            | string           | ОКТМО                  |
+----------------------+------------------+------------------------+
| ``registrationDate`` | `date <#date>`__ | `Описание типа         |
|                      |                  | date <#date>`__        |
+----------------------+------------------+------------------------+
| ``dissolutionDate``  | `date <#date>`__ | `Описание типа         |
|                      |                  | date <#date>`__        |
+----------------------+------------------+------------------------+

briefReport
'''''''''''

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``inn``               | string                | ИНН(ИП)               |
+-----------------------+-----------------------+-----------------------+
| ``ogrn``              | string                | ОГРН(ИП)              |
+-----------------------+-----------------------+-----------------------+
| ``focusHref``         | string                | Ссылка на карточку    |
|                       |                       | юридического лица     |
|                       |                       | (ИП) в Контур.Фокусе  |
|                       |                       | (для работы требуется |
|                       |                       | подписка на           |
|                       |                       | Контур.Фокус и        |
|                       |                       | дополнительная        |
|                       |                       | авторизация)          |
+-----------------------+-----------------------+-----------------------+
| ``UL``                | object                | Информация о          |
|                       |                       | юридическом лице      |
+-----------------------+-----------------------+-----------------------+
| ``IP``                | object                | Информация об         |
|                       |                       | индивидуальном        |
|                       |                       | предпринимателе       |
+-----------------------+-----------------------+-----------------------+
| ``briefReport``       | object                | Экспресс-отчет по     |
|                       |                       | контрагенту           |
+-----------------------+-----------------------+-----------------------+
| ``contactPhones``     | object                | Контактные телефоны   |
|                       |                       | из Контур.Справочника |
|                       |                       | (для получения        |
|                       |                       | контактов требуется   |
|                       |                       | отдельная подписка и  |
|                       |                       | вызов отдельного      |
|                       |                       | метода)               |
+-----------------------+-----------------------+-----------------------+

summary
       

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``greenStatements``   | boolean               | Наличие информации,   |
|                       |                       | помеченной зеленым    |
|                       |                       | цветом                |
+-----------------------+-----------------------+-----------------------+
| ``yellowStatements``  | boolean               | Наличие информации,   |
|                       |                       | помеченной желтым     |
|                       |                       | цветом                |
+-----------------------+-----------------------+-----------------------+
| ``redStatements``     | boolean               | Наличие информации,   |
|                       |                       | помеченной красным    |
|                       |                       | цветом                |
+-----------------------+-----------------------+-----------------------+

=========== ====== =====================================
Параметр    Тип    Описание
=========== ====== =====================================
``summary`` object Сводная информация из экспресс-отчета
=========== ====== =====================================

contactPhones
'''''''''''''

========= ======= ========================================
Параметр  Тип     Описание
========= ======= ========================================
``count`` integer Количество найденных контактых телефонов
========= ======= ========================================

egrDetails
~~~~~~~~~~

Расширенные сведения на основе ЕГРЮЛ/ЕГРИП

Адрес сервиса:
```https://focus-api.kontur.ru/api3/egrDetails`` <https://focus-api.kontur.ru/api3/egrDetails>`__

.. _date-1:

date
^^^^

======== === ========
Параметр Тип Описание
======== === ========
======== === ========

.. _toponym-1:

toponym
^^^^^^^

================= ====== ==================================
Параметр          Тип    Описание
================= ====== ==================================
``topoShortName`` string Краткое наименование вида топонима
``topoFullName``  string Полное наименование вида топонима
``topoValue``     string Значение топонима
================= ====== ==================================

activity
^^^^^^^^

======== ================ ==============================
Параметр Тип              Описание
======== ================ ==============================
``code`` string           Код вида деятельности
``text`` string           Название вида деятельности
``date`` `date <#date>`__ `Описание типа date <#date>`__
======== ================ ==============================

statedCapital
^^^^^^^^^^^^^

======== ================ ==============================
Параметр Тип              Описание
======== ================ ==============================
``sum``  number           Сумма в рублях
``date`` `date <#date>`__ `Описание типа date <#date>`__
======== ================ ==============================

share
^^^^^

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``sum``               | number                | Сумма в рублях        |
+-----------------------+-----------------------+-----------------------+
| ``percentagePlain``   | number                | Размер доли в         |
|                       |                       | процентах             |
+-----------------------+-----------------------+-----------------------+
| ``                    | integer               | Размер доли в виде    |
| percentageNominator`` |                       | простой дроби         |
|                       |                       | (числитель)           |
+-----------------------+-----------------------+-----------------------+
| ``pe                  | integer               | Размер доли в виде    |
| rcentageDenominator`` |                       | простой дроби         |
|                       |                       | (знаменатель)         |
+-----------------------+-----------------------+-----------------------+

notarization
^^^^^^^^^^^^

================== ================ ==============================
Параметр           Тип              Описание
================== ================ ==============================
``fio``            string           ФИО нотариуса
``innfl``          string           ИННФЛ нотариуса
``contractNumber`` string           Номер договора залога
``date``           `date <#date>`__ `Описание типа date <#date>`__
================== ================ ==============================

pledgeInfo
^^^^^^^^^^

============= ====== ==============================================
Параметр      Тип    Описание
============= ====== ==============================================
``duration``  string Срок обременения или порядок определения срока
``pledgeeUL`` object Залогодержатель — юрлицо
``pledgeeFL`` object Залогодержатель — физлицо
============= ====== ==============================================

pledgeeUL
'''''''''

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``name``              | string                | Краткое наименование  |
|                       |                       | юридического лица     |
+-----------------------+-----------------------+-----------------------+
| ``ogrn``              | string                | ОГРН                  |
+-----------------------+-----------------------+-----------------------+
| ``inn``               | string                | ИНН                   |
+-----------------------+-----------------------+-----------------------+
| ``notarization``      | `notarizati           | `Описание типа        |
|                       | on <#notarization>`__ | notarizati            |
|                       |                       | on <#notarization>`__ |
+-----------------------+-----------------------+-----------------------+

pledgeeFL
'''''''''

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``fio``               | string                | ФИО физического лица  |
+-----------------------+-----------------------+-----------------------+
| ``innfl``             | string                | ИННФЛ                 |
+-----------------------+-----------------------+-----------------------+
| ``notarization``      | `notarizati           | `Описание типа        |
|                       | on <#notarization>`__ | notarizati            |
|                       |                       | on <#notarization>`__ |
+-----------------------+-----------------------+-----------------------+

shareholderFL
^^^^^^^^^^^^^

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``fio``               | string                | ФИО                   |
+-----------------------+-----------------------+-----------------------+
| ``address``           | string                | Местожительство       |
|                       |                       | физлица               |
+-----------------------+-----------------------+-----------------------+
| ``                    | number                | Доля обыкновенных     |
| votingSharesPercent`` |                       | акций в процентах     |
+-----------------------+-----------------------+-----------------------+
| ``c                   | number                | Доля участия в        |
| apitalSharesPercent`` |                       | уставном капитале в   |
|                       |                       | процентах             |
+-----------------------+-----------------------+-----------------------+
| ``date``              | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+

shareholderUL
^^^^^^^^^^^^^

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``ogrn``              | string                | ОГРН                  |
+-----------------------+-----------------------+-----------------------+
| ``inn``               | string                | ИНН                   |
+-----------------------+-----------------------+-----------------------+
| ``fullName``          | string                | Полное наименование   |
|                       |                       | юридического лица     |
+-----------------------+-----------------------+-----------------------+
| ``address``           | string                | Местонахождение       |
|                       |                       | юрлица                |
+-----------------------+-----------------------+-----------------------+
| ``                    | number                | Доля обыкновенных     |
| votingSharesPercent`` |                       | акций в процентах     |
+-----------------------+-----------------------+-----------------------+
| ``c                   | number                | Доля участия в        |
| apitalSharesPercent`` |                       | уставном капитале в   |
|                       |                       | процентах             |
+-----------------------+-----------------------+-----------------------+
| ``date``              | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+

shareholderOther
^^^^^^^^^^^^^^^^

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``fullName``          | string                | Полное наименование   |
|                       |                       | лица                  |
+-----------------------+-----------------------+-----------------------+
| ``address``           | string                | Местонахождение       |
|                       |                       | юрлица или            |
|                       |                       | Местожительство       |
|                       |                       | физлица               |
+-----------------------+-----------------------+-----------------------+
| ``                    | number                | Доля обыкновенных     |
| votingSharesPercent`` |                       | акций в процентах     |
+-----------------------+-----------------------+-----------------------+
| ``c                   | number                | Доля участия в        |
| apitalSharesPercent`` |                       | уставном капитале в   |
|                       |                       | процентах             |
+-----------------------+-----------------------+-----------------------+
| ``date``              | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+

shareholders
^^^^^^^^^^^^

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``date``              | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+
| ``shareholdersFL``    | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| `                     | `shareholderF         | `Описание типа        |
| `shareholdersFL``\ [] | L <#shareholderFL>`__ | shareholderF          |
|                       |                       | L <#shareholderFL>`__ |
+-----------------------+-----------------------+-----------------------+
| ``shareholdersUL``    | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| `                     | `shareholderU         | `Описание типа        |
| `shareholdersUL``\ [] | L <#shareholderUL>`__ | shareholderU          |
|                       |                       | L <#shareholderUL>`__ |
+-----------------------+-----------------------+-----------------------+
| ``shareholdersOther`` | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``sh                  | `shareholderOther <   | `Описание типа        |
| areholdersOther``\ [] | #shareholderOther>`__ | shareholderOther <    |
|                       |                       | #shareholderOther>`__ |
+-----------------------+-----------------------+-----------------------+

founderFL
^^^^^^^^^

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``fio``               | string                | ФИО                   |
+-----------------------+-----------------------+-----------------------+
| ``innfl``             | string                | ИННФЛ                 |
+-----------------------+-----------------------+-----------------------+
| ``share``             | `share <#share>`__    | `Описание типа        |
|                       |                       | share <#share>`__     |
+-----------------------+-----------------------+-----------------------+
| ``pledges``           | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``pledges``\ []       | `pledge               | `Описание типа        |
|                       | Info <#pledgeInfo>`__ | pledge                |
|                       |                       | Info <#pledgeInfo>`__ |
+-----------------------+-----------------------+-----------------------+
| ``date``              | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+
| ``firstDate``         | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+

founderUL
^^^^^^^^^

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``ogrn``              | string                | ОГРН                  |
+-----------------------+-----------------------+-----------------------+
| ``inn``               | string                | ИНН                   |
+-----------------------+-----------------------+-----------------------+
| ``fullName``          | string                | Полное наименование   |
|                       |                       | юридического лица     |
+-----------------------+-----------------------+-----------------------+
| ``share``             | `share <#share>`__    | `Описание типа        |
|                       |                       | share <#share>`__     |
+-----------------------+-----------------------+-----------------------+
| ``pledges``           | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``pledges``\ []       | `pledge               | `Описание типа        |
|                       | Info <#pledgeInfo>`__ | pledge                |
|                       |                       | Info <#pledgeInfo>`__ |
+-----------------------+-----------------------+-----------------------+
| ``date``              | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+
| ``firstDate``         | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+

founderForeign
^^^^^^^^^^^^^^

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``fullName``          | string                | Полное наименование   |
|                       |                       | юридического лица     |
+-----------------------+-----------------------+-----------------------+
| ``country``           | string                | Страна                |
+-----------------------+-----------------------+-----------------------+
| ``share``             | `share <#share>`__    | `Описание типа        |
|                       |                       | share <#share>`__     |
+-----------------------+-----------------------+-----------------------+
| ``pledges``           | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``pledges``\ []       | `pledge               | `Описание типа        |
|                       | Info <#pledgeInfo>`__ | pledge                |
|                       |                       | Info <#pledgeInfo>`__ |
+-----------------------+-----------------------+-----------------------+
| ``date``              | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+
| ``firstDate``         | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+

nalogRegBody
^^^^^^^^^^^^

=================== ================ ==============================
Параметр            Тип              Описание
=================== ================ ==============================
``nalogCode``       string           Код налогового органа
``nalogName``       string           Наименование налогового органа
``nalogRegDate``    `date <#date>`__ `Описание типа date <#date>`__
``nalogRegAddress`` string           Адрес регистрирующего органа
``kpp``             string           КПП
``date``            `date <#date>`__ `Описание типа date <#date>`__
=================== ================ ==============================

egrRecord
^^^^^^^^^

+------------------------+------------------+------------------------+
| Параметр               | Тип              | Описание               |
+========================+==================+========================+
| ``grn``                | string           | ГРН записи             |
+------------------------+------------------+------------------------+
| ``date``               | `date <#date>`__ | `Описание типа         |
|                        |                  | date <#date>`__        |
+------------------------+------------------+------------------------+
| ``name``               | string           | Причина внесения       |
|                        |                  | записи                 |
+------------------------+------------------+------------------------+
| ``isInvalid``          | boolean          | Признак                |
|                        |                  | недействительности     |
|                        |                  | записи                 |
+------------------------+------------------+------------------------+
| ``invalidSince``       | `date <#date>`__ | `Описание типа         |
|                        |                  | date <#date>`__        |
+------------------------+------------------+------------------------+
| ``regName``            | string           | Имя регистрирующего    |
|                        |                  | органа, который внес   |
|                        |                  | запись                 |
+------------------------+------------------+------------------------+
| ``regCode``            | string           | Код регистрирующего    |
|                        |                  | органа, который внес   |
|                        |                  | запись                 |
+------------------------+------------------+------------------------+
| ``documents``          | array            |                        |
+------------------------+------------------+------------------------+
| ``documents``\ [].name | string           | Имя документа          |
+------------------------+------------------+------------------------+
| ``documents``\ [].date | `date <#date>`__ | `Описание типа         |
|                        |                  | date <#date>`__        |
+------------------------+------------------+------------------------+
| ``certificates``       | array            |                        |
+------------------------+------------------+------------------------+
| ``certifica            | string           | Серия и номер          |
| tes``\ [].serialNumber |                  |                        |
+------------------------+------------------+------------------------+
| ``c                    | `date <#date>`__ | `Описание типа         |
| ertificates``\ [].date |                  | date <#date>`__        |
+------------------------+------------------+------------------------+

.[]
^^^

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``inn``               | string                | ИНН(ИП)               |
+-----------------------+-----------------------+-----------------------+
| ``ogrn``              | string                | ОГРН(ИП)              |
+-----------------------+-----------------------+-----------------------+
| ``focusHref``         | string                | Ссылка на карточку    |
|                       |                       | юридического лица     |
|                       |                       | (ИП) в Контур.Фокусе  |
|                       |                       | (для работы требуется |
|                       |                       | подписка на           |
|                       |                       | Контур.Фокус и        |
|                       |                       | дополнительная        |
|                       |                       | авторизация)          |
+-----------------------+-----------------------+-----------------------+
| ``UL``                | object                | Информация о          |
|                       |                       | юридическом лице      |
+-----------------------+-----------------------+-----------------------+
| ``IP``                | object                | Информация об         |
|                       |                       | индивидуальном        |
|                       |                       | предпринимателе       |
+-----------------------+-----------------------+-----------------------+

.. _ul-1:

UL
''

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``okpo``              | string                | ОКПО                  |
+-----------------------+-----------------------+-----------------------+
| ``pfrRegNumber``      | string                | Регистрационный номер |
|                       |                       | ПФР                   |
+-----------------------+-----------------------+-----------------------+
| ``fssRegNumber``      | string                | Регистрационный номер |
|                       |                       | ФСС                   |
+-----------------------+-----------------------+-----------------------+
| ``fomsRegNumber``     | string                | Регистрационный номер |
|                       |                       | ФОМС                  |
+-----------------------+-----------------------+-----------------------+
| ``activities``        | object                | Виды деятельности     |
+-----------------------+-----------------------+-----------------------+
| ``regInfo``           | object                | Сведения о            |
|                       |                       | регистрации           |
+-----------------------+-----------------------+-----------------------+
| ``nalogRegBody``      | `nalogRegBo           | `Описание типа        |
|                       | dy <#nalogRegBody>`__ | nalogRegBo            |
|                       |                       | dy <#nalogRegBody>`__ |
+-----------------------+-----------------------+-----------------------+
| ``regBody``           | `nalogRegBo           | `Описание типа        |
|                       | dy <#nalogRegBody>`__ | nalogRegBo            |
|                       |                       | dy <#nalogRegBody>`__ |
+-----------------------+-----------------------+-----------------------+
| ``shareholders``      | `shareholde           | `Описание типа        |
|                       | rs <#shareholders>`__ | shareholde            |
|                       |                       | rs <#shareholders>`__ |
+-----------------------+-----------------------+-----------------------+
| ``statedCapital``     | `statedCapita         | `Описание типа        |
|                       | l <#statedCapital>`__ | statedCapita          |
|                       |                       | l <#statedCapital>`__ |
+-----------------------+-----------------------+-----------------------+
| ``foundersFL``        | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``foundersFL``\ []    | `foun                 | `Описание типа        |
|                       | derFL <#founderFL>`__ | foun                  |
|                       |                       | derFL <#founderFL>`__ |
+-----------------------+-----------------------+-----------------------+
| ``foundersUL``        | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``foundersUL``\ []    | `foun                 | `Описание типа        |
|                       | derUL <#founderUL>`__ | foun                  |
|                       |                       | derUL <#founderUL>`__ |
+-----------------------+-----------------------+-----------------------+
| ``foundersForeign``   | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``                    | `founderForeign       | `Описание типа        |
| foundersForeign``\ [] |  <#founderForeign>`__ | founderForeign        |
|                       |                       |  <#founderForeign>`__ |
+-----------------------+-----------------------+-----------------------+
| ``predecessors``      | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``successors``        | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``egrRecords``        | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``egrRecords``\ []    | object                | Записи в ЕГРЮЛ        |
+-----------------------+-----------------------+-----------------------+
| ``history``           | object                | ``???``               |
+-----------------------+-----------------------+-----------------------+

activities
          

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``principalActivity`` | `ac                   | `Описание типа        |
|                       | tivity <#activity>`__ | ac                    |
|                       |                       | tivity <#activity>`__ |
+-----------------------+-----------------------+-----------------------+
| ``comp                | array                 |                       |
| lementaryActivities`` |                       |                       |
+-----------------------+-----------------------+-----------------------+
| ``compleme            | `ac                   | `Описание типа        |
| ntaryActivities``\ [] | tivity <#activity>`__ | ac                    |
|                       |                       | tivity <#activity>`__ |
+-----------------------+-----------------------+-----------------------+
| ``okvedVersion``      | string                | Версия справочника    |
|                       |                       | ОКВЭД. Значение “2”   |
|                       |                       | соответствует ОК      |
|                       |                       | 029-2014 (КДЕС Ред.   |
|                       |                       | 2), отсутствие поля   |
|                       |                       | версии соответствует  |
|                       |                       | ОК 029-2001 (КДЕС     |
|                       |                       | Ред.1)                |
+-----------------------+-----------------------+-----------------------+

regInfo
       

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``ogrnDate``          | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+
| ``regName``           | string                | Наименование органа,  |
|                       |                       | зарегистрировавшего   |
|                       |                       | юридическое лицо до 1 |
|                       |                       | июля 2002 года        |
+-----------------------+-----------------------+-----------------------+
| ``regNum``            | string                | Регистрационный       |
|                       |                       | номер, присвоенный до |
|                       |                       | 1 июля 2002 года      |
+-----------------------+-----------------------+-----------------------+

.. _items-1:

items
     

======== ================ ==============================
Параметр Тип              Описание
======== ================ ==============================
``name`` string           Наименование организации
``inn``  string           ИНН
``ogrn`` string           ОГРН
``date`` `date <#date>`__ `Описание типа date <#date>`__
======== ================ ==============================

.. _items-2:

items
     

======== ================ ==============================
Параметр Тип              Описание
======== ================ ==============================
``name`` string           Наименование организации
``inn``  string           ИНН
``ogrn`` string           ОГРН
``date`` `date <#date>`__ `Описание типа date <#date>`__
======== ================ ==============================

.. _history-1:

history
       

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``shareholders``      | `shareholde           | `Описание типа        |
|                       | rs <#shareholders>`__ | shareholde            |
|                       |                       | rs <#shareholders>`__ |
+-----------------------+-----------------------+-----------------------+
| ``statedCapitals``    | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| `                     | `statedCapita         | `Описание типа        |
| `statedCapitals``\ [] | l <#statedCapital>`__ | statedCapita          |
|                       |                       | l <#statedCapital>`__ |
+-----------------------+-----------------------+-----------------------+
| ``foundersFL``        | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``foundersFL``\ []    | `foun                 | `Описание типа        |
|                       | derFL <#founderFL>`__ | foun                  |
|                       |                       | derFL <#founderFL>`__ |
+-----------------------+-----------------------+-----------------------+
| ``foundersUL``        | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``foundersUL``\ []    | `foun                 | `Описание типа        |
|                       | derUL <#founderUL>`__ | foun                  |
|                       |                       | derUL <#founderUL>`__ |
+-----------------------+-----------------------+-----------------------+
| ``foundersForeign``   | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``                    | `founderForeign       | `Описание типа        |
| foundersForeign``\ [] |  <#founderForeign>`__ | founderForeign        |
|                       |                       |  <#founderForeign>`__ |
+-----------------------+-----------------------+-----------------------+

.. _ip-1:

IP
''

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``okpo``              | string                | ОКПО                  |
+-----------------------+-----------------------+-----------------------+
| ``pfrRegNumber``      | string                | Регистрационный номер |
|                       |                       | ПФР                   |
+-----------------------+-----------------------+-----------------------+
| ``fssRegNumber``      | string                | Регистрационный номер |
|                       |                       | ФСС                   |
+-----------------------+-----------------------+-----------------------+
| ``fomsRegNumber``     | string                | Регистрационный номер |
|                       |                       | ФОМС                  |
+-----------------------+-----------------------+-----------------------+
| ``okato``             | string                | ОКАТО (может          |
|                       |                       | отсутствовать или     |
|                       |                       | устареть)             |
+-----------------------+-----------------------+-----------------------+
| ``shortenedAddress``  | object                | Информация о          |
|                       |                       | местонахождении ИП    |
|                       |                       | (может отсутствовать  |
|                       |                       | или устареть)         |
+-----------------------+-----------------------+-----------------------+
| ``activities``        | object                | Виды деятельности     |
+-----------------------+-----------------------+-----------------------+
| ``regInfo``           | object                | Сведения о            |
|                       |                       | регистрации           |
+-----------------------+-----------------------+-----------------------+
| ``nalogRegBody``      | `nalogRegBo           | `Описание типа        |
|                       | dy <#nalogRegBody>`__ | nalogRegBo            |
|                       |                       | dy <#nalogRegBody>`__ |
+-----------------------+-----------------------+-----------------------+
| ``regBody``           | `nalogRegBo           | `Описание типа        |
|                       | dy <#nalogRegBody>`__ | nalogRegBo            |
|                       |                       | dy <#nalogRegBody>`__ |
+-----------------------+-----------------------+-----------------------+
| ``egrRecords``        | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``egrRecords``\ []    | object                | Записи в ЕГРИП        |
+-----------------------+-----------------------+-----------------------+

shortenedAddress
                

+----------------+------------------------+--------------------------------------+
| Параметр       | Тип                    | Описание                             |
+================+========================+======================================+
| ``regionCode`` | string                 | Код региона                          |
+----------------+------------------------+--------------------------------------+
| ``regionName`` | `toponym <#toponym>`__ | `Описание типа toponym <#toponym>`__ |
+----------------+------------------------+--------------------------------------+
| ``district``   | `toponym <#toponym>`__ | `Описание типа toponym <#toponym>`__ |
+----------------+------------------------+--------------------------------------+
| ``city``       | `toponym <#toponym>`__ | `Описание типа toponym <#toponym>`__ |
+----------------+------------------------+--------------------------------------+
| ``settlement`` | `toponym <#toponym>`__ | `Описание типа toponym <#toponym>`__ |
+----------------+------------------------+--------------------------------------+

.. _activities-1:

activities
          

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``principalActivity`` | `ac                   | `Описание типа        |
|                       | tivity <#activity>`__ | ac                    |
|                       |                       | tivity <#activity>`__ |
+-----------------------+-----------------------+-----------------------+
| ``comp                | array                 |                       |
| lementaryActivities`` |                       |                       |
+-----------------------+-----------------------+-----------------------+
| ``compleme            | `ac                   | `Описание типа        |
| ntaryActivities``\ [] | tivity <#activity>`__ | ac                    |
|                       |                       | tivity <#activity>`__ |
+-----------------------+-----------------------+-----------------------+
| ``okvedVersion``      | string                | Версия справочника    |
|                       |                       | ОКВЭД                 |
+-----------------------+-----------------------+-----------------------+

.. _reginfo-1:

regInfo
       

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``ogrnDate``          | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+
| ``regName``           | string                | Наименование органа,  |
|                       |                       | зарегистрировавшего   |
|                       |                       | индивидуального       |
|                       |                       | предпринимателя до 1  |
|                       |                       | января 2004 года      |
+-----------------------+-----------------------+-----------------------+
| ``regNum``            | string                | Регистрационный       |
|                       |                       | номер, присвоенный до |
|                       |                       | 1 января 2004 года    |
+-----------------------+-----------------------+-----------------------+

licences
~~~~~~~~

Информация о лицензиях

Адрес сервиса:
```https://focus-api.kontur.ru/api3/licences`` <https://focus-api.kontur.ru/api3/licences>`__

.. _date-2:

date
^^^^

======== === ========
Параметр Тип Описание
======== === ========
======== === ========

.. _section-1:

.[]
^^^

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``inn``               | string                | ИНН(ИП)               |
+-----------------------+-----------------------+-----------------------+
| ``ogrn``              | string                | ОГРН(ИП)              |
+-----------------------+-----------------------+-----------------------+
| ``focusHref``         | string                | Ссылка на карточку    |
|                       |                       | юридического лица     |
|                       |                       | (ИП) в Контур.Фокусе  |
|                       |                       | (для работы требуется |
|                       |                       | подписка на           |
|                       |                       | Контур.Фокус и        |
|                       |                       | дополнительная        |
|                       |                       | авторизация)          |
+-----------------------+-----------------------+-----------------------+
| ``licenses``          | array                 |                       |
+-----------------------+-----------------------+-----------------------+
| ``                    | string                | Источник сведений о   |
| licenses``\ [].source |                       | лицензии (конкретный  |
|                       |                       | орган управления или  |
|                       |                       | ЕГРЮЛ)                |
+-----------------------+-----------------------+-----------------------+
| ``licen               | string                | Номер лицензии (если  |
| ses``\ [].officialNum |                       | известен)             |
+-----------------------+-----------------------+-----------------------+
| ``lice                | string                | Название органа,      |
| nses``\ [].issuerName |                       | выдавшего лицензию    |
|                       |                       | (если известен)       |
+-----------------------+-----------------------+-----------------------+
| ``licenses``\ [].date | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+
| ``lic                 | `date <#date>`__      | `Описание типа        |
| enses``\ [].dateStart |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+
| ``l                   | `date <#date>`__      | `Описание типа        |
| icenses``\ [].dateEnd |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+
| ``licenses``\         | string                | Строковое описание    |
|  [].statusDescription |                       | статуса (если         |
|                       |                       | известно)             |
+-----------------------+-----------------------+-----------------------+
| ``li                  | string                | Описание вида         |
| censes``\ [].activity |                       | деятельности          |
+-----------------------+-----------------------+-----------------------+
| ``li                  | array                 |                       |
| censes``\ [].services |                       |                       |
+-----------------------+-----------------------+-----------------------+
| ``lice                | string                | Описание видов        |
| nses``\ [].services[] |                       | работ/услуг           |
+-----------------------+-----------------------+-----------------------+
| ``lic                 | array                 |                       |
| enses``\ [].addresses |                       |                       |
+-----------------------+-----------------------+-----------------------+
| ``licen               | string                | Места действия        |
| ses``\ [].addresses[] |                       | лицензии (массив      |
|                       |                       | неформализованных     |
|                       |                       | строк)                |
+-----------------------+-----------------------+-----------------------+

analytics
~~~~~~~~~

.. _date-3:

date
^^^^

======== === ========
Параметр Тип Описание
======== === ========
======== === ========

.. _section-2:

.[]
^^^

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``inn``               | string                | ИНН(ИП)               |
+-----------------------+-----------------------+-----------------------+
| ``ogrn``              | string                | ОГРН(ИП)              |
+-----------------------+-----------------------+-----------------------+
| ``focusHref``         | string                | Ссылка на карточку    |
|                       |                       | юридического лица     |
|                       |                       | (ИП) в Контур.Фокусе  |
|                       |                       | (для работы требуется |
|                       |                       | подписка на           |
|                       |                       | Контур.Фокус и        |
|                       |                       | дополнительная        |
|                       |                       | авторизация)          |
+-----------------------+-----------------------+-----------------------+
| ``analytics``         | object                | Маркеры               |
|                       |                       | автоматической        |
|                       |                       | проверки и числовые   |
|                       |                       | индикаторы            |
+-----------------------+-----------------------+-----------------------+

.. _analytics-1:

analytics
'''''''''

Расширенная аналитика

Адрес сервиса:
```https://focus-api.kontur.ru/api3/analytics`` <https://focus-api.kontur.ru/api3/analytics>`__

+-----------------------+-----------------------+-----------------------+
| Параметр              | Тип                   | Описание              |
+=======================+=======================+=======================+
| ``q1001``             | integer               | Исп. пр-ва.           |
|                       |                       | Количество найденных  |
|                       |                       | исполнительных        |
|                       |                       | производств в         |
|                       |                       | отношении организаций |
|                       |                       | со схожими            |
|                       |                       | реквизитами (за 12    |
|                       |                       | последних месяцев)    |
+-----------------------+-----------------------+-----------------------+
| ``s1001``             | number                | Исп. пр-ва. Сумма (в  |
|                       |                       | рублях) найденных     |
|                       |                       | исполнительных        |
|                       |                       | производств в         |
|                       |                       | отношении организаций |
|                       |                       | со схожими            |
|                       |                       | реквизитами (за 12    |
|                       |                       | последних месяцев)    |
+-----------------------+-----------------------+-----------------------+
| ``q1002``             | integer               | Исп. пр-ва.           |
|                       |                       | Количество найденных  |
|                       |                       | исполнительных        |
|                       |                       | производств в         |
|                       |                       | отношении организаций |
|                       |                       | со схожими            |
|                       |                       | реквизитами (всего)   |
+-----------------------+-----------------------+-----------------------+
| ``s1002``             | number                | Исп. пр-ва. Сумма (в  |
|                       |                       | рублях) найденных     |
|                       |                       | исполнительных        |
|                       |                       | производств в         |
|                       |                       | отношении организаций |
|                       |                       | со схожими            |
|                       |                       | реквизитами (всего)   |
+-----------------------+-----------------------+-----------------------+
| ``m1003``             | boolean               | Исп. пр-ва. У         |
|                       |                       | организации со        |
|                       |                       | схожими реквизитами   |
|                       |                       | были найдены          |
|                       |                       | исполнительные        |
|                       |                       | производства,         |
|                       |                       | предметом которых     |
|                       |                       | является заработная   |
|                       |                       | плата                 |
+-----------------------+-----------------------+-----------------------+
| ``m1004``             | boolean               | Исп. пр-ва. У         |
|                       |                       | организации со        |
|                       |                       | схожими реквизитами   |
|                       |                       | были найдены          |
|                       |                       | исполнительные        |
|                       |                       | производства,         |
|                       |                       | предметом которых     |
|                       |                       | является наложение    |
|                       |                       | ареста                |
+-----------------------+-----------------------+-----------------------+
| ``m1005``             | boolean               | Исп. пр-ва. У         |
|                       |                       | организации со        |
|                       |                       | схожими реквизитами   |
|                       |                       | были найдены          |
|                       |                       | исполнительные        |
|                       |                       | производства,         |
|                       |                       | предметом которых     |
|                       |                       | является кредитные    |
|                       |                       | платежи               |
+-----------------------+-----------------------+-----------------------+
| ``m1006``             | boolean               | Исп. пр-ва. У         |
|                       |                       | организации со        |
|                       |                       | схожими реквизитами   |
|                       |                       | были найдены          |
|                       |                       | исполнительные        |
|                       |                       | производства,         |
|                       |                       | предметом которых     |
|                       |                       | является обращение    |
|                       |                       | взыскания на          |
|                       |                       | заложенное имущество  |
+-----------------------+-----------------------+-----------------------+
| ``s1007``             | number                | Исп. пр-ва. Сумма (в  |
|                       |                       | рублях) найденных     |
|                       |                       | исполнительных        |
|                       |                       | производств,          |
|                       |                       | предметом которых     |
|                       |                       | являются налоги и     |
|                       |                       | сборы, в отношении    |
|                       |                       | организаций со        |
|                       |                       | схожими реквизитами   |
|                       |                       | (всего)               |
+-----------------------+-----------------------+-----------------------+
| ``s1008``             | number                | Исп. пр-ва. Сумма (в  |
|                       |                       | рублях) найденных     |
|                       |                       | исполнительных        |
|                       |                       | производств,          |
|                       |                       | предметом которых     |
|                       |                       | являются страховые    |
|                       |                       | взносы, в отношении   |
|                       |                       | организаций со        |
|                       |                       | схожими реквизитами   |
|                       |                       | (всего)               |
+-----------------------+-----------------------+-----------------------+
| ``q2001``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка количества дел |
|                       |                       | за 12 последних       |
|                       |                       | месяцев               |
+-----------------------+-----------------------+-----------------------+
| ``q2002``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка количества дел |
|                       |                       | за 3 года             |
+-----------------------+-----------------------+-----------------------+
| ``q2003``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве истца.       |
|                       |                       | Оценка количества дел |
|                       |                       | за 12 последних       |
|                       |                       | месяцев               |
+-----------------------+-----------------------+-----------------------+
| ``q2004``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве истца.       |
|                       |                       | Оценка количества дел |
|                       |                       | за 3 года             |
+-----------------------+-----------------------+-----------------------+
| ``s2001``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка исковой суммы  |
|                       |                       | за 12 последних       |
|                       |                       | месяцев (в рублях)    |
+-----------------------+-----------------------+-----------------------+
| ``s2002``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка исковой суммы  |
|                       |                       | за 3 последних года   |
|                       |                       | (в рублях)            |
+-----------------------+-----------------------+-----------------------+
| ``s2003``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве истца.       |
|                       |                       | Оценка исковой суммы  |
|                       |                       | за 12 последних       |
|                       |                       | месяцев (в рублях)    |
+-----------------------+-----------------------+-----------------------+
| ``s2004``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве истца.       |
|                       |                       | Оценка исковой суммы  |
|                       |                       | за 3 последних года   |
|                       |                       | (в рублях)            |
+-----------------------+-----------------------+-----------------------+
| ``q2011``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка количества     |
|                       |                       | проигранных дел (за 3 |
|                       |                       | года)                 |
+-----------------------+-----------------------+-----------------------+
| ``q2012``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка количества     |
|                       |                       | частично проигранных  |
|                       |                       | дел (за 3 года)       |
+-----------------------+-----------------------+-----------------------+
| ``q2013``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка количества не  |
|                       |                       | проигранных дел (за 3 |
|                       |                       | года)                 |
+-----------------------+-----------------------+-----------------------+
| ``q2014``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка количества дел |
|                       |                       | в процессе            |
|                       |                       | рассмотрения (за 3    |
|                       |                       | года)                 |
+-----------------------+-----------------------+-----------------------+
| ``q2015``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка количества     |
|                       |                       | дел, исход которых    |
|                       |                       | определить не удалось |
|                       |                       | (за 3 года)           |
+-----------------------+-----------------------+-----------------------+
| ``q2016``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка количества     |
|                       |                       | проигранных дел (за   |
|                       |                       | 12 месяцев)           |
+-----------------------+-----------------------+-----------------------+
| ``q2017``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка количества     |
|                       |                       | частично проигранных  |
|                       |                       | дел (за 12 месяцев)   |
+-----------------------+-----------------------+-----------------------+
| ``q2018``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка количества не  |
|                       |                       | проигранных дел (за   |
|                       |                       | 12 месяцев)           |
+-----------------------+-----------------------+-----------------------+
| ``q2019``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка количества дел |
|                       |                       | в процессе            |
|                       |                       | рассмотрения (за 12   |
|                       |                       | месяцев)              |
+-----------------------+-----------------------+-----------------------+
| ``s2011``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка исковой суммы  |
|                       |                       | проигранных дел (за 3 |
|                       |                       | года)                 |
+-----------------------+-----------------------+-----------------------+
| ``s2012``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка исковой суммы  |
|                       |                       | частично проигранных  |
|                       |                       | дел (за 3 года)       |
+-----------------------+-----------------------+-----------------------+
| ``s2013``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка исковой суммы  |
|                       |                       | не проигранных дел    |
|                       |                       | (за 3 года)           |
+-----------------------+-----------------------+-----------------------+
| ``s2014``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка исковой суммы  |
|                       |                       | по делам в процессе   |
|                       |                       | рассмотрения (за 3    |
|                       |                       | года)                 |
+-----------------------+-----------------------+-----------------------+
| ``s2015``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка исковой суммы  |
|                       |                       | по делам, исход       |
|                       |                       | которых определить не |
|                       |                       | удалось (за 3 года)   |
+-----------------------+-----------------------+-----------------------+
| ``s2016``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка исковой суммы  |
|                       |                       | проигранных дел (за   |
|                       |                       | 12 месяцев)           |
+-----------------------+-----------------------+-----------------------+
| ``s2017``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка исковой суммы  |
|                       |                       | частично проигранных  |
|                       |                       | дел (за 12 месяцев)   |
+-----------------------+-----------------------+-----------------------+
| ``s2018``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка исковой суммы  |
|                       |                       | не проигранных дел    |
|                       |                       | (за 12 месяцев)       |
+-----------------------+-----------------------+-----------------------+
| ``s2019``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка исковой суммы  |
|                       |                       | дел в процессе        |
|                       |                       | рассмотрения (за 12   |
|                       |                       | месяцев)              |
+-----------------------+-----------------------+-----------------------+
| ``q2021``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве истца.       |
|                       |                       | Оценка количества не  |
|                       |                       | выигранных дел (за 3  |
|                       |                       | года)                 |
+-----------------------+-----------------------+-----------------------+
| ``q2022``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве истца.       |
|                       |                       | Оценка количества     |
|                       |                       | частично выигранных   |
|                       |                       | дел (за 3 года)       |
+-----------------------+-----------------------+-----------------------+
| ``q2023``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве истца.       |
|                       |                       | Оценка количества     |
|                       |                       | выигранных дел (за 3  |
|                       |                       | года)                 |
+-----------------------+-----------------------+-----------------------+
| ``q2024``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве истца.       |
|                       |                       | Оценка количества дел |
|                       |                       | в процессе            |
|                       |                       | рассмотрения (за 3    |
|                       |                       | года)                 |
+-----------------------+-----------------------+-----------------------+
| ``q2025``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве истца.       |
|                       |                       | Оценка количества     |
|                       |                       | дел, исход которых    |
|                       |                       | определить не удалось |
|                       |                       | (за 3 года)           |
+-----------------------+-----------------------+-----------------------+
| ``s2021``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве истца.       |
|                       |                       | Оценка исковой суммы  |
|                       |                       | не выигранных дел (за |
|                       |                       | 3 года)               |
+-----------------------+-----------------------+-----------------------+
| ``s2022``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве истца.       |
|                       |                       | Оценка исковой суммы  |
|                       |                       | частично выигранных   |
|                       |                       | дел (за 3 года)       |
+-----------------------+-----------------------+-----------------------+
| ``s2023``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве истца.       |
|                       |                       | Оценка исковой суммы  |
|                       |                       | выигранных дел (за 3  |
|                       |                       | года)                 |
+-----------------------+-----------------------+-----------------------+
| ``s2024``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве истца.       |
|                       |                       | Оценка исковой суммы  |
|                       |                       | по делам в процессе   |
|                       |                       | рассмотрения (за 3    |
|                       |                       | года)                 |
+-----------------------+-----------------------+-----------------------+
| ``s2025``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве истца.       |
|                       |                       | Оценка исковой суммы  |
|                       |                       | по делам, исход       |
|                       |                       | которых определить не |
|                       |                       | удалось (за 3 года)   |
+-----------------------+-----------------------+-----------------------+
| ``q2031``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка количества     |
|                       |                       | дел, которые связаны  |
|                       |                       | с проведением         |
|                       |                       | процедуры банкротства |
+-----------------------+-----------------------+-----------------------+
| ``q2032``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка количества     |
|                       |                       | дел, которые связаны  |
|                       |                       | с обязательствами по  |
|                       |                       | договорам займа,      |
|                       |                       | кредита, лизинга      |
+-----------------------+-----------------------+-----------------------+
| ``q2033``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка количества     |
|                       |                       | дел, которые связаны  |
|                       |                       | с налогами: иски      |
|                       |                       | налоговых органов,    |
|                       |                       | взыскание налогов,    |
|                       |                       | оспаривание решений   |
|                       |                       | налоговых органов     |
+-----------------------+-----------------------+-----------------------+
| ``q2034``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка количества     |
|                       |                       | дел, которые связаны  |
|                       |                       | с обязательствами по  |
|                       |                       | договорам на оказание |
|                       |                       | услуг                 |
+-----------------------+-----------------------+-----------------------+
| ``q2035``             | integer               | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка количества     |
|                       |                       | дел, которые связаны  |
|                       |                       | с обязательствами по  |
|                       |                       | договорам поставки    |
+-----------------------+-----------------------+-----------------------+
| ``s2031``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка исковой суммы  |
|                       |                       | у дел, которые        |
|                       |                       | связаны с проведением |
|                       |                       | процедуры банкротства |
+-----------------------+-----------------------+-----------------------+
| ``s2032``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка исковой суммы  |
|                       |                       | у дел, которые        |
|                       |                       | связаны с             |
|                       |                       | обязательствами по    |
|                       |                       | договорам займа,      |
|                       |                       | кредита, лизинга      |
+-----------------------+-----------------------+-----------------------+
| ``s2033``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка исковой суммы  |
|                       |                       | у дел, которые        |
|                       |                       | связаны с налогами:   |
|                       |                       | иски налоговых        |
|                       |                       | органов, взыскание    |
|                       |                       | налогов, оспаривание  |
|                       |                       | решений налоговых     |
|                       |                       | органов               |
+-----------------------+-----------------------+-----------------------+
| ``s2034``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка исковой суммы  |
|                       |                       | у дел, которые        |
|                       |                       | связаны с             |
|                       |                       | обязательствами по    |
|                       |                       | договорам на оказание |
|                       |                       | услуг                 |
+-----------------------+-----------------------+-----------------------+
| ``s2035``             | number                | Арбитраж. Дела в      |
|                       |                       | качестве ответчика.   |
|                       |                       | Оценка исковой суммы  |
|                       |                       | у дел, которые        |
|                       |                       | связаны с             |
|                       |                       | обязательствами по    |
|                       |                       | договорам поставки    |
+-----------------------+-----------------------+-----------------------+
| ``m4001``             | boolean               | Госконтракты.         |
|                       |                       | Организация была      |
|                       |                       | найдена в реестре     |
|                       |                       | недобросовестных      |
|                       |                       | поставщиков (ФАС,     |
|                       |                       | Федеральное           |
|                       |                       | Казначейство)         |
+-----------------------+-----------------------+-----------------------+
| ``q4002``             | integer               | Госконтракты.         |
|                       |                       | Количество            |
|                       |                       | госконтрактов (44ФЗ и |
|                       |                       | 223ФЗ), в которых     |
|                       |                       | организация участвует |
|                       |                       | в качестве поставщика |
|                       |                       | (за 12 последних      |
|                       |                       | месяцев)              |
+-----------------------+-----------------------+-----------------------+
| ``s4002``             | number                | Госконтракты. Сумма   |
|                       |                       | по госконтрактам      |
|                       |                       | (44ФЗ и 223ФЗ), в     |
|                       |                       | которых организация   |
|                       |                       | участвует в качестве  |
|                       |                       | поставщика (за 12     |
|                       |                       | последних месяцев, с  |
|                       |                       | оценкой в рублях)     |
+-----------------------+-----------------------+-----------------------+
| ``q4003``             | integer               | Госконтракты.         |
|                       |                       | Количество            |
|                       |                       | госконтрактов (44ФЗ и |
|                       |                       | 223ФЗ), в которых     |
|                       |                       | организация участвует |
|                       |                       | в качестве поставщика |
+-----------------------+-----------------------+-----------------------+
| ``s4003``             | number                | Госконтракты. Сумма   |
|                       |                       | по госконтрактам      |
|                       |                       | (44ФЗ и 223ФЗ), в     |
|                       |                       | которых организация   |
|                       |                       | участвует в качестве  |
|                       |                       | поставщика (с оценкой |
|                       |                       | в рублях)             |
+-----------------------+-----------------------+-----------------------+
| ``q4004``             | integer               | Госконтракты.         |
|                       |                       | Количество            |
|                       |                       | госконтрактов (44ФЗ и |
|                       |                       | 223ФЗ), в которых     |
|                       |                       | организация участвует |
|                       |                       | в качестве заказчика  |
|                       |                       | (за 12 последних      |
|                       |                       | месяцев)              |
+-----------------------+-----------------------+-----------------------+
| ``s4004``             | number                | Госконтракты. Сумма   |
|                       |                       | по госконтрактам      |
|                       |                       | (44ФЗ и 223ФЗ), в     |
|                       |                       | которых организация   |
|                       |                       | участвует в качестве  |
|                       |                       | заказчика (за 12      |
|                       |                       | последних месяцев, с  |
|                       |                       | оценкой в рублях)     |
+-----------------------+-----------------------+-----------------------+
| ``q4005``             | integer               | Госконтракты.         |
|                       |                       | Количество            |
|                       |                       | госконтрактов (44ФЗ и |
|                       |                       | 223ФЗ), в которых     |
|                       |                       | организация участвует |
|                       |                       | в качестве заказчика  |
+-----------------------+-----------------------+-----------------------+
| ``s4005``             | number                | Госконтракты. Сумма   |
|                       |                       | по госконтрактам      |
|                       |                       | (44ФЗ и 223ФЗ), в     |
|                       |                       | которых организация   |
|                       |                       | участвует в качестве  |
|                       |                       | заказчика (с оценкой  |
|                       |                       | в рублях)             |
+-----------------------+-----------------------+-----------------------+
| ``m5001``             | boolean               | Особые реестры ФНС.   |
|                       |                       | ФИО руководителей или |
|                       |                       | учредителей были      |
|                       |                       | найдены в реестре     |
|                       |                       | дисквалифицированных  |
|                       |                       | лиц (ФНС). Поле       |
|                       |                       | оставлено для         |
|                       |                       | сохранения обратной   |
|                       |                       | совместимости.        |
|                       |                       | Воспользуйтесь        |
|                       |                       | маркером m5008.       |
+-----------------------+-----------------------+-----------------------+
| ``m5008``             | boolean               | Особые реестры ФНС.   |
|                       |                       | ФИО руководителей     |
|                       |                       | были найдены в        |
|                       |                       | реестре               |
|                       |                       | дисквалифицированных  |
|                       |                       | лиц (ФНС) или в       |
|                       |                       | выписке ЕГРЮЛ         |
+-----------------------+-----------------------+-----------------------+
| ``m5002``             | boolean               | В связи с отказом ФНС |
|                       |                       | от реестра            |
|                       |                       | организаций, связь с  |
|                       |                       | которыми по           |
|                       |                       | указанному адресу     |
|                       |                       | отсутствует, данные о |
|                       |                       | недостоверности       |
|                       |                       | адреса берутся из     |
|                       |                       | ЕГРЮЛ. Рекомендуем    |
|                       |                       | использовать маркер   |
|                       |                       | m5006                 |
+-----------------------+-----------------------+-----------------------+
| ``m5003``             | boolean               | Особые реестры ФНС.   |
|                       |                       | Адрес организации был |
|                       |                       | найден в списке       |
|                       |                       | “адресов массовой     |
|                       |                       | регистрации” (ФНС)    |
+-----------------------+-----------------------+-----------------------+
| ``m5004``             | boolean               | Особые реестры ФНС.   |
|                       |                       | Организация была      |
|                       |                       | найдена в списке      |
|                       |                       | юридических лиц,      |
|                       |                       | имеющих задолженность |
|                       |                       | по уплате налогов     |
|                       |                       | более 1000 руб,       |
|                       |                       | которая направлялась  |
|                       |                       | на взыскание          |
|                       |                       | судебному             |
|                       |                       | приставу-исполнителю  |
|                       |                       | (ФНС)                 |
+-----------------------+-----------------------+-----------------------+
| ``m5005``             | boolean               | Особые реестры ФНС.   |
|                       |                       | Организация была      |
|                       |                       | найдена в списке      |
|                       |                       | юридических лиц, не   |
|                       |                       | представляющих        |
|                       |                       | налоговую отчетность  |
|                       |                       | более года (ФНС)      |
+-----------------------+-----------------------+-----------------------+
| ``m5009``             | boolean               | Особые реестры ФНС.   |
|                       |                       | ФИО руководителя было |
|                       |                       | найдено в списке      |
|                       |                       | “массовых”            |
|                       |                       | руководителей         |
+-----------------------+-----------------------+-----------------------+
| ``m5010``             | boolean               | Особые реестры ФНС.   |
|                       |                       | ФИО учредителя было   |
|                       |                       | найдено в списке      |
|                       |                       | “массовых”            |
|                       |                       | учредителей           |
+-----------------------+-----------------------+-----------------------+
| ``m5006``             | boolean               | В ЕГРЮЛ указан        |
|                       |                       | признак               |
|                       |                       | недостоверности       |
|                       |                       | сведений в отношении  |
|                       |                       | адреса                |
+-----------------------+-----------------------+-----------------------+
| ``m5007``             | boolean               | В ЕГРЮЛ указан        |
|                       |                       | признак               |
|                       |                       | недостоверности       |
|                       |                       | сведений в отношении  |
|                       |                       | руководителя или      |
|                       |                       | учредителей           |
+-----------------------+-----------------------+-----------------------+
| ``q6001``             | integer               | Финансы. Год, за      |
|                       |                       | который доступна      |
|                       |                       | последняя             |
|                       |                       | бухгалтерская         |
|                       |                       | отчетность по         |
|                       |                       | организации           |
+-----------------------+-----------------------+-----------------------+
| ``m6002``             | boolean               | Финансы. Есть         |
|                       |                       | бухгалтерская         |
|                       |                       | отчетность за         |
|                       |                       | последний отчетный    |
|                       |                       | год (на момент, когда |
|                       |                       | такая отчетность      |
|                       |                       | становится доступна в |
|                       |                       | Контур.Фокусе)        |
+-----------------------+-----------------------+-----------------------+
| ``s6003``             | number                | Финансы. Выручка на   |
|                       |                       | начало отчетного      |
|                       |                       | периода (за последний |
|                       |                       | отчетный год, оценка  |
|                       |                       | в рублях)             |
+-----------------------+-----------------------+-----------------------+
| ``s6004``             | number                | Финансы. Выручка на   |
|                       |                       | конец отчетного       |
|                       |                       | периода (за последний |
|                       |                       | отчетный год, оценка  |
|                       |                       | в рублях)             |
+-----------------------+-----------------------+-----------------------+
| ``s6005``             | number                | Финансы. Баланс на    |
|                       |                       | начало отчетного      |
|                       |                       | периода (за последний |
|                       |                       | отчетный год, оценка  |
|                       |                       | в рублях)             |
+-----------------------+-----------------------+-----------------------+
| ``s6006``             | number                | Финансы. Баланс на    |
|                       |                       | конец отчетного       |
|                       |                       | периода (за последний |
|                       |                       | отчетный год, оценка  |
|                       |                       | в рублях)             |
+-----------------------+-----------------------+-----------------------+
| ``s6007``             | number                | Финансы. Чистая       |
|                       |                       | прибыль на начало     |
|                       |                       | отчетного периода (за |
|                       |                       | последний отчетный    |
|                       |                       | год, оценка в рублях) |
+-----------------------+-----------------------+-----------------------+
| ``s6008``             | number                | Финансы. Чистая       |
|                       |                       | прибыль на конец      |
|                       |                       | отчетного периода (за |
|                       |                       | последний отчетный    |
|                       |                       | год, оценка в рублях) |
+-----------------------+-----------------------+-----------------------+
| ``s6009``             | number                | Финансы. Общая сумма  |
|                       |                       | уплаченных налогов и  |
|                       |                       | сборов в году,        |
|                       |                       | предшествующем году   |
|                       |                       | размещения таких      |
|                       |                       | сведений. Размещение  |
|                       |                       | сведений ежегодно 1   |
|                       |                       | октября (ФНС)         |
+-----------------------+-----------------------+-----------------------+
| ``s6010``             | number                | Финансы. Сумма        |
|                       |                       | доходов по данным     |
|                       |                       | бухгалтерской         |
|                       |                       | отчетности за год,    |
|                       |                       | предшествующий году   |
|                       |                       | размещения таких      |
|                       |                       | сведений. Размещение  |
|                       |                       | сведений ежегодно 1   |
|                       |                       | октября (ФНС)         |
+-----------------------+-----------------------+-----------------------+
| ``s6011``             | number                | Финансы. Сумма        |
|                       |                       | расходов по данным    |
|                       |                       | бухгалтерской         |
|                       |                       | отчетности за год,    |
|                       |                       | предшествующий году   |
|                       |                       | размещения таких      |
|                       |                       | сведений. Размещение  |
|                       |                       | сведений ежегодно 1   |
|                       |                       | октября (ФНС)         |
+-----------------------+-----------------------+-----------------------+
| ``s6012``             | number                | Финансы. Общая сумма  |
|                       |                       | задолженности по      |
|                       |                       | налогам на 31 декабря |
|                       |                       | года, предшествующего |
|                       |                       | году размещения таких |
|                       |                       | сведений. При         |
|                       |                       | условии, что она не   |
|                       |                       | уплачена до 1 октября |
|                       |                       | года размещения таких |
|                       |                       | сведений. Размещение  |
|                       |                       | сведений ежегодно 1   |
|                       |                       | декабря (ФНС)         |
+-----------------------+-----------------------+-----------------------+
| ``s6013``             | number                | Финансы. Общая сумма  |
|                       |                       | штрафов за налоговые  |
|                       |                       | правонарушения,       |
|                       |                       | назначенные в году,   |
|                       |                       | предшествующем году   |
|                       |                       | размещения таких      |
|                       |                       | сведений. При         |
|                       |                       | условии, что они не   |
|                       |                       | уплачены до 1 октября |
|                       |                       | года размещения таких |
|                       |                       | сведений. Размещение  |
|                       |                       | сведений ежегодно 1   |
|                       |                       | декабря (ФНС)         |
+-----------------------+-----------------------+-----------------------+
| ``m7001``             | boolean               | Юр. признаки. Маркер  |
|                       |                       | ‘Рекомендована        |
|                       |                       | дополнительная        |
|                       |                       | проверка’             |
+-----------------------+-----------------------+-----------------------+
| ``m7002``             | boolean               | Юр. признаки.         |
|                       |                       | Организация           |
|                       |                       | зарегистрирована      |
|                       |                       | менее 3 месяцев тому  |
|                       |                       | назад                 |
+-----------------------+-----------------------+-----------------------+
| ``m7003``             | boolean               | Юр. признаки.         |
|                       |                       | Организация           |
|                       |                       | зарегистрирована      |
|                       |                       | менее 6 месяцев тому  |
|                       |                       | назад                 |
+-----------------------+-----------------------+-----------------------+
| ``m7004``             | boolean               | Юр. признаки.         |
|                       |                       | Организация           |
|                       |                       | зарегистрирована      |
|                       |                       | менее 12 месяцев тому |
|                       |                       | назад                 |
+-----------------------+-----------------------+-----------------------+
| ``q7005``             | integer               | Юр. признаки. Оценка  |
|                       |                       | числа связанных       |
|                       |                       | компаний, которые     |
|                       |                       | были ликвидированы в  |
|                       |                       | результате            |
|                       |                       | банкротства           |
+-----------------------+-----------------------+-----------------------+
| ``q7006``             | integer               | Юр. признаки.         |
|                       |                       | Количество не         |
|                       |                       | ликвидированных       |
|                       |                       | юридических лиц,      |
|                       |                       | зарегистрированных по |
|                       |                       | тому же адресу (с     |
|                       |                       | учетом номера офиса)  |
|                       |                       | на текущий момент     |
|                       |                       | времени               |
+-----------------------+-----------------------+-----------------------+
| ``q7007``             | integer               | Юр. признаки.         |
|                       |                       | Количество не         |
|                       |                       | ликвидированных       |
|                       |                       | юридических лиц,      |
|                       |                       | зарегистрированных по |
|                       |                       | тому же адресу (без   |
|                       |                       | учета номера офиса)   |
|                       |                       | на текущий момент     |
|                       |                       | времени               |
+-----------------------+-----------------------+-----------------------+
| ``q7008``             | integer               | Юр. признаки.         |
|                       |                       | Количество            |
|                       |                       | юридических лиц,      |
|                       |                       | когда-либо            |
|                       |                       | зарегистрированных по |
|                       |                       | тому же адресу (с     |
|                       |                       | учетом номера офиса)  |
+-----------------------+-----------------------+-----------------------+
| ``q7009``             | integer               | Юр. признаки.         |
|                       |                       | Количество            |
|                       |                       | юридических лиц,      |
|                       |                       | когда-либо            |
|                       |                       | зарегистрированных по |
|                       |                       | тому же адресу (без   |
|                       |                       | учета номера офиса)   |
+-----------------------+-----------------------+-----------------------+
| ``d7010``             | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+
| ``m7010``             | boolean               | Юр. признаки. Наличие |
|                       |                       | ограничений на        |
|                       |                       | операции по           |
|                       |                       | банковским счетам     |
|                       |                       | организации,          |
|                       |                       | установленных ФНС, по |
|                       |                       | состоянию на d7010    |
+-----------------------+-----------------------+-----------------------+
| ``m7013``             | boolean               | Юр. признаки.         |
|                       |                       | Обнаружены            |
|                       |                       | арбитражные дела о    |
|                       |                       | банкротстве за        |
|                       |                       | последние 3 месяца    |
+-----------------------+-----------------------+-----------------------+
| ``q7026``             | integer               | Юр. признаки. Число   |
|                       |                       | арбитражных дел о     |
|                       |                       | банкротстве в         |
|                       |                       | качестве ответчика    |
|                       |                       | (наличие дела о       |
|                       |                       | банкротстве может не  |
|                       |                       | свидетельствовать о   |
|                       |                       | начале процедуры      |
|                       |                       | банкротства)          |
+-----------------------+-----------------------+-----------------------+
| ``m7014``             | boolean               | Юр. признаки.         |
|                       |                       | Обнаружены сообщения  |
|                       |                       | о банкротстве за      |
|                       |                       | последние 12 месяцев  |
+-----------------------+-----------------------+-----------------------+
| ``m7015``             | boolean               | Юр. признаки.         |
|                       |                       | Обнаружены сообщения  |
|                       |                       | о намерении           |
|                       |                       | обратиться в суд с    |
|                       |                       | заявлением о          |
|                       |                       | банкротстве за        |
|                       |                       | последние 3 месяца    |
+-----------------------+-----------------------+-----------------------+
| ``e7014``             | string                | Юр. признаки. Текущая |
|                       |                       | стадия банкротства по |
|                       |                       | решению суда от d7014 |
|                       |                       | (вычисляется на       |
|                       |                       | основе сообщений о    |
|                       |                       | банкротстве)          |
+-----------------------+-----------------------+-----------------------+
| ``d7014``             | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+
| ``m7016``             | boolean               | Юр. признаки.         |
|                       |                       | Обнаружены признаки   |
|                       |                       | завершенной процедуры |
|                       |                       | банкротства           |
+-----------------------+-----------------------+-----------------------+
| ``q7017``             | integer               | Юр. признаки.         |
|                       |                       | Количество юрлиц, в   |
|                       |                       | уставном капитале     |
|                       |                       | которых есть доля     |
|                       |                       | текущего юрлица       |
|                       |                       | (учрежденные юрлица)  |
+-----------------------+-----------------------+-----------------------+
| ``q7018``             | integer               | Юр. признаки.         |
|                       |                       | Количество не         |
|                       |                       | ликвидированных       |
|                       |                       | юридических лиц, в    |
|                       |                       | которых в качестве    |
|                       |                       | действующего          |
|                       |                       | руководителя упомянут |
|                       |                       | действующий           |
|                       |                       | руководитель текущей  |
|                       |                       | организации (с учетом |
|                       |                       | ИННФЛ, если известен) |
+-----------------------+-----------------------+-----------------------+
| ``q7019``             | integer               | Юр. признаки.         |
|                       |                       | Количество не         |
|                       |                       | ликвидированных       |
|                       |                       | юридических лиц, в    |
|                       |                       | которых в качестве    |
|                       |                       | действующего          |
|                       |                       | руководителя упомянут |
|                       |                       | действующий           |
|                       |                       | руководитель текущей  |
|                       |                       | организации (с учетом |
|                       |                       | только ФИО)           |
+-----------------------+-----------------------+-----------------------+
| ``q7020``             | integer               | Юр. признаки.         |
|                       |                       | Количество            |
|                       |                       | юридических лиц, в    |
|                       |                       | которых в качестве    |
|                       |                       | действующего или      |
|                       |                       | бывшего руководителя  |
|                       |                       | упомянут действующий  |
|                       |                       | руководитель текущей  |
|                       |                       | организации (с учетом |
|                       |                       | ИННФЛ, если известен) |
+-----------------------+-----------------------+-----------------------+
| ``q7021``             | integer               | Юр. признаки.         |
|                       |                       | Количество            |
|                       |                       | юридических лиц, в    |
|                       |                       | которых в качестве    |
|                       |                       | действующего или      |
|                       |                       | бывшего руководителя  |
|                       |                       | упомянут действующий  |
|                       |                       | руководитель текущей  |
|                       |                       | организации (с учетом |
|                       |                       | только ФИО)           |
+-----------------------+-----------------------+-----------------------+
| ``q7022``             | integer               | Юр. признаки.         |
|                       |                       | Среднесписочная       |
|                       |                       | численность           |
|                       |                       | работников за         |
|                       |                       | календарный год,      |
|                       |                       | предшествующий году   |
|                       |                       | размещения таких      |
|                       |                       | сведений. Размещение  |
|                       |                       | сведений ежегодно 8   |
|                       |                       | апреля (ФНС)          |
+-----------------------+-----------------------+-----------------------+
| ``m7022``             | boolean               | Юр. признаки. Наличие |
|                       |                       | за последние 12       |
|                       |                       | месяцев сообщений о   |
|                       |                       | банкротстве физлица,  |
|                       |                       | являющегося           |
|                       |                       | руководителем (лицом  |
|                       |                       | с правом подписи без  |
|                       |                       | доверенности),        |
|                       |                       | учредителем, либо     |
|                       |                       | индивидуальным        |
|                       |                       | предпринимателем.     |
|                       |                       | Необходимо изучить    |
|                       |                       | сообщения             |
+-----------------------+-----------------------+-----------------------+
| ``m7026``             | boolean               | Юр. признаки. Наличие |
|                       |                       | за последние 12       |
|                       |                       | месяцев сообщений о   |
|                       |                       | банкротстве физлица,  |
|                       |                       | являющегося           |
|                       |                       | учредителем, либо     |
|                       |                       | индивидуальным        |
|                       |                       | предпринимателем.     |
|                       |                       | Необходимо изучить    |
|                       |                       | сообщения             |
+-----------------------+-----------------------+-----------------------+
| ``d7023``             | `date <#date>`__      | `Описание типа        |
|                       |                       | date <#date>`__       |
+-----------------------+-----------------------+-----------------------+
| ``m7023``             | boolean               | Юр. признаки. Наличие |
|                       |                       | категории             |
|                       |                       | микропредприятия в    |
|                       |                       | едином реестре        |
|                       |                       | субъектов малого и    |
|                       |                       | среднего              |
|                       |                       | предпринимательства   |
|                       |                       | (ФНС)                 |
+-----------------------+-----------------------+-----------------------+
| ``m7024``             | boolean               | Юр. признаки. Наличие |
|                       |                       | категории малого      |
|                       |                       | предприятия в едином  |
|                       |                       | реестре субъектов     |
|                       |                       | малого и среднего     |
|                       |                       | предпринимательства   |
|                       |                       | (ФНС)                 |
+-----------------------+-----------------------+-----------------------+
| ``m7025``             | boolean               | Юр. признаки. Наличие |
|                       |                       | категории среднего    |
|                       |                       | предприятия в едином  |
|                       |                       | реестре субъектов     |
|                       |                       | малого и среднего     |
|                       |                       | предпринимательства   |
|                       |                       | (ФНС)                 |
+-----------------------+-----------------------+-----------------------+
| ``m7027``             | boolean               | Юр. признаки.         |
|                       |                       | Применяет упрощенную  |
|                       |                       | систему               |
|                       |                       | налогообложения — УСН |
|                       |                       | по состоянию на 31    |
|                       |                       | декабря года,         |
|                       |                       | предшествующего году  |
|                       |                       | размещения таких      |
|                       |                       | сведений. Размещение  |
|                       |                       | сведений ежегодно 1   |
|                       |                       | августа (ФНС)         |
+-----------------------+-----------------------+-----------------------+
| ``m7028``             | boolean               | Юр. признаки.         |
|                       |                       | Является плательщиком |
|                       |                       | единого налога на     |
|                       |                       | вмененный доход —     |
|                       |                       | ЕНВД по состоянию на  |
|                       |                       | 31 декабря года,      |
|                       |                       | предшествующего году  |
|                       |                       | размещения таких      |
|                       |                       | сведений. Размещение  |
|                       |                       | сведений ежегодно 1   |
|                       |                       | августа (ФНС)         |
+-----------------------+-----------------------+-----------------------+
| ``m7029``             | boolean               | Юр. признаки.         |
|                       |                       | Является плательщиком |
|                       |                       | единого               |
|                       |                       | сельскохозяйственного |
|                       |                       | налога — ЕСХН по      |
|                       |                       | состоянию на 31       |
|                       |                       | декабря года,         |
|                       |                       | предшествующего году  |
|                       |                       | размещения таких      |
|                       |                       | сведений. Размещение  |
|                       |                       | сведений ежегодно 1   |
|                       |                       | августа (ФНС)         |
+-----------------------+-----------------------+-----------------------+
| ``m7030``             | boolean               | Юр. признаки.         |
|                       |                       | Применяет соглашение  |
|                       |                       | о разделе продукции — |
|                       |                       | СРП по состоянию на   |
|                       |                       | 31 декабря года,      |
|                       |                       | предшествующего году  |
|                       |                       | размещения таких      |
|                       |                       | сведений. Размещение  |
|                       |                       | сведений ежегодно 1   |
|                       |                       | августа (ФНС)         |
+-----------------------+-----------------------+-----------------------+
| ``m7031``             | boolean               | Юр. признаки.         |
|                       |                       | Организация является  |
|                       |                       | участником            |
|                       |                       | консолидированной     |
|                       |                       | группы                |
|                       |                       | налогоплательщиков по |
|                       |                       | состоянию на 31       |
|                       |                       | декабря года,         |
|                       |                       | предшествующего году  |
|                       |                       | размещения таких      |
|                       |                       | сведений. Размещение  |
|                       |                       | сведений ежегодно 1   |
|                       |                       | августа (ФНС)         |
+-----------------------+-----------------------+-----------------------+
| ``m7032``             | boolean               | Юр. признаки.         |
|                       |                       | Организация является  |
|                       |                       | ответственным         |
|                       |                       | участником            |
|                       |                       | консолидированной     |
|                       |                       | группы                |
|                       |                       | налогоплательщиков по |
|                       |                       | состоянию на 31       |
|                       |                       | декабря года,         |
|                       |                       | предшествующего году  |
|                       |                       | размещения таких      |
|                       |                       | сведений. Размещение  |
|                       |                       | сведений ежегодно 1   |
|                       |                       | августа (ФНС)         |
+-----------------------+-----------------------+-----------------------+
| ``m7033``             | boolean               | Юр. признаки. Наличие |
|                       |                       | организации в перечне |
|                       |                       | стратегических        |
|                       |                       | предприятий и         |
|                       |                       | стратегических        |
|                       |                       | акционерных обществ,  |
|                       |                       | который утвержден     |
|                       |                       | Указом Президента     |
|                       |                       | Российской Федерации  |
|                       |                       | от 04.08.2004 № 1009  |
+-----------------------+-----------------------+-----------------------+
| ``m7034``             | boolean               | Юр. признаки. Наличие |
|                       |                       | организации в перечне |
|                       |                       | АО по Распоряжению    |
|                       |                       | Правительства № 91-Р  |
|                       |                       | — “Золотая акция”     |
|                       |                       | государства           |
+-----------------------+-----------------------+-----------------------+
| ``m7035``             | boolean               | Юр. признаки. Наличие |
|                       |                       | организации в едином  |
|                       |                       | реестре членов СРО    |
|                       |                       | НОСТРОЙ               |
+-----------------------+-----------------------+-----------------------+
| ``m7036``             | boolean               | Юр. признаки. Наличие |
|                       |                       | организации в едином  |
|                       |                       | реестре членов СРО    |
|                       |                       | НОПРИЗ                |
+-----------------------+-----------------------+-----------------------+
| ``m7037``             | boolean               | Юр. признаки. За      |
|                       |                       | последний месяц       |
|                       |                       | организация подавала  |
|                       |                       | заявления в ЕГРЮЛ,    |
|                       |                       | связанные с           |
|                       |                       | планируемой           |
|                       |                       | ликвидацией           |
|                       |                       | организации           |
+-----------------------+-----------------------+-----------------------+
| ``m7038``             | boolean               | Юр. признаки. За      |
|                       |                       | последний месяц       |
|                       |                       | организация подавала  |
|                       |                       | заявления в ЕГРЮЛ,    |
|                       |                       | связанные с           |
|                       |                       | изменением            |
|                       |                       | руководителя или      |
|                       |                       | управляющей компании. |
|                       |                       | По заявлениям еще не  |
|                       |                       | принято решение о     |
|                       |                       | государственной       |
|                       |                       | регистрации, либо     |
|                       |                       | принято решение об    |
|                       |                       | отказе в регистрации  |
+-----------------------+-----------------------+-----------------------+
| ``m7039``             | boolean               | Юр. признаки. За      |
|                       |                       | последний месяц       |
|                       |                       | организация подавала  |
|                       |                       | заявления в ЕГРЮЛ,    |
|                       |                       | связанные с           |
|                       |                       | изменением состава    |
|                       |                       | участников            |
|                       |                       | (владельцев). По      |
|                       |                       | заявлениям еще не     |
|                       |                       | принято решение о     |
|                       |                       | государственной       |
|                       |                       | регистрации, либо     |
|                       |                       | принято решение об    |
|                       |                       | отказе в регистрации  |
+-----------------------+-----------------------+-----------------------+
| ``m7040``             | boolean               | Юр. признаки. За      |
|                       |                       | последний месяц       |
|                       |                       | организация подавала  |
|                       |                       | заявления в ЕГРЮЛ,    |
|                       |                       | связанные с           |
|                       |                       | изменением            |
|                       |                       | юридического адреса.  |
|                       |                       | По заявлениям еще не  |
|                       |                       | принято решение о     |
|                       |                       | государственной       |
|                       |                       | регистрации, либо     |
|                       |                       | принято решение об    |
|                       |                       | отказе в регистрации  |
+-----------------------+-----------------------+-----------------------+
| ``m7041``             | boolean               | Юр. признаки. За      |
|                       |                       | последний месяц       |
|                       |                       | организация подавала  |
|                       |                       | заявления в ЕГРЮЛ,    |
|                       |                       | связанные с           |
|                       |                       | изменением уставного  |
|                       |                       | капитала. По          |
|                       |                       | заявлениям еще не     |
|                       |                       | принято решение о     |
|                       |                       | государственной       |
|                       |                       | регистрации, либо     |
|                       |                       | принято решение об    |
|                       |                       | отказе в регистрации  |
+-----------------------+-----------------------+-----------------------+
| ``m7042``             | boolean               | Юр. признаки. За      |
|                       |                       | последний месяц были  |
|                       |                       | поданы заявления в    |
|                       |                       | ЕГРИП, связанные с    |
|                       |                       | прекращением          |
|                       |                       | деятельности лица в   |
|                       |                       | качестве ИП           |
+-----------------------+-----------------------+-----------------------+
| ``q8001``             | integer               | Число потенциальных   |
|                       |                       | сайтов компании       |
+-----------------------+-----------------------+-----------------------+
| ``q9001``             | integer               | Количество товарных   |
|                       |                       | знаков, действующих   |
|                       |                       | или недействующих, в  |
|                       |                       | которых упоминается   |
|                       |                       | текущая компания      |
+-----------------------+-----------------------+-----------------------+
| ``q1101``             | integer               | Залоги. Число         |
|                       |                       | уведомлений о залогах |
|                       |                       | движимого имущества   |
|                       |                       | (залогодатель)        |
+-----------------------+-----------------------+-----------------------+
| ``q1102``             | integer               | Залоги. Число         |
|                       |                       | уведомлений о залогах |
|                       |                       | движимого имущества   |
|                       |                       | (залогодержатель)     |
+-----------------------+-----------------------+-----------------------+
| ``m8001``             | boolean               | Санкции. Наличие      |
|                       |                       | организации в         |
|                       |                       | санкционном списке    |
|                       |                       | США                   |
+-----------------------+-----------------------+-----------------------+
| ``m8002``             | boolean               | Санкции. Наличие      |
|                       |                       | организации в         |
|                       |                       | секторальном          |
|                       |                       | санкционном списке    |
|                       |                       | США                   |
+-----------------------+-----------------------+-----------------------+
| ``m8003``             | boolean               | Санкции. Наличие      |
|                       |                       | организации в         |
|                       |                       | санкционном списке    |
|                       |                       | Евросоюза             |
+-----------------------+-----------------------+-----------------------+
| ``m8004``             | boolean               | Санкции. Наличие      |
|                       |                       | организации в         |
|                       |                       | санкционном списке    |
|                       |                       | Великобритании        |
+-----------------------+-----------------------+-----------------------+
| ``m8005``             | boolean               | Санкции. Наличие      |
|                       |                       | организации в         |
|                       |                       | санкционном списке    |
|                       |                       | Украины               |
+-----------------------+-----------------------+-----------------------+
| ``m8006``             | boolean               | Санкции. Наличие      |
|                       |                       | организации в         |
|                       |                       | санкционном списке    |
|                       |                       | Швейцарии             |
+-----------------------+-----------------------+-----------------------+
| ``m8007``             | boolean               | Санкции. Совладельцы  |
|                       |                       | компании попадают под |
|                       |                       | санкции США, и их     |
|                       |                       | суммарная доля        |
|                       |                       | владения не меньше    |
|                       |                       | 50%                   |
+-----------------------+-----------------------+-----------------------+
| ``m8008``             | boolean               | Санкции. Совладельцы  |
|                       |                       | компании попадают под |
|                       |                       | секторальные санкции  |
|                       |                       | США, и их суммарная   |
|                       |                       | доля владения не      |
|                       |                       | меньше 50%            |
+-----------------------+-----------------------+-----------------------+
| ``m8009``             | boolean               | Санкции. Совладельцы  |
|                       |                       | компании попадают под |
|                       |                       | санкции Евросоюза, и  |
|                       |                       | их суммарная доля     |
|                       |                       | владения не меньше    |
|                       |                       | 50%                   |
+-----------------------+-----------------------+-----------------------+
| ``m8010``             | boolean               | Санкции. Совладельцы  |
|                       |                       | компании попадают под |
|                       |                       | санкции               |
|                       |                       | Великобритании, и их  |
|                       |                       | суммарная доля        |
|                       |                       | владения не меньше    |
|                       |                       | 50%                   |
+-----------------------+-----------------------+-----------------------+
| ``m8011``             | boolean               | Санкции. Совладельцы  |
|                       |                       | компании попадают под |
|                       |                       | санкции Украины, и их |
|                       |                       | суммарная доля        |
|                       |                       | владения не меньше    |
|                       |                       | 50%                   |
+-----------------------+-----------------------+-----------------------+
| ``m8012``             | boolean               | Санкции. Совладельцы  |
|                       |                       | компании попадают под |
|                       |                       | санкции Швейцарии, и  |
|                       |                       | их суммарная доля     |
|                       |                       | владения не меньше    |
|                       |                       | 50%                   |
+-----------------------+-----------------------+-----------------------+

Зависимости
-----------

Описание используемых фреймворков и стороннего ПО
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+-----------------------+-----------------------+-----------------------+
| Зависимость           | Версия                | Описание              |
+=======================+=======================+=======================+
| `spring-              | 2.3.0. RELEASE        | Зависимости для       |
| boot-starter-web <htt |                       | запуска web-сервера   |
| ps://mvnrepository.co |                       | внутри приложения     |
| m/artifact/org.spring |                       |                       |
| framework.boot/spring |                       |                       |
| -boot-starter-web>`__ |                       |                       |
+-----------------------+-----------------------+-----------------------+
| `s                    | 2.3.0. RELEASE        | Эндпоинты с           |
| pring-boot-starter-ac |                       | информацией о сервисе |
| tuator <https://docs. |                       |                       |
| spring.io/spring-boot |                       |                       |
| /docs/current/referen |                       |                       |
| ce/html/production-re |                       |                       |
| ady-features.html>`__ |                       |                       |
+-----------------------+-----------------------+-----------------------+
| `springfox-sw         | 2.9.2                 | Генерация             |
| agger2 <https://sprin |                       | документации в        |
| gfox.github.io/spring |                       | формате Open API      |
| fox/docs/current/>`__ |                       | Specification         |
+-----------------------+-----------------------+-----------------------+
| `springfox-swagger-   | 2.9.2                 | Генерация страницы    |
| ui <https://www.googl |                       | swagger-ui.html для   |
| e.com/search?q=spring |                       | отображения           |
| fox-swagger-ui+maven& |                       | swagger-докуаментации |
| oq=springfox-swagger- |                       |                       |
| ui&aqs=chrome.1.69i57 |                       |                       |
| j0l7.1136j0j4&sourcei |                       |                       |
| d=chrome&ie=UTF-8>`__ |                       |                       |
+-----------------------+-----------------------+-----------------------+
| `m                    | 1.5.1                 | Сбор метрик           |
| icrometer-core <https |                       |                       |
| ://mvnrepository.com/ |                       |                       |
| artifact/io.micromete |                       |                       |
| r/micrometer-core>`__ |                       |                       |
| и                     |                       |                       |
| `micrometer           |                       |                       |
| -registry-prometheus  |                       |                       |
| <https://mvnrepositor |                       |                       |
| y.com/artifact/io.mic |                       |                       |
| rometer/micrometer-re |                       |                       |
| gistry-prometheus>`__ |                       |                       |
+-----------------------+-----------------------+-----------------------+
| `snakeyaml            | 1.26                  | Сериализация и        |
|  <https://bitbucket.o |                       | десериализация yaml   |
| rg/asomov/snakeyaml/w |                       |                       |
| iki/Documentation>`__ |                       |                       |
+-----------------------+-----------------------+-----------------------+
| `groovy-all <https:   | 2.4.19                | Мощный, динамичный    |
| //mvnrepository.com/a |                       | язык для JVM          |
| rtifact/org.codehaus. |                       |                       |
| groovy/groovy-all>`__ |                       |                       |
+-----------------------+-----------------------+-----------------------+
| `gr                   | 2.9.2-01              | Адаптер Maven для     |
| oovy-eclipse-compiler |                       | пакетного компилятора |
|  <https://mvnreposito |                       | Groovy-Eclipse        |
| ry.com/artifact/org.c |                       |                       |
| odehaus.groovy/groovy |                       |                       |
| -eclipse-compiler>`__ |                       |                       |
+-----------------------+-----------------------+-----------------------+
| `groovy-eclipse-ba    | 2.4.19-01             | Groovy Eclipse        |
| tch <https://mvnrepos |                       | Compiler, упакованный |
| itory.com/artifact/or |                       | для пакетного         |
| g.codehaus.groovy/gro |                       | использования от      |
| ovy-eclipse-batch>`__ |                       | Maven                 |
+-----------------------+-----------------------+-----------------------+
| `failsafe <>`__       | 1.0.4                 |                       |
+-----------------------+-----------------------+-----------------------+

Конфигурация
------------

.. code:: yaml

   spring:
     profiles:
       active: default,swagger

   swagger.host: localhost:8080

   kontur-focus.baseUrl: http://srv-psb-test-01/api/kontur-focus
   dadata.baseUrl: http://moos/api/dadata
   es.baseUrl: http://srv-psb-test-01/es

   logging.level.com.farzoom: DEBUG

   #Metrics related configurations
   management:
     security:
       enabled: false

Метрики
~~~~~~~

Включены все дефолтные метрики Spring Boot и Prometheus:

.. code:: html

   http://localhost:8080/actuator
   http://localhost:8080/prometheus

Сборка
------

Сборка java-проекта
~~~~~~~~~~~~~~~~~~~

Сборка проекта осуществляется при помощи maven.

.. code:: bash

   mvn clean package

Сборка Docker-образа
~~~~~~~~~~~~~~~~~~~~

.. code:: bash

   mvn clean package
   cd ./service
   docker build .
