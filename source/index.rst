.. docs.farzoom documentation master file, created by
   sphinx-quickstart on Wed Jun 22 16:51:54 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to docs.farzoom's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   bg-api-agent-billing
   bg-api-cbs
   bg-api-qrcode-scanner
   common-api-doc
   common-api-doc-template
   common-api-es-company-enricher

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`