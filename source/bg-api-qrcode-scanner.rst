bg-api-qrcode-scanner
=====================

Назначение сервиса
------------------

Обработка PDF-сканов гарантий, предварительно загруженных в определённую
системными настройками папку. По каждому из обрабатываемых файлов
происходит поиск QR-кода, из которого становится известным ID заявки.
Далее обработанный файл загружается в docapi с заданными doctype и
направляется сообщение в Camunda с businessKey = orderId

.. figure:: /media/QR.jpg
   :alt: Краткий бизнес-процесс

   Краткий бизнес-процесс

Используемые фреймворки и стороннее ПО
--------------------------------------

-  Apache Camel
-  SpringFramework
-  Micrometer Application Monitoring
-  Project Lombok
-  SpringFox
-  ApachePDFBox
-  Google ZXing
-  Commons IO
-  JSON Web Token Support For The JVM
-  SnakeYAML

API
---

Авторизация
~~~~~~~~~~~

-  oAuth 2.0
-  XSRF ### Метод \| Тип запроса \| endPoint \| Назначение \| \|–|–|–\|
   \| POST \| /fileUpload \| Загрузка файла \| ### Пример запроса

::

   curl -X POST "https://pear-uat.moos.solutions/api/qrcode-scanner/fileUpload" -H "accept: */*" -H "Content-Type: multipart/form-data" -H "X-XSRF-TOKEN: eyJpdiI6IjAraGRKT2hTejNKVVRvbmUwMzZvVXc9PSIsInZhbHVlIjoiQTNuTWplOG9nUTc5ckZtSmNLSzZuUT09IiwibWFjIjoiMDIwNmMwM2NmOTE4ZTRlMzNhZTA4NGIyMGJkYTI4ZDYzMTQ4ZDUyNzZjNjQ1N2Y2OWM2ODcyMmFhZGQzMjJkZSJ9" -d {"file":{}}

Ответы сервиса
--------------

=== ==================
Код Описание
=== ==================
200 Документ принят
201 Документ обработан
401 Ошибка авторизации
403 Доступ запрещен
404 Сервис не найден
=== ==================

Build
-----

::

   mvn clean deploy
   docker build .

Configuration
-------------

``application.yml``

.. code:: yaml

   spring:
     profiles:
       active: swagger
   # Папка, в которую попадают сканы
   # '?moveFailed=.error` добавляется к пути до папки - это настройка Camel File
   bgScan.dir: '/tmp/bgscan?moveFailed=.error'

   # DPI (dots per inch) параметр, по умолчанию  - 250
   # используется при рендеринг PDF в изображение, на котором потом ищем QR код
   # надо подбирать в зависимости от качества входящих PDF (250-500) 
   bgScan.dpi: 250

   # Базовый URL к Camunda
   camunda.baseUrl: "http://camunda:8080/engine-rest"
   # Название сообщения для отправки в Camunda
   camunda.messageName: BG_SCAN

   # Количество попыток отправить сообщение в Camunda
   camunda.redeliveryAttempts: 200

   # Интервал между повторами отправки сообщений в Camunda в случае ошибки (мс):
   camunda.redeliveryInterval: 3600000

   # Параметр загрузки сканов в 'common-api-doc'
   doc:
     # Тип документа
     typeName: BG_SCAN
     # Базовый URL к 'common-api-doc'
     baseUrl: "http://proxy:8888/docapi"
   # Ключ для проверки корректности QR-кода
   qrcode.key: 2OJnRYbu9C9WWfBuXsOB7ENO7mXIALsJW3EKjd54Nhs=

Links
-----

1) File -
   https://github.com/apache/camel/blob/master/components/camel-file/src/main/docs/file-component.adoc

2) https://github.com/apache/camel/blob/master/components/camel-spring-boot/src/main/docs/spring-boot.adoc

3) сканы гарантий оказываются в заданной папке

4) берется каждый файл из папке (предполагается что все файлы PDF)

5) ищется в файле qrcode c идентификатором заявки

6) загружается файл в docapi с заданными doctype

7) отправляется сообщение в Camunda с businessKey = orderId
